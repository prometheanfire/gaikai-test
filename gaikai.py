#!/usr/bin/env python
#Copyright 2013 Matthew Thode

#Licensed under the Apache License, Version 2.0 (the "License");
#you may not use this file except in compliance with the License.
#You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
#Unless required by applicable law or agreed to in writing, software
#distributed under the License is distributed on an "AS IS" BASIS,
#WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#See the License for the specific language governing permissions and
#limitations under the License.

# time for a little self critique :D
# the lines are a little long...
# this wasn't quite perfect, as the subsections don't have their title ('Design and development of' should be before
# 'Integration test strategies' for instance.
# for this reason, I decided to remove the breaks that caused the issue


from flask import Flask
import get_and_process
import json


app = Flask(__name__)


@app.route('/')
def index():
    """
     Just didn't want it blank
    """
    return 'Index Page\n'


@app.route('/jobs/')
@app.route('/jobs')
@app.route('/jobs/<title>')
def jobs(title=None):
    """
     Will return a list of jobs in json format unless a specific job is requested
    """
    raw_html = get_and_process.get_page("http://www.gaikai.com/careers")
    jobs = get_and_process.process_page(raw_html)
    if title is None:
        return json.dumps(jobs)
    else:
        for job in jobs:
            if job['title'] == title:
                return json.dumps(job)


if __name__ == '__main__':
    app.run()
