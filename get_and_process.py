#!/usr/bin/env python
#Copyright 2013 Matthew Thode

#Licensed under the Apache License, Version 2.0 (the "License");
#you may not use this file except in compliance with the License.
#You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
#Unless required by applicable law or agreed to in writing, software
#distributed under the License is distributed on an "AS IS" BASIS,
#WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#See the License for the specific language governing permissions and
#limitations under the License.

# time for a little self critique :D
# the lines are a little long...
# this wasn't quite perfect, as the subsections don't have their title
#   ('Design and development of' should be before 'Integration test strategies'
# for this reason, I decided to remove the breaks that caused the issue


from lxml import etree
from StringIO import StringIO
import urllib2
import json
import re


def dictlist(node):
    """
     creates a dictionary list
    """
    res = {node.tag: []}
    xmltodict(node, res[node.tag])
    return {node.tag: {'value': res[node.tag],
                       'attribs': node.attrib,
                       'tail': node.tail}}


def xmltodict(node, res):
    """
     convert xml to dict
    """
    if len(node):
        for n in list(node):
            rep = {node.tag: []}
            value = xmltodict(n, rep[node.tag])
            if len(n):
                value = {'value': rep[node.tag],
                         'attributes': n.attrib,
                         'tail': n.tail}
                res.append({n.tag: value})
            else:
                res.append(rep[node.tag][0])
    else:
        value = {'value': node.text,
                 'attributes': node.attrib,
                 'tail': node.tail}
        res.append({node.tag: value})
    return


def unicode_cleanup(input_string):
    """
     replacing unicode characters with ascii for simplicity
    """
    input_string = input_string.replace('\n', '')
    #removing bullets
    output = re.sub(u'\u2022' + ' ' + '|' + ' ' +
                    u'\u2022' + ' ', '', input_string)
    #replacing ...
    output = re.sub(u'\u2026', '...', output)
    #replacing right quote mark with a simple quote
    output = re.sub(u'\u2019', '\'', output)
    return output


def get_page(url="http://www.gaikai.com/careers"):
    """
     gets the raw html for the gaikai page
    """
    return urllib2.urlopen(url).read()


def process_page(raw_html):
    """
     pulls the career page and converts the listed jobs to json.
    """
    #I don't think you want nested lists (made from <br>), so I discounted them
    #the code is commented out below that can handle it.
    processed = re.sub('&#58;<br>', '&#58; ', raw_html)
    processed = re.sub('<br>', ', ', processed)
    parser = etree.HTMLParser()
    tree = etree.parse(StringIO(processed), parser)
    res = dictlist(tree.getroot())
    #try and access the data
    try:
        filtered = res['html']['value'][2]['body']['value'][0]['div']['value'][1]['div']['value'][0]['section']['value'][1]['div']['value'][1]['section']['value']
    except(IndexError, KeyError):
        return None

    job_list = []
    for job in filtered:
        title = ''
        responsibilities = []
        requirements = []
        skills = []
        additional_attributes = []

        for item in job['article']['value']:
            #get job title
            if 'h1' in item:
                title = unicode_cleanup(item['h1']['value'][0]['span']['tail'])
            #get the rest of the data
            if 'section' in item:
                for section in item:
                    #get Responsibilities
                    if item['section']['value'][0]['h2']['value'] == 'Responsibilities':
                        for responsibility in item['section']['value'][1]['ul']['value']:
                            responsibilities.append(unicode_cleanup(responsibility['li']['value']))
                    #get Requirements
                    if item['section']['value'][0]['h2']['value'] == 'Requirements':
                        for requirement in item['section']['value'][1]['ul']['value']:
                            requirements.append(unicode_cleanup(requirement['li']['value']))
                    #get Skills & Knowledge
                    if item['section']['value'][0]['h2']['value'] == 'Skills & Knowledge':
                        for skill in item['section']['value'][1]['ul']['value']:
                            skills.append(unicode_cleanup(skill['li']['value']))
                    #get Additional Attributes
                    if item['section']['value'][0]['h2']['value'] == 'Additional Attributes':
                        for attribute in item['section']['value'][1]['ul']['value']:
                            additional_attributes.append(unicode_cleanup(attribute['li']['value']))
        job_list.append({
            'title': title,
            'responsibilities': responsibilities,
            'requirements': requirements,
            'skills': skills,
            'additionalAttributes': additional_attributes
        })
    return job_list


if __name__ == '__main__':
    """
     get the page, process it, then display it nicely
    """
    print json.dumps(process_page(get_page()), indent=2)
