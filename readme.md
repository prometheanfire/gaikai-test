# This will be a simple how-to for running this small applet thingy


## prerequisites
make sure you have the following installed

* lxml
* urllib2 (should be installed in the base python)
* flask
* unittests (if you wish to run tests)

### getting the code
    git clone https://prometheanfire@bitbucket.org/prometheanfire/gaikai-test.git
    cd gaikai-test
    python gaikai.py

In another terminal run the following

    curl "http://127.0.0.1:5000/jobs"
    curl "http://127.0.0.1:5000/jobs/Business%20Planning%20Analyst"

### Tests
Run the following command in the base of the git directory to test

    python test.py
