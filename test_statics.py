#!/usr/bin/env python
# coding=utf-8
#Copyright 2013 Matthew Thode

#Licensed under the Apache License, Version 2.0 (the "License");
#you may not use this file except in compliance with the License.
#You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
#Unless required by applicable law or agreed to in writing, software
#distributed under the License is distributed on an "AS IS" BASIS,
#WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#See the License for the specific language governing permissions and
#limitations under the License.


good_html = """<!DOCTYPE html>
<!--[if lt IE 7 ]> <html lang="en" class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en" class="no-js"> <!--<![endif]-->
<head>
    <!-- d:19, s:0.9.15-->
    <meta charset="utf-8" />
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <!-- meta name="viewport" content="initial-scale=1.0, width=device-width" /-->
    <meta name="robots" content="index,follow" />
    <meta name="robots" content="noodp,noydir" />
    <meta name="copyright" content="Gaikai 2013 All Rights Reserved" />

    <meta property="og:url" content="http://www.gaikai.com" />
    <meta property="og:title" content="Gaikai, Inc. A Sony Computer Entertainment Company" />
    <meta property="og:type" content="website" />
    <meta property="og:image" content="https://fbcdn-photos-a.akamaihd.net/photos-ak-snc1/v85006/99/217593111660471/app_1_217593111660471_1988187257.gif" />
    <meta property="og:description" content="Gaikai is Cloud Gaming." />

    <script type="text/javascript" src="/assets/js/modernizr.js"></script>
    <link rel="shortcut icon" href="/assets/images/favicon.ico" />
    <title>Gaikai.com :: Careers</title>

    <link rel="stylesheet" href="/assets/css/screen.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="/assets/css/woff-embed.css" type="text/css" media="screen and (min-width:641px)" />
    <script type="text/javascript" data-main="/assets/js/main" src="/assets/js/require-jquery.js"></script>

</head>
<body data-resource="careers">

<div id="body-container">

    <header>
        <div class="wrap">
            <section class="content">
                <h1><a href="/" target="_parent">::Gaikai&trade;</a></h1>
                <nav>
                    <a href="/" target="_top">Home</a>
                    <a href="/about" target="_top">About</a>
                    <a href="/history" class="history" target="_top">History</a>
                    <a href="/qa" target="_top">Q&A</a>
                    <a href="/careers" class="activeSection" target="_top">Careers</a>
                    <a class="contact">Contact</a>
                </nav>
            </section>
            <section class="contact">
                <div class="information">
                    <span class="business">
                        <strong>Business Inquiries</strong>
                        <a href="mailto:business@gaikai.com">business@gaikai.com</a>
                    </span>
                    <span class="press">
                        <strong>Press Inquiries</strong>
                        <a href="mailto:press@gaikai.com">press@gaikai.com</a>
                    </span>
                    <span class="support">
                        <strong>Support</strong>
                        <a href="mailto:support@gaikai.com">support@gaikai.com</a>
                    </span>
                </div>
                <span class="close">Close</span>
            </section>
        </div>
    </header>

    <div id="content">

<section id="careers">
    <div class="mainBGContainer" data-bg-src="/assets/images/backgrounds/careers_20121116.jpg"></div>
    <div class="wrap">
        <section class="overview">
            <h1>Grow with us</h1>
            <p>We&#39;re growing, and we&#39;ve got room for people who share our passion for elevating
the video game industry by making all games as accessible as movies and music are today.</p>
<p>Our many benefits include medical, dental, vision, disability, health club discounts,
life insurance, 401k matching, employee discounts, and our sunny Orange County headquarters
location.</p>
<p>We have open positions for every level of talent, from new college grads to seasoned
video game industry professionals. Have a look at our list of open positions and apply if
something looks like a fit.</p>
<p><a href="mailto:careers@gaikai.com" title="Careers" class="linkCareersMain">Click here to apply via email.</a></p>

        </section>

        <section class="openings accordion">

            <article id="opening-0" class="row">
                <h1><span class="arrow"></span>Senior Solution Architect</h1>



                    <section class="content">

                        <h2>Responsibilities</h2>

                        <ul>

                            <li>Interface with lead software engineers, network engineering, and service reliability engineering.</li>

                            <li>Define system and software architecture for highly distributed and scalable communication and management systems.</li>

                            <li>Create detailed design specifications and APIs for internal and external developers.</li>

                        </ul>
                    </section>

                    <section class="content">

                        <h2>Requirements</h2>

                        <ul>

                            <li>BS, MS in Computer Science or 8 years of relevant work experience.</li>

                            <li>Expertise in several of: C, C++, Java, Python/Bash, UNIX/Linux use and internals, IP networking, and network devices.</li>

                            <li>Knowledge of high-availability software design.</li>

                        </ul>
                    </section>

                    <section class="content">

                        <h2>Skills &amp; Knowledge</h2>

                        <ul>

                            <li>10 years of relevant hands-on technical management experience of software engineering, as well as a record of individual technical achievements.</li>

                            <li>Capable of technical deep-dives into software architecture, distributed systems, networking, and operating systems yet verbally and cognitively agile enough to hold their own in a strategy discussion with Gaikai’s executive team.</li>

                            <li>That rare mix of intelligence, integrity, domain knowledge, verbal agility, and diplomacy which allows you to rapidly earn the trust of technically astute teams across the company.</li>

                        </ul>
                    </section>



            </article>

            <article id="opening-1" class="row">
                <h1><span class="arrow"></span>Security Manager</h1>



                    <section class="content">

                        <h2>Responsibilities</h2>

                        <ul>

                            <li>Build and lead the Gaikai security team which will create and maintain the safest operating environment for Gaikai’s users and employees.</li>

                            <li>Protect the Gaikai network boundaries, keep computer systems and network devices hardened against attacks, and provide security services to protect highly sensitive data like passwords and customer information.</li>

                            <li>Work with all Gaikai staff to proactively identify and fix security flaws and vulnerabilities.</li>

                            <li>Work with Sony security engineering to define and enforce security policies and guidelines.</li>

                        </ul>
                    </section>

                    <section class="content">

                        <h2>Requirements</h2>

                        <ul>

                            <li>BS, MS in Computer Science or 8 years of relevant work experience.</li>

                            <li>10 years of relevant hands-on technical management experience of security engineering, as well as a record of individual technical achievements.</li>

                            <li>Strong foundation and in-depth technical knowledge of security engineering, computer and network security, authentication and security protocols, and applied cryptography.</li>

                        </ul>
                    </section>

                    <section class="content">

                        <h2>Skills &amp; Knowledge</h2>

                        <ul>

                            <li>Capable of technical deep-dives into security, code, networking, and operating systems yet verbally and cognitively agile enough to hold their own in a strategy discussion with Gaikai’s executive team.</li>

                            <li>Comfortable recruiting and managing a team of very bright and experienced engineers.</li>

                            <li>That rare mix of intelligence, integrity, domain knowledge, verbal agility, and diplomacy which allows you to rapidly earn the trust of technically astute teams across the company.</li>

                        </ul>
                    </section>



            </article>

            <article id="opening-2" class="row">
                <h1><span class="arrow"></span>Senior Software Engineer</h1>



                    <section class="content">

                        <h2>Responsibilities</h2>

                        <ul>

                            <li>Design and development of:<br>&bull; Reliable and flexible automated test systems<br>&bull; Automated test frameworks and libraries<br>&bull; Integration test strategies<br>&bull; Integration and load testing scripts</li>

                            <li>Work with teams, Test Engineering managers, and project managers to define project requirements and deliver them within schedule constraints.</li>

                            <li>Work with cloud, operations, and core software engineers to develop integration and load tests for various software components.</li>

                        </ul>
                    </section>

                    <section class="content">

                        <h2>Requirements</h2>

                        <ul>

                            <li>BS in Computer Science or 10 years of relevant work experience</li>

                            <li>5 years of relevant programming experience</li>

                            <li>Strong, demonstrable knowledge of Python and automated testing</li>

                        </ul>
                    </section>

                    <section class="content">

                        <h2>Skills &amp; Knowledge</h2>

                        <ul>

                            <li>Strong knowledge of Bash</li>

                            <li>Knowledge of build tools and systems</li>

                            <li>Knowledge of networked and distributed systems</li>

                            <li>Familiar with Agile development environment</li>

                            <li>Linux experience</li>

                        </ul>
                    </section>

                    <section class="content">

                        <h2>Additional Attributes</h2>

                        <ul>

                            <li>Version control systems (preferably Git)</li>

                            <li>Gamer is a plus</li>

                            <li>Enjoys working in a fast-paced environment</li>

                            <li>Strong communication and documentation skills</li>

                        </ul>
                    </section>



            </article>

            <article id="opening-3" class="row">
                <h1><span class="arrow"></span>Release Engineer</h1>



                    <section class="content">

                        <h2>Responsibilities</h2>

                        <ul>

                            <li>Maintain build systems, continuous integration/delivery tools, and pipeline</li>

                            <li>Design and development of Release Engineering projects and tools to aid in release pipeline</li>

                            <li>Work with team and project managers to deliver quality software within schedule constraints</li>

                        </ul>
                    </section>

                    <section class="content">

                        <h2>Requirements</h2>

                        <ul>

                            <li>Demonstrable knowledge of distributed architectures and OOP</li>

                            <li>BS in Computer Science or two years of professional experience</li>

                        </ul>
                    </section>

                    <section class="content">

                        <h2>Skills &amp; Knowledge</h2>

                        <ul>

                            <li>Expert level knowledge of Linux</li>

                            <li>Strong Bash scripting skills</li>

                            <li>Advanced skills in Python</li>

                            <li>Familiarity with build systems</li>

                            <li>Familiarity with continuous integration and delivery</li>

                        </ul>
                    </section>

                    <section class="content">

                        <h2>Additional Attributes</h2>

                        <ul>

                            <li>Version control systems (preferably Git)</li>

                            <li>Gamer is a plus</li>

                            <li>Enjoys working in a fast-paced environment</li>

                        </ul>
                    </section>



            </article>

            <article id="opening-4" class="row">
                <h1><span class="arrow"></span>Senior Python Engineer</h1>



                    <section class="content">

                        <h2>Responsibilities</h2>

                        <ul>

                            <li>Design and development of:<br>&bull; Server-side applications for cloud automation on a large distributed infrastructure.<br>&bull; Tools to aid with debugging, server management, and integration with various system components.</li>

                            <li>Developing libraries, frameworks, or toolkits used by multiple projects.</li>

                            <li>Work with team and project managers to deliver quality software within schedule constraints.</li>

                        </ul>
                    </section>

                    <section class="content">

                        <h2>Requirements</h2>

                        <ul>

                            <li>Demonstrable knowledge of distributed architectures, OOP, Python, and Multithreading.</li>

                            <li>5+ years programming experience.</li>

                            <li>BS or 8 years of relevant work experience.</li>

                        </ul>
                    </section>

                    <section class="content">

                        <h2>Skills &amp; Knowledge</h2>

                        <ul>

                            <li>Client/Server and p2p architectures and protocols</li>

                            <li>RESTful interfaces, UML</li>

                            <li>Programming best practices including unit testing, integration testing, static analysis, and code documentation</li>

                            <li>Familiar with Agile and other development best practices</li>

                            <li>Knowledge of networked and distributed storage approaches</li>

                        </ul>
                    </section>

                    <section class="content">

                        <h2>Additional Attributes</h2>

                        <ul>

                            <li>Can work with small, high-functioning teams or independently as needed</li>

                            <li>Good communication skills</li>

                            <li>Team player</li>

                            <li>Thrives in a fast-paced creative environment</li>

                            <li>Gamer is a plus</li>

                            <li>Familiarity with GitHub, pyLint, py.test, and Sphinx</li>

                            <li>Strong work ethic, willing to go the extra mile when required</li>

                            <li>Contributor to open source projects is a plus</li>

                        </ul>
                    </section>



            </article>

            <article id="opening-5" class="row">
                <h1><span class="arrow"></span>Senior Business Intelligence Analyst</h1>



                    <section class="content">

                        <h2>Responsibilities</h2>

                        <ul>

                            <li>Maintain the current BI solution in various environments (Development, Staging, and Production).</li>

                            <li>Interface with both Business users and Engineers to satisfy reporting requirements.</li>

                            <li>Develop new reporting projects and gather requirements.</li>

                            <li>Architect data models and warehouse solutions for new projects.</li>

                        </ul>
                    </section>

                    <section class="content">

                        <h2>Requirements</h2>

                        <ul>

                            <li>BS or 5 years of relevant work experience.</li>

                            <li>5 years experience with MicroStrategy (minimum version 9.0) including, but not limited to&#58;<br>
Architecture, Reporting Design, Dashboard Design, Mobile Technologies, Administrator, Clustered Environments, Object Manager, and Integrity Manager.
</li>

                            <li>Working knowledge or experience with data visualization tools (Tableau, Qlikview, or Spotfire).</li>

                        </ul>
                    </section>

                    <section class="content">

                        <h2>Skills &amp; Knowledge</h2>

                        <ul>

                            <li>Strong SQL skills and the ability to tune SQL queries for performance.</li>

                            <li>Experience with Big Data solutions such as Vertica and Hadoop.</li>

                            <li>Database Modeling and Warehouse design (Snowflake and Star Schema).</li>

                            <li>Strong understanding of ETL techniques.</li>

                        </ul>
                    </section>

                    <section class="content">

                        <h2>Additional Attributes</h2>

                        <ul>

                            <li>SDK experience preferred but not necessary</li>

                            <li>Critical Thinking</li>

                            <li>Good communication skills</li>

                            <li>Understanding of basic business principles and the ability to self-prioritize</li>

                        </ul>
                    </section>



            </article>

            <article id="opening-6" class="row">
                <h1><span class="arrow"></span>IT Manager</h1>



                    <section class="content">

                        <h2>Responsibilities</h2>

                        <ul>

                            <li>Account management via LDAP, Google Apps, and Atlassian Crowd for all Gaikai employees and services.</li>

                            <li>Purchasing for all of company. This includes ordering accessories via online retailers and issuing PO’s for larger orders.</li>

                            <li>Administration of IT services on Gentoo Linux including&#58;<br> &bull; Atlassian Crowd, JIRA, Confluence, FishEye<br> &bull; OpenCA<br> &bull; Graphite<br> &bull; OpenLDAP<br> &bull; BIND<br> &bull; Automated backups<br> &bull; NetApp<br></li>

                            <li>Administration of all desktop computers and laptops used by employees. Entails having a desktop or laptop ready on the employee’s first day. Operating systems include Windows 7, Windows 8, Mac OS X, Gentoo and Ubuntu Linux.</li>

                            <li>Manage IP address space and DNS names for office computers.</li>

                            <li>Perform physical network moves, adds, and changes.</li>

                            <li>Perform basic management of the desk phones.</li>

                            <li>Administration of the office wireless network.</li>

                            <li>Move, install, and provision servers in offices and datacenters.</li>

                            <li>Manage and mentor peers.</li>

                            <li>Be a main point of contact for employees seeking information and resources.</li>

                        </ul>
                    </section>



            </article>

            <article id="opening-7" class="row">
                <h1><span class="arrow"></span>Technical Operations Project Manager x2</h1>


                    <p class="content">Gaikai is seeking a Technical Operations Project Manager with enterprise infrastructure experience. The projects will support the Technical Operations organization whose responsibilities include the growth, management, and 24/7 upkeep of Gaikai/SCE's streaming platform.</p>




                    <section class="content">

                        <h2>Responsibilities</h2>

                        <ul>

                            <li>Manage cross-functional site infrastructure projects in a matrix organization covering a range of areas (datacenter, network, hardware systems, infrastructure software engineering, capacity management).</li>

                            <li>Develop and manage end-to-end project plans and ensure on-time delivery.</li>

                            <li>Provide hands-on program management during analysis, design, development, testing, implementation, and post implementation phases.</li>

                            <li>Perform risk management and change management on projects.</li>

                            <li>Provide day-to-day coordination and quality assurance for projects and tasks.</li>

                            <li>Drive internal process improvements across multiple teams and functions.</li>

                            <li>Interface with Engineering and Business Owners for project requirements and scope.</li>

                            <li>Report directly to VP of Technical Operations</li>

                        </ul>
                    </section>

                    <section class="content">

                        <h2>Requirements</h2>

                        <ul>

                            <li>BS in a technical discipline or 6 years of relevant work experience.</li>

                            <li>Strong verbal and written communication skills.</li>

                            <li>Strong organizational and coordination skills along with multitasking capabilities to get things done in a fast-paced environment.</li>

                            <li>Excellent interpersonal skills, including relationship building and collaboration within a diverse, cross-functional team.</li>

                            <li>Working knowledge of Project Management tools. Knowledge of Microsoft Project.</li>

                            <li>Experience understanding application requirements and translating needs into capacity plans.</li>

                        </ul>
                    </section>

                    <section class="content">

                        <h2>Additional Attributes</h2>

                        <ul>

                            <li>San Francisco Bay area (San Mateo) or Orange County (Aliso Viejo)</li>

                            <li>Experience with datacenter architecture and HW life cycle management a plus.</li>

                            <li>Internet startup and technical infrastructure management experience a big plus.</li>

                            <li>Experience with managing projects related to rapid network, systems, datacenter growth, and hardware design. Capacity management a plus.</li>

                            <li>PMP Certification (PMI) a plus.</li>

                        </ul>
                    </section>



            </article>

            <article id="opening-8" class="row">
                <h1><span class="arrow"></span>Service Reliability Engineering Manager (1x US, 1x EU)</h1>


                    <p class="content">The Service Reliability Engineering Manager is responsible for day-to-day health and uptime for all Gaikai/SCE services. As the manager, you are responsible for maintaining and improving service uptime, headcount growth, personnel management, and service stability. Additionally, this role is responsible for handling either planned or unplanned maintenance events as well as executing capacity and capability growth as Gaikai expands.</p>




                    <section class="content">

                        <h2>Responsibilities</h2>

                        <ul>

                            <li>Build and direct a team of engineers across many time zones who work to analyze and maintain service stability by documenting policies and best practices in a 24/7/365 operation.</li>

                            <li>Own the day-to-day health, uptime and reliability of all network, server, storage, and ancillary infrastructure to fulfill the mission of unyielding site stewardship.</li>

                            <li>Work closely with cross-functional teams to negotiate requirements, specifications, schedules, quality, and technical acceptance criteria.</li>

                            <li>Work closely with Project Management, Operational and Engineering peers to develop innovative technical solutions that meet Gaikai’s needs with respect to functionality, performance, scalability, and reliability.</li>

                            <li>Identify tactical issues and emerging areas of concern.</li>

                            <li>Develop reports and feedback to inform technical solutions that meet design needs.</li>

                        </ul>
                    </section>

                    <section class="content">

                        <h2>Requirements</h2>

                        <ul>

                            <li>BS in Computer Science or equivalent experience preferred.</li>

                            <li>At least 5 or 6 years of experience managing an operations organization in a 24/7 global infrastructure as well as a record of individual technical achievements.</li>

                            <li>A strong background in Internet service deployments and a deep understanding of Linux and Internet technologies.</li>

                            <li>A natural team leader who can motivate and encourage personal advancement.</li>

                            <li>Excellent project management skills and the ability to work in a fast-paced and hectic work environment.</li>

                            <li>Capable of technical deep-dives into code, networking, systems, and storage with very bright, experienced engineers.</li>

                            <li>Capable of leading a discussion with executive management.</li>

                            <li>Demonstrated experience in network and large-scale UNIX system troubleshooting and maintenance practices.</li>

                            <li>Must be willing to travel to domestic and international datacenters and office locations.</li>

                        </ul>
                    </section>



            </article>

            <article id="opening-9" class="row">
                <h1><span class="arrow"></span>Vendor Management / Sourcing Manager</h1>



                    <section class="content">

                        <h2>Responsibilities</h2>

                        <ul>

                            <li>Contract Strategy and Execution&#58; Lead the business negotiation process with internal partners and suppliers. Own contract closure.</li>

                            <li>Develop negotiation approaches, fallback positions, acceptable terms and conditions through to closure.</li>

                            <li>RFx Services&#58; Own and lead the supplier interface for all RFI, RFP, and RFQ services. Partner with appropriate internal stakeholders.</li>

                            <li>Present RFx results to evaluation team and drive the closure of a sourcing and purchase decision.</li>

                            <li>SLA Performance&#58; Continually measure, manage, and improve supplier actual performance against negotiated SLA.</li>

                            <li>Competitive Benchmarking&#58; Identify key benchmark suppliers in RFx efforts. Partner to identify evaluation criterion and provide relevant data back to internal teams.</li>

                            <li>Cost Modeling&#58; Partner with Site Operations to ensure best in class pricing is secured for datacenter infrastructure and operations. Drive continual improvement of cost productivity.</li>

                            <li>Supplier survey, audit and qualification&#58; Lead and coordinate all efforts to survey, audit, assess, and qualify new suppliers.</li>

                            <li>Technology Roadmap&#58; Partner with Gaikai Site Operations to coordinate suppliers and review supplier technology and services roadmaps.</li>

                            <li>Preferred Supplier Management&#58; Identify, qualify, and formalize which suppliers are on Gaikai’s preferred supplier list. Partner with internal stakeholders to ensure alignment.</li>

                            <li>Perform risk analysis of Supplier and Supply Chain. Develop risk mitigation and business continuity plans.</li>

                            <li>Ongoing Cost Productivity&#58; Measure and report action cost productivity as a function of market pricing.</li>

                            <li>Scorecard Management&#58; Lead measuring supplier performance, root causing issues, and drive improved performance. Drive improvement on all aspects of supplier performance (cost, quality, and delivery).</li>

                        </ul>
                    </section>

                    <section class="content">

                        <h2>Requirements</h2>

                        <ul>

                            <li>BA/BS or equivalent with at least 10 years of relevant experience.</li>

                            <li>At least 5 years of Sourcing Management or Supply Chain experience.</li>

                            <li>Management experience in related products and services.</li>

                            <li>Must have 5+ years experience in negotiating cost, managing supplier performance, and driving supplier quality improvements.</li>

                            <li>Willing to travel domestically and internationally, as needed.</li>

                            <li>Proven success in contract negotiations related to commodities, equipment, services, and end-to-end supplier management. Must be analytical and results focused.</li>

                        </ul>
                    </section>

                    <section class="content">

                        <h2>Additional Attributes</h2>

                        <ul>

                            <li>Reports directly to VP of Technical Operations</li>

                            <li>San Francisco Bay area (San Mateo) or Orange County (Aliso Viejo)</li>

                            <li>Prior experience in networking, cloud industry, or related field</li>

                        </ul>
                    </section>



            </article>

            <article id="opening-10" class="row">
                <h1><span class="arrow"></span>Datacenter Site Operations / Provisioning Engineer</h1>



                    <section class="content">

                        <h2>Responsibilities</h2>

                        <ul>

                            <li>Install and troubleshoot physical infrastructure, including switches, routers, and servers.</li>

                            <li>Maintain accurate and complete inventory of datacenter assets.</li>

                            <li>Follow best practices in datacenter and rack-level design, power distribution, and cabling.</li>

                            <li>Assist in onsite troubleshooting of server or network issues.</li>

                        </ul>
                    </section>

                    <section class="content">

                        <h2>Requirements</h2>

                        <ul>

                            <li>4-5 years working with Linux and hardware systems support in a datacenter setting.</li>

                            <li>Experienced in large-scale datacenter deployments including server, storage, and network equipment installs.</li>

                            <li>Understanding of Out-of-band management methods such as IPMI and serial console.</li>

                            <li>Must be able to lift 60-70 lbs of equipment on a daily basis.</li>

                            <li>Excellent verbal and written communication skills.</li>

                            <li>Available for extensive travel, both domestically and internationally.</li>

                        </ul>
                    </section>

                    <section class="content">

                        <h2>Additional Attributes</h2>

                        <ul>

                            <li>Reports to Director of Datacenter Operations</li>

                            <li>San Francisco Bay area (San Mateo) or Orange County (Aliso Viejo)</li>

                        </ul>
                    </section>



            </article>

            <article id="opening-11" class="row">
                <h1><span class="arrow"></span>Datacenter Logistics Manager</h1>


                    <p class="content">Gaikai is looking for an industry professional that has experience in the following infrastructure and supply chain operations areas: datacenters, procurement, IT, people management (local/remote), and domestic/international logistics experience. The Datacenter Logistics Manager will be responsible for all aspects of datacenter logistics, procurement, personnel, vendors, contractors, assets, inventory, and other operational requirements of the business.</p>

                    <p class="content">Successful candidate should be adaptable in using their organizational skills to prioritize tasks and opportunistic in taking ownership of various projects, have the urgency to anticipate busy schedules to communicate with other members of the department to complete assignments on time and accurately, and possess openness in their problem-solving and integrity in their research to effectively address the needs of employees company-wide. This is an exciting time to join a company that values and rewards its employees. If you are interested in providing operations support, value working relationships, and enjoy datacenter logistics & procurement operations, this is the place for you!</p>




                    <section class="content">

                        <h2>Responsibilities</h2>

                        <ul>

                            <li>Day-to-day datacenter logistics operations and long-term strategy</li>

                            <li>Must possess an excellent understanding of an ERP (Enterprise Resource Planning) system dealing with but not limited to inventory control, supply/demand allocation, warehousing, and shipping/receiving systems.</li>

                            <li>Experience managing multiple warehouse locations, inventory management, and shipping/receiving.  International experience is a must.</li>

                            <li>Excellent verbal and written communication skills</li>

                            <li>Oversee the lead time report preparation/distribution</li>

                            <li>Monitor open purchase orders for receipt of goods, monitor receipts, and packing slips</li>

                            <li>Project Management</li>

                            <li>Product life cycle</li>

                            <li>Financial tracking</li>

                            <li>SOX Compliance</li>

                            <li>Logistics, transportation, asset management, and RFID</li>

                            <li>Creation and management of documentation, reporting, and metrics</li>

                            <li>Provides direction to staff, establishes work priorities, and evaluates proposed solutions</li>

                            <li>Management of ticket queue and other work requested</li>

                            <li>Contract Management</li>

                            <li>Computer hardware, servers, network equipment, miscellaneous equipment, and spares</li>

                            <li>Responding to business emergencies</li>

                            <li>Enforcing, managing, and creating policies, procedures, and security</li>

                            <li>Set schedule and resolve staff scheduling issues</li>

                            <li>Management of industry logistics standards and best practices</li>

                            <li>Training, mentoring, and documentation</li>

                            <li>Perform on-call support as needed</li>

                            <li>Occasional travel, only if required</li>

                            <li>Responsible but not limited to RMA’s, shipments, PO’s, sign in/out, filing, and audits</li>

                            <li>Ability to effectively manage multiple priorities while adhering to deadlines</li>

                            <li>Must be flexible and willing to work overtime as needed.</li>

                            <li>Proficiency in Microsoft Suite and Project Management</li>

                            <li>Capable of working closely with other functional departments to drive continuous improvements</li>

                            <li>Other duties not yet defined</li>

                        </ul>
                    </section>

                    <section class="content">

                        <h2>Requirements</h2>

                        <ul>

                            <li>BS/BA degree or equivalent, preferably in a technical discipline, or equivalent training, education, and on the job experience</li>

                            <li>At least 3+ years of datacenter technical experience</li>

                            <li>5+ years of datacenter work experience in a management role</li>

                            <li>At least 2+ years of IT Procurement</li>

                            <li>Understanding of server hardware - Sun, Intel, Network Appliance, and Cisco</li>

                            <li>Cabling knowledge including fiber, coax, and copper runs</li>

                            <li>Demonstrated project management and problem analysis/resolution skills.</li>

                            <li>Must be able to go up and down a ladder, lift medium to heavy objects (50 lbs), bend and stoop over for a lengthy period of time, distinguish colors, and have hand dexterity.</li>

                            <li>Candidate must be driven, self-motivated, and a strong team player</li>

                            <li>Must be able to learn quickly and adapt to new solutions/programs/ideas!</li>

                        </ul>
                    </section>

                    <section class="content">

                        <h2>Additional Attributes</h2>

                        <ul>

                            <li>APICS Certification desired (CPIM, CSCP, CFPIM).</li>

                            <li>Domestic/international logistics</li>

                            <li>Procurement</li>

                            <li>IT/Datacenter Operations</li>

                            <li>Understanding of IEEE specifications for systems and network cabling</li>

                            <li>Familiar with power requirements for datacenter hardware</li>

                            <li>Networking hardware such as Cisco, Juniper, Foundry, and/or others</li>

                            <li>Developing and meeting service-level agreement requirements</li>

                            <li>Implementation of an inventory control, asset system, and RFID</li>

                        </ul>
                    </section>



            </article>

            <article id="opening-12" class="row">
                <h1><span class="arrow"></span>Service Reliability Engineer / Systems Administrator (3x US, 5x EU)</h1>


                    <p class="content">Our SRE's focus is on three things: overall ownership of production, production code quality, and deployments.</p>

                    <p class="content">We expect our SREs to have opinions on the state of our network, what we are doing right, and what we can do better.  They are empowered to say when new features are ready for production, and work with other teams to make sure our requirements are met as early in the life cycle as possible.</p>




                    <section class="content">

                        <h2>Requirements</h2>

                        <ul>

                            <li>5+ years in either Software Development or Systems Administration (or both!)&#58; We expect you to be knowledgeable in one or two core fields and open to coming up to speed quickly in everything else.</li>

                            <li>Strong interpersonal and communication skills&#58; You will interact with other teams on a daily basis.</li>

                            <li>A strong sense of responsibility&#58; SREs are largely self-directed, and are key decision makers so they must take pride in the part(s) of production they own.</li>

                            <li>Available for on-call.  There will be times when your expertise is needed outside of core hours.</li>

                        </ul>
                    </section>

                    <section class="content">

                        <h2>Skills &amp; Knowledge</h2>

                        <ul>

                            <li>Development experience in one or more languages&#58; SRE tools are primarily written in Python and Bash, but often need to inspect other languages such as Java, Node.js, C++, and Ruby.</li>

                            <li>Comfortable at a Bash prompt&#58;  Ideal candidate will also be familiar with *nix debugging tools, both at the system (lsof, strace, tcpdump) and code level (gdb, jvisualvm, etc.).</li>

                            <li>One or more of the following areas of expertise&#58;<br> &bull; SQL (ideally MySQL or Postgres)<br> &bull; NoSQL at scale (ideally Hadoop, Mongo clusters, and/or sharded Redis)<br> &bull; Event Aggregation (e.g. Graphite, Zenoss, Flume, Splunk)<br> &bull; Virtualization (ideally in-house clouds using OpenStack or Eucalyptus)<br> &bull; Release Engineering (package management and distribution at scale)<br> &bull; Load Testing (QA or SDET experience is a big plus)</li>

                        </ul>
                    </section>

                    <section class="content">

                        <h2>Additional Attributes</h2>

                        <ul>

                            <li>Reports to SRE Manager</li>

                        </ul>
                    </section>



            </article>

            <article id="opening-13" class="row">
                <h1><span class="arrow"></span>Network Automation Software Engineer</h1>


                    <p class="content">We are looking for talented coders that enjoy a challenge. We value aptitude, motivation, and design sensibility as much as experience.  The work is project-driven development.  Candidates should possess some, but not necessarily all, of the following qualities.</p>




                    <section class="content">

                        <h2>Disposition</h2>

                        <ul>

                            <li>Comfortable with milestone-driven software development</li>

                            <li>Comfortable with quick mock-ups and long-term vision</li>

                            <li>Comfortable with Python, or willing to learn</li>

                            <li>Comfortable with network management concepts, or willing to learn</li>

                        </ul>
                    </section>

                    <section class="content">

                        <h2>Desirable Experience</h2>

                        <ul>

                            <li>Development around data stores, such as HBase, MySQL</li>

                            <li>Development around MVC structured projects</li>

                            <li>Development around ORM based projects</li>

                            <li>REST, SOAP, WSDL APIs</li>

                            <li>Big Data</li>

                            <li>XML, JSON</li>

                        </ul>
                    </section>

                    <section class="content">

                        <h2>Desirable Network Experience</h2>

                        <ul>

                            <li>NetFlow, sFlow</li>

                            <li>SNMP and robust polling architectures</li>

                            <li>MPLS-TE, BGP, ISIS, LLDP, Ethernet, IP</li>

                            <li>Routing Registries, RPSL</li>

                        </ul>
                    </section>



            </article>

            <article id="opening-14" class="row">
                <h1><span class="arrow"></span>Senior Network Engineer with ISP focus</h1>


                    <p class="content">We are looking for talented engineers that enjoy a challenge.  We value aptitude, motivation, and design sensibility as much as experience.  As a senior member of the team, you will be responsible for the mid to long-term considerations of a very large IP network. Candidates should possess some, but not necessarily all, of the following qualities.</p>




                    <section class="content">

                        <h2>Disposition</h2>

                        <ul>

                            <li>Comfortable with multiregional BGP route dissemination</li>

                            <li>Comfortable with bilateral and multilateral peering policy</li>

                            <li>Comfortable with ISIS</li>

                            <li>Comfortable with IPV6</li>

                            <li>Comfortable with MPLS-TE</li>

                        </ul>
                    </section>

                    <section class="content">

                        <h2>Desirable Experience</h2>

                        <ul>

                            <li>Experience with Backbone policy, engineering, and operations</li>

                            <li>Experience with Edge policy, engineering, and operations</li>

                            <li>Experience with multiregional networks</li>

                            <li>Familiarity with multilateral and bilateral peering arrangements</li>

                            <li>Familiarity with Optical Transport</li>

                        </ul>
                    </section>



            </article>

            <article id="opening-15" class="row">
                <h1><span class="arrow"></span>Content Delivery Network Engineer</h1>


                    <p class="content">As a Network Engineer with content focus you will direct the operational models and architecture around Server Load Balancing, Global Server Load Balancing, and CDN.  You will work closely with application owners to optimize the tiers of application delivery. Candidates should have some, but not necessarily all, of the following qualifications.</p>




                    <section class="content">

                        <h2>Skills &amp; Knowledge</h2>

                        <ul>

                            <li>Comfortable with advanced forms of Layer 4 Load Balancing</li>

                            <li>Comfortable with various GSLB technologies</li>

                            <li>Comfortable with application performance benchmarking</li>

                            <li>Comfortable with TCP and UDP</li>

                            <li>Comfortable with Anycast</li>

                            <li>Comfortable with various  LB platforms</li>

                            <li>Comfortable with Layer 7 protocols such as HTTP</li>

                            <li>Comfortable with CDN operations</li>

                        </ul>
                    </section>

                    <section class="content">

                        <h2>Requirements</h2>

                        <ul>

                            <li>Experience with IPV4 and IPV6</li>

                            <li>Experience with Linux</li>

                            <li>Experience with NGINX and Apache</li>

                            <li>Experience with caching methodologies</li>

                        </ul>
                    </section>



            </article>

            <article id="opening-16" class="row">
                <h1><span class="arrow"></span>Network Deployment / Provisioning Engineer</h1>


                    <p class="content">As a Network Engineer with deployment focus you will be responsible for roll-out logistics, processes, and execution of network deployment.  You will work closely with remote Network Engineers and Datacenter Operations to turn up, configure, test, and deliver entire network POPs and datacenters. Candidates should have some, but not necessarily all of the following qualifications.</p>




                    <section class="content">

                        <h2>Skills &amp; Knowledge</h2>

                        <ul>

                            <li>Comfortable with travel</li>

                            <li>Comfortable with various network OSes</li>

                            <li>Comfortable with some network testing equipment</li>

                            <li>Comfortable with structured cabling</li>

                            <li>Comfortable with Optical Transport</li>

                            <li>Comfortable with interface and chassis diagnostics</li>

                            <li>Comfortable with basic power estimation and calculation</li>

                        </ul>
                    </section>

                    <section class="content">

                        <h2>Requirements</h2>

                        <ul>

                            <li>Experience with serial consoles</li>

                            <li>Experience with IPV4 and IPV6</li>

                            <li>Experience with Linux</li>

                            <li>Automation and scripting</li>

                            <li>Multilingual speech</li>

                        </ul>
                    </section>



            </article>

            <article id="opening-17" class="row">
                <h1><span class="arrow"></span>Enterprise Network Engineer</h1>


                    <p class="content">As a Network Engineer with enterprise focus you will be responsible for the design, implementation, and troubleshooting of the corporate office network.  You will work closely with Business Operations and Infrastructure Operations to ensure a reliable, secure, and robust corporate network. Candidates should have some, but not necessarily all of the following qualifications.</p>




                    <section class="content">

                        <h2>Skills &amp; Knowledge</h2>

                        <ul>

                            <li>Comfortable with travel</li>

                            <li>Comfortable with various network OSes</li>

                            <li>Comfortable with some network testing equipment</li>

                            <li>Comfortable with structured cabling</li>

                            <li>Comfortable with optical transport</li>

                            <li>Comfortable with interface and chassis diagnostics</li>

                            <li>Comfortable with basic power estimation and calculation</li>

                        </ul>
                    </section>

                    <section class="content">

                        <h2>Requirements</h2>

                        <ul>

                            <li>Experience with serial consoles</li>

                            <li>Experience with IPV4 and IPV6</li>

                            <li>Experience with Linux</li>

                            <li>Automation and scripting</li>

                            <li>Multilingual speech</li>

                        </ul>
                    </section>



            </article>

            <article id="opening-18" class="row">
                <h1><span class="arrow"></span>Senior Network Engineer with Datacenter focus</h1>


                    <p class="content">As a senior member of the team you will be responsible for the mid to long-term considerations of large datacenter fabrics. Candidates should possess some of, but not necessarily all of the following qualities.</p>




                    <section class="content">

                        <h2>Skills &amp; Knowledge</h2>

                        <ul>

                            <li>Comfortable with basic BGP concepts</li>

                            <li>Comfortable with TCAM based L3/L2 switches</li>

                            <li>Comfortable with large bandwidth platforms</li>

                            <li>Comfortable with ISIS and/or OSPF</li>

                            <li>Comfortable with 802.1Q, LACP, and LLDP</li>

                        </ul>
                    </section>

                    <section class="content">

                        <h2>Requirements</h2>

                        <ul>

                            <li>Experience with IPV4 and IPV6</li>

                            <li>Experience with Linux</li>

                            <li>Experience with DHCP</li>

                            <li>Familiarity with datacenter provisioning</li>

                            <li>Familiarity with structured cabling</li>

                        </ul>
                    </section>



            </article>

            <article id="opening-19" class="row">
                <h1><span class="arrow"></span>Technical Project Manager with Network experience</h1>


                    <p class="content">As a TPM for the Network team you will be responsible for the management of short, medium and long-term projects.  You will work closely with other PMs and various disciplines within networking to ensure the success of major projects.  Candidates should have some, but not necessarily all of the following:</p>




                    <section class="content">

                        <h2>Requirements</h2>

                        <ul>

                            <li>Experience with various project management disciplines</li>

                            <li>Comfortable with various types of network equipment</li>

                            <li>Comfortable with various network acronyms and jargon</li>

                            <li>Comfortable with network components and cabling</li>

                            <li>Comfortable with basic networking concepts, such as addressing, cross-connects, patching, panels, etc.</li>

                            <li>Comfortable with structured cabling concepts</li>

                            <li>Comfortable with datacenter networking concepts</li>

                            <li>Comfortable with Backbone networking concepts</li>

                            <li>Comfortable with peering concepts</li>

                            <li>Comfortable with basic IP concepts, including DNS, DHCP, etc.</li>

                        </ul>
                    </section>

                    <section class="content">

                        <h2>Additional Attributes</h2>

                        <ul>

                            <li>Experience with supporting network provisioning</li>

                            <li>Experience with IPV4 and IPV6</li>

                            <li>Experience with project management software</li>

                            <li>Spreadsheet calculations</li>

                            <li>Leading or managing small teams</li>

                        </ul>
                    </section>



            </article>

            <article id="opening-20" class="row">
                <h1><span class="arrow"></span>Business Planning Analyst</h1>


                    <p class="content">We are looking for a Business Planning Analyst to assist with financial, operational, and strategic planning to support the Business Planning Unit and our parent company. A track record in analytics, business planning, and modeling is a must. Location is Aliso Viejo, CA.</p>




                    <section class="content">

                        <h2>Responsibilities</h2>

                        <ul>

                            <li>Building financial, operational, and strategic planning models.</li>

                            <li>Presentation of model data, forecasts, and other related information at a senior management level.</li>

                            <li>Maintenance and updating of models as information emerges and new requirements are presented.</li>

                            <li>Create detailed forecasts and P&Ls for internal and external parties with focus on relevant metrics and KPIs (ROI, NPV, cash flow analysis, B/E, customer LTV, ARPU, multi-variable correlation, etc.).</li>

                            <li>Assist in qualitative and quantitative research and competitive analysis of overall industry trends.</li>

                            <li>Frequent support in generating internal reports and key PowerPoint presentations optimizing for salient information and flow.</li>

                        </ul>
                    </section>

                    <section class="content">

                        <h2>Requirements</h2>

                        <ul>

                            <li>3+ years of direct work experience in business planning, business intelligence, financial analysis, and/or managerial/operational model building.</li>

                            <li>Advanced model building in Excel including usage of common plug-ins.</li>

                            <li>BA, BS or equivalent degree in Business, Economics, and/or Finance is required.</li>

                            <li>MBA is strongly desired.</li>

                            <li>Startup, Internet, or gaming industry experience preferred.</li>

                            <li>Passion for video games is a plus.</li>

                            <li>Management consulting experience is a plus.</li>

                        </ul>
                    </section>

                    <section class="content">

                        <h2>Skills &amp; Knowledge</h2>

                        <ul>

                            <li>Strong presentation building and communication skills (proficiency in PowerPoint is a must).</li>

                            <li>Understanding of business and accounting finance.</li>

                            <li>Understanding of consumer behavior data set management and manipulation and/or prior work in dense server or cloud system environments is a strong plus.</li>

                            <li>Additional languages (Japanese, Korean, German, Chinese, and/or Spanish) is a strong plus.</li>

                        </ul>
                    </section>

                    <section class="content">

                        <h2>Additional Attributes</h2>

                        <ul>

                            <li>Ability to work with amorphous problems sometimes under limited supervision.</li>

                            <li>Excellent creative problem solving and critical thinking skills.</li>

                            <li>Ability to work under pressure with fixed deadlines.</li>

                            <li>Quick turnaround and accuracy is required.</li>

                            <li>Strong work ethics, high-energy, and driven to match internal team dynamics.</li>

                            <li>Be a team player and very proactive in a highly collaborative culture.</li>

                        </ul>
                    </section>



            </article>

            <article id="opening-21" class="row">
                <h1><span class="arrow"></span>Senior Java Engineer</h1>



                    <section class="content">

                        <h2>Responsibilities</h2>

                        <ul>

                            <li>Design and development of server-side applications for large distributed systems.</li>

                            <li>Design and development of  tools to aid with debugging, management, and interaction with various system components.</li>

                            <li>Developing libraries, frameworks, or toolkits used by multiple projects.</li>

                            <li>Work with team and project managers to deliver quality software within schedule constraints.</li>

                        </ul>
                    </section>

                    <section class="content">

                        <h2>Requirements</h2>

                        <ul>

                            <li>Demonstrable knowledge of distributed architectures, OOP, Core Java, Multithreading, and Networking.</li>

                            <li>5+ years programming experience.</li>

                            <li>BS or 8 years of relevant work experience.</li>

                        </ul>
                    </section>

                    <section class="content">

                        <h2>Skills &amp; Knowledge</h2>

                        <ul>

                            <li>Server/Client Architectures</li>

                            <li>NoSQL, Spring, Tomcat, JUnit, JMS, RESTful interfaces, UML</li>

                            <li>Programming best practices including unit testing, integration testing, static analysis, code documentation, and performance testing</li>

                            <li>Familiar with Agile and other development best practices</li>

                        </ul>
                    </section>

                    <section class="content">

                        <h2>Additional Attributes</h2>

                        <ul>

                            <li>Can work with both a small high-functioning teams or independently as needed</li>

                            <li>Good communication skills</li>

                            <li>Team player</li>

                            <li>Thrives in a fast-paced creative environment</li>

                            <li>Gamer is a plus</li>

                            <li>Strong work ethic, willing to go the extra mile when required</li>

                            <li>Contributor to open source projects is a plus</li>

                        </ul>
                    </section>



            </article>

            <article id="opening-22" class="row">
                <h1><span class="arrow"></span>Front-End Engineer</h1>


                    <p class="content">As a part of Sony Computer Entertainment, Gaikai is leading the cloud gaming revolution, putting console-quality video games on any device, from TVs to consoles to mobile devices and beyond. <br><br>As a Front-End Engineer, you’ll play a key role in architecting and building internal and consumer-facing web apps, including content and catalog management, reporting dashboards, and websites, with an opportunity to define the technology we’ll use to accomplish our goals.</p>




                    <section class="content">

                        <h2>Responsibilities</h2>

                        <ul>

                            <li>Work with designers to implement visually-stunning user interfaces.</li>

                            <li>Work with designers and UX engineers to design user interactions, navigation paradigms, UI transitions and animations, etc. … and then make them happen with elegant JavaScript, HTML, and CSS.</li>

                            <li>Work with back-end web developers to define and implement RESTful APIs that will provide data to the front-end interfaces.</li>

                            <li>Design JavaScript, HTML, and CSS templating systems and processes that are modular and reusable across multiple projects.</li>

                        </ul>
                    </section>

                    <section class="content">

                        <h2>Requirements</h2>

                        <ul>

                            <li>Extensive experience building complex web applications in HTML, CSS, and JavaScript.</li>

                            <li>Advanced knowledge of Sass and Compass.</li>

                            <li>Complete fluency with jQuery, and understanding of JavaScript concepts such as prototypal inheritance, closures, and scope.</li>

                            <li>Familiarity with client-side templating engines, such as Mustache, Handlebars, etc.</li>

                            <li>Working knowledge of major JavaScript libraries and micro-frameworks such as Backbone, Underscore, Ember, etc.</li>

                            <li>Ability to take Photoshop files and translate them into pixel-perfect web interfaces, including animations and transitions both with CSS and jQuery.</li>

                            <li>Experience using Git (specifically GitHub) for source control.</li>

                        </ul>
                    </section>

                    <section class="content">

                        <h2>Additional Attributes</h2>

                        <ul>

                            <li>Professional-level design skills</li>

                            <li>Contributions to open source projects, including your own</li>

                            <li>Ability to code in (or at least grok) web programming languages like PHP or Node.js</li>

                            <li>Experience writing unit tests, especially for JavaScript (JsTestDriver, Jasmine)</li>

                            <li>Experience working with HTML5 features (even the ones that aren’t technically HTML5)&#58; canvas, WebGL, video/audio, local storage, caching, web workers, etc.</li>

                            <li>Experience using web technologies to build applications that run somewhere other than a browser, i.e. smartphones, tablets, TVs, HUDs, kiosks, etc.</li>

                        </ul>
                    </section>



            </article>

            <article id="opening-23" class="row">
                <h1><span class="arrow"></span>Client Software Engineer (C++ / WebKit)</h1>


                    <p class="content">Gaikai is leading the cloud gaming revolution, putting console-quality video games on any device, from TVs to consoles to mobile devices and beyond.<br><br>To do this, we’re using the latest browser technologies (HTML5, CSS3, and JavaScript) in WebKit-based apps to build responsive user interfaces that work across a range of screen sizes, aspect ratios, and use cases.</p>




                    <section class="content">

                        <h2>Responsibilities</h2>

                        <ul>

                            <li>Porting, modifying, hacking and optimizing WebKit to run on a variety of devices with varying levels of CPU, RAM, GPU, etc.</li>

                            <li>Integrating with other technology stacks and application components from other Gaikai teams, other SCE teams, and various device manufacturers and middleware providers.</li>

                            <li>Profile and improve WebKit performance</li>

                        </ul>
                    </section>

                    <section class="content">

                        <h2>Requirements</h2>

                        <ul>

                            <li>Expertise in C/C++ programming</li>

                            <li>Extensive experience porting and modifying WebKit</li>

                            <li>Knowledge of web development using browser technologies like HTML5, CSS3, and JavaScript.</li>

                        </ul>
                    </section>

                    <section class="content">

                        <h2>Additional Attributes</h2>

                        <ul>

                            <li>Experience writing unit and integration tests</li>

                            <li>Knowledge of open source projects, development processes, and best practices</li>

                            <li>Experience using Git (specifically GitHub) for source control</li>

                            <li>Experience building native apps on devices such as smartphones, tablets, TVs, consoles, set-top boxes, etc.</li>

                            <li>Intimate familiarity with WebKit, as a contributor or reviewer</li>

                            <li>Contributions to open source projects, including your own</li>

                            <li>Active in software development community, as a speaker, blogger, or thought leader</li>

                            <li>Experience creating developer tools, like debuggers and profilers</li>

                        </ul>
                    </section>



            </article>

            <article id="opening-24" class="row">
                <h1><span class="arrow"></span>Graphic Designer</h1>


                    <p class="content">As a part of Sony Computer Entertainment, Gaikai is leading the cloud gaming revolution, putting console-quality video games on any device, from TVs to consoles to mobile devices and beyond.<br><br>The Graphic Designer will be responsible for the design and execution of creative concepts for Marketing and Web solutions. You will work with the guidance of Creative Leads and collaborate with developers, IAs, copywriters, and content strategists as a part of the creative process.<br><br>Applicants must submit a resume and URL or PDF of your portfolio. Only applicants with both will be considered.</p>




                    <section class="content">

                        <h2>Responsibilities</h2>

                        <ul>

                            <li>Assist in the execution and production of existing concepts to provide complete deliverables, applying style/brand guides and templates where applicable.</li>

                            <li>Ensure quality and prepare deliverables for hand off to developers, clients, printers, etc.</li>

                            <li>Create graphic assets with some guidance.</li>

                            <li>Assist Lead Designer in creating elements for designs and layouts; apply design principles such as color, typography, photographic selection, organizing elements, and usability.</li>

                            <li>Present rationale for design choices; assure design meets business objectives as stated in creative brief.</li>

                            <li>Edit and make iterations on presentation deck as guided by Lead Designer/Senior Leadership.</li>

                            <li>Evaluate a brand’s visual language (color palette, logo, font, etc.) across a range of media (may include print, online marketing, website, packaging, etc.) with guidance and oversight; document findings.</li>

                            <li>Create posters/mailers for internal use with some guidance.</li>

                        </ul>
                    </section>

                    <section class="content">

                        <h2>Requirements</h2>

                        <ul>

                            <li>Minimum of 1+ years of industry experience</li>

                            <li>Strong design portfolio</li>

                            <li>Expert knowledge of Adobe Creative Suite</li>

                            <li>Ability to multi-task and work efficiently under pressure with careful attention to detail is a must</li>

                            <li>A good working knowledge of interactivity and the Web</li>

                            <li>Passion for design and eagerness to collaborate with other creative people</li>

                            <li>Immaculate attention to detail; must be pixel-perfect</li>

                        </ul>
                    </section>



            </article>

            <article id="opening-25" class="row">
                <h1><span class="arrow"></span>Jr. User Experience Designer</h1>


                    <p class="content">As a part of Sony Computer Entertainment, Gaikai is leading the cloud gaming revolution, putting console-quality video games on any device, from TVs to consoles to mobile devices and beyond.<br><br>The Jr. User Experience Designer must be able to think in terms of the end-to-end user experience and create designs that are visually appealing, easy-to-use, and emotionally satisfying. You will work collaboratively with fellow members of the User Experience team to conceptualize, design, and wireframe user interface ideas.<br><br>Applicants must submit a resume and URL or PDF of your portfolio. Only applicants with both will be considered.</p>




                    <section class="content">

                        <h2>Responsibilities</h2>

                        <ul>

                            <li>Work closely with the UIX Director, Art Director, and UIX Engineers to ensure design concepts are executed as proposed, with the ability to update or revamp changes or solutions where necessary.</li>

                            <li>Help the product development team visualize, architect, and document the concept design based on product requirements. This includes creating wireframes, mock-ups, process flows, and specifications.</li>

                            <li>Create static and interactive design presentations to key members and executive management for review and approval.</li>

                        </ul>
                    </section>

                    <section class="content">

                        <h2>Requirements</h2>

                        <ul>

                            <li>1+ years of industry experience</li>

                            <li>Strong design portfolio</li>

                            <li>Expert knowledge of Adobe Creative Suite</li>

                            <li>Expert knowledge of Axure RP Pro</li>

                            <li>Understanding of how experience design works in harmony with visual design.</li>

                            <li>Good insight into current web design trends and related creative spaces.</li>

                            <li>Immaculate attention to detail; must be pixel-perfect</li>

                            <li>Excellent communication skills, both written and oral</li>

                            <li>Ability to achieve quality results on a tight deadline</li>

                            <li>Motion prototyping is a plus</li>

                        </ul>
                    </section>



            </article>

            <article id="opening-26" class="row">
                <h1><span class="arrow"></span>Data Visualization Architect</h1>


                    <p class="content">As a Data Visualization Architect, you'll play a key role in developing an intuitive web interface that displays big data in creative and easy to understand formats.</p>




                    <section class="content">

                        <h2>Requirements</h2>

                        <ul>

                            <li>Minimum of 5+ years programming experience</li>

                            <li>Web development skills (front and back-end)</li>

                            <li>UI/UX experience</li>

                            <li>Data visualization experience</li>

                            <li>JavaScript expert</li>

                            <li>Ability to take abstract concepts and turn them into an easy to use, intuitive interface of real-time data visualization, and reporting tools</li>

                            <li>Ability to design massively scaleable solutions</li>

                        </ul>
                    </section>

                    <section class="content">

                        <h2>Additional Attributes</h2>

                        <ul>

                            <li>D3 or similar experience</li>

                            <li>Experience with Node.js</li>

                            <li>Familiarity with real-time data monitoring software such as Graphite, Splunk, Ganglia, Zenoss, etc.</li>

                            <li>Familiarity with JQuery, PHP, other languages, etc.</li>

                        </ul>
                    </section>



            </article>

        </section>
        <noscript>
            <style type="text/css">
                #content #qa .accordion article .content, #content #careers .accordion article .content{
                    display: block;
                }
            </style>
        </noscript>

    </div>
    <div class="clearfix"></div>
</section>

    </div>
    <div id="detached"></div>
</div>

<footer>
    <div class="wrap">
        <section class="content">
            <p>GAIKAI and the GAIKAI logo are trademarks or registered trademarks of Gaikai, Inc. (a Sony Computer Entertainment company). ©&nbsp;2013 Gaikai, Inc. All rights reserved.</p>
        </section>
        <section class="social">
            <a href="http://www.facebook.com/GaikaiInc" class="facebook" target="_blank">Facebook</a>
            <a href="http://www.youtube.com/gaikai" class="youtube" target="_blank">YouTube</a>
            <a href="http://twitter.com/gaikai_inc" class="twitter" target="_blank">Twitter</a>
        </section>
    </div>
</footer>

<!-- Grid Wrap Asset //-->
<div id="gridWrap"><img src="/assets/images/grid.png" /></div>


<!--[if lt IE 8 ]><style type="text/css">.history{display: none;}</style><![endif]-->
<link rel="stylesheet" href="/assets/css/fonts.css" type="text/css" media="screen" />
</body>
</html>"""

bad_html = """<!doctype html><html itemscope="itemscope" itemtype="http://schema.org/WebPage"><head><meta content="Search the world's information, including webpages, images, videos and more. Google has many special features to help you find exactly what you're looking for." name="description"><meta content="noodp" name="robots"><meta itemprop="image" content="/images/google_favicon_128.png"><meta id="mref" name="referrer" content="origin"><title>Google</title><script>(function(){
window.google={kEI:"3wXyUfrWMpPK4AOaqIHgCA",getEI:function(a){for(var b;a&&(!a.getAttribute||!(b=a.getAttribute("eid")));)a=a.parentNode;return b||google.kEI},https:function(){return"https:"==window.location.protocol},kEXPI:"31215,4000116,4004334,4004844,4004949,4004953,4005875,4006037,4006263,4006426,4006442,4006602,4006727,4007055,4007080,4007117,4007232,4007463,4007638,4007661,4007688,4007779,4007862,4007874,4007902,4007928,4008041,4008067,4008115,4008133,4008142,4008170,4008184,4008191,4008209,4008269,4008298,4008314,4008379,4008396,4008423,4008431,4008487,4008488,4008502,4008595,4008610,4008668,4008726,4008790,4008799",kCSI:{e:"31215,4000116,4004334,4004844,4004949,4004953,4005875,4006037,4006263,4006426,4006442,4006602,4006727,4007055,4007080,4007117,4007232,4007463,4007638,4007661,4007688,4007779,4007862,4007874,4007902,4007928,4008041,4008067,4008115,4008133,4008142,4008170,4008184,4008191,4008209,4008269,4008298,4008314,4008379,4008396,4008423,4008431,4008487,4008488,4008502,4008595,4008610,4008668,4008726,4008790,4008799",ei:"3wXyUfrWMpPK4AOaqIHgCA"},authuser:0,ml:function(){},kHL:"en",time:function(){return(new Date).getTime()},log:function(a,b,c,l,k){var d=new Image,f=google.lc,e=google.li,g="",h="gen_204";k&&(h=
k);d.onerror=d.onload=d.onabort=function(){delete f[e]};f[e]=d;c||-1!=b.search("&ei=")||(g="&ei="+google.getEI(l));c=c||"/"+h+"?atyp=i&ct="+a+"&cad="+b+g+"&zx="+google.time();
a=/^http:/i;a.test(c)&&google.https()?(google.ml(Error("GLMM"),!1,{src:c}),delete f[e]):(d.src=c,google.li=e+1)},lc:[],li:0,j:{en:1,b:!!location.hash&&!!location.hash.match("[#&]((q|fp)=|tbs=simg|tbs=sbi)"),bv:21,cf:"",pm:"p",u:"5901c05d"},Toolbelt:{},y:{},x:function(a,b){google.y[a.id]=[a,b];return!1},load:function(a,b,c){google.x({id:a+m++},function(){google.load(a,b,c)})}};var m=0;window.onpopstate=function(){google.j.psc=1};
window.chrome||(window.chrome={});window.chrome.sv=2.00;window.chrome.searchBox||(window.chrome.searchBox={});var n=function(){google.x({id:"psyapi"},function(){var a=encodeURIComponent(window.chrome.searchBox.value);google.nav.search({q:a,sourceid:"chrome-psyapi2"})})};window.chrome.searchBox.onsubmit=n;})();
(function(){google.sn="webhp";google.timers={};google.startTick=function(a,b){google.timers[a]={t:{start:google.time()},bfr:!!b}};google.tick=function(a,b,g){google.timers[a]||google.startTick(a);google.timers[a].t[b]=g||google.time()};google.startTick("load",!0);
try{google.pt=window.chrome&&window.chrome.csi&&Math.floor(window.chrome.csi().pageT);}catch(d){}})();
(function(){'use strict';var c=this,g=Date.now||function(){return+new Date};var m=function(d,k){return function(a){a||(a=window.event);return k.call(d,a)}},t="undefined"!=typeof navigator&&/Macintosh/.test(navigator.userAgent),u="undefined"!=typeof navigator&&!/Opera/.test(navigator.userAgent)&&/WebKit/.test(navigator.userAgent),v="undefined"!=typeof navigator&&!/Opera|WebKit/.test(navigator.userAgent)&&/Gecko/.test(navigator.product),x=v?"keypress":"keydown";var y=function(){this.g=[];this.a=[];this.e={};this.d=null;this.c=[]},z="undefined"!=typeof navigator&&/iPhone|iPad|iPod/.test(navigator.userAgent),A=/\s*;\s*/,B=function(d,k){return function(a){var b;i:{b=k;if("click"==b&&(t&&a.metaKey||!t&&a.ctrlKey||2==a.which||null==a.which&&4==a.button||a.shiftKey))b="clickmod";else{var e=a.which||a.keyCode||a.key,f;if(f=a.type==x){f=a.srcElement||a.target;var n=f.tagName.toUpperCase();f=!("TEXTAREA"==n||"BUTTON"==n||"INPUT"==n||"A"==n||f.isContentEditable)&&!(a.ctrlKey||a.shiftKey||a.altKey||a.metaKey)&&(13==e||32==e||u&&3==e)}f&&
(b="clickkey")}for(f=e=a.srcElement||a.target;f&&f!=this;f=f.parentNode){var n=f,l;var h=n;l=b;var p=h.__jsaction;if(!p){p={};h.__jsaction=p;var r=null;"getAttribute"in h&&(r=h.getAttribute("jsaction"));if(h=r)for(var h=h.split(A),r=0,P=h?h.length:0;r<P;r++){var q=h[r];if(q){var w=q.indexOf(":"),H=-1!=w,Q=H?q.substr(0,w).replace(/^\s+/,"").replace(/\s+$/,""):"click",q=H?q.substr(w+1).replace(/^\s+/,"").replace(/\s+$/,""):q;p[Q]=q}}}h=void 0;"clickkey"==l?l="click":"click"==l&&(h=p.click||p.clickonly);l=(h=h||p[l])?{h:l,action:h}:void 0;if(l){b={eventType:l.h,event:a,targetElement:e,action:l.action,actionElement:n};break i}}b=null}if(b)if("A"==b.actionElement.tagName&&"click"==k&&(a.preventDefault?a.preventDefault():a.returnValue=!1),d.d)d.d(b);else{var s;if((e=c.document)&&!e.createEvent&&e.createEventObject)try{s=e.createEventObject(a)}catch(U){s=a}else s=a;v&&(s.timeStamp=g());b.event=s;d.c.push(b)}}},C=function(d,k){return function(a){var b=d,e=k,f=!1;if(a.addEventListener){if("focus"==b||"blur"==b)f=!0;a.addEventListener(b,e,f)}else a.attachEvent&&("focus"==b?b="focusin":"blur"==b&&(b="focusout"),e=m(a,e),a.attachEvent("on"+b,e));return{h:b,i:e,capture:f}}},D=function(d,k){if(!d.e.hasOwnProperty(k)){var a=B(d,k),b=C(k,a);d.e[k]=a;d.g.push(b);for(a=0;a<d.a.length;++a){var e=d.a[a];e.c.push(b.call(null,e.a))}"click"==k&&D(d,x)}};y.prototype.i=function(d){return this.e[d]};var F=function(){this.a=E;this.c=[]};var G=new y,E=window.document.documentElement,I;i:{for(var J=0;J<G.a.length;J++){for(var K=G.a[J].a,L=E;K!=L&&L.parentNode;)L=L.parentNode;if(K==L){I=!0;break i}}I=!1}if(!I){z&&(E.style.cursor="pointer");for(var M=new F,N=0;N<G.g.length;++N)M.c.push(G.g[N].call(null,M.a));G.a.push(M)}D(G,"click");D(G,"focus");D(G,"focusin");D(G,"blur");D(G,"focusout");D(G,"change");D(G,"keydown");D(G,"keypress");D(G,"mousedown");D(G,"mouseout");D(G,"mouseover");D(G,"mouseup");D(G,"touchstart");D(G,"touchmove");D(G,"touchend");var O=function(d){G.d=d;G.c&&(0<G.c.length&&d(G.c),G.c=null)},R=["google","jsad"],S=c;R[0]in S||!S.execScript||S.execScript("var "+R[0]);for(var T;R.length&&(T=R.shift());)R.length||void 0===O?S=S[T]?S[T]:S[T]={}:S[T]=O;}).call(window);google.arwt=function(a){a.href=document.getElementById(a.id.substring(1)).href;return!0};</script><style>#gb{font:13px/27px Arial,sans-serif;height:102px}#gbz,#gbg{position:absolute;white-space:nowrap;top:0;height:30px;z-index:1000}#gbz{left:0;padding-left:4px}#gbg{right:0;padding-right:5px}#gbs{background:transparent;position:absolute;top:-999px;visibility:hidden;z-index:998;right:0}.gbto #gbs{background:#fff}#gbx3,#gbx4{background-color:#2d2d2d;background-image:none;_background-image:none;background-position:0 -138px;background-repeat:repeat-x;border-bottom:1px solid #000;font-size:24px;height:29px;_height:30px;opacity:1;filter:alpha(opacity=100);position:absolute;top:0;width:100%;z-index:990}#gbx3{left:0}#gbx4{right:0}#gbb{position:relative}#gbbw{left:0;position:absolute;top:102px;width:100%}.gbtcb{position:absolute;visibility:hidden}#gbz .gbtcb{right:0}#gbg .gbtcb{left:0}.gbxx{display:none !important}.gbxo{opacity:0 !important;filter:alpha(opacity=0) !important}.gbm{position:absolute;z-index:999;top:-999px;visibility:hidden;text-align:left;border:1px solid #bebebe;background:#fff;-moz-box-shadow:-1px 1px 1px rgba(0,0,0,.2);-webkit-box-shadow:0 2px 4px rgba(0,0,0,.2);box-shadow:0 2px 4px rgba(0,0,0,.2)}.gbrtl .gbm{-moz-box-shadow:1px 1px 1px rgba(0,0,0,.2)}.gbto .gbm,.gbto #gbs{top:51px;visibility:visible}#gbz .gbm{left:0}#gbg .gbm{right:0}.gbxms{background-color:#ccc;display:block;position:absolute;z-index:1;top:-1px;left:-2px;right:-2px;bottom:-2px;opacity:.4;-moz-border-radius:3px;filter:progid:DXImageTransform.Microsoft.Blur(pixelradius=5);*opacity:1;*top:-2px;*left:-5px;*right:5px;*bottom:4px;-ms-filter:"progid:DXImageTransform.Microsoft.Blur(pixelradius=5)";opacity:1\0/;top:-4px\0/;left:-6px\0/;right:5px\0/;bottom:4px\0/}.gbma{position:relative;top:-1px;border-style:solid dashed dashed;border-color:transparent;border-top-color:#c0c0c0;display:-moz-inline-box;display:inline-block;font-size:0;height:0;line-height:0;width:0;border-width:3px 3px 0;padding-top:1px;left:4px}#gbztms1,#gbi4m1,#gbi4s,#gbi4t{zoom:1}.gbtc,.gbmc,.gbmcc{display:block;list-style:none;margin:0;padding:0}.gbmc{background:#fff;padding:10px 0;position:relative;z-index:2;zoom:1}.gbt{position:relative;display:-moz-inline-box;display:inline-block;line-height:27px;padding:0;vertical-align:top}.gbt{*display:inline}.gbto{box-shadow:0 2px 4px rgba(0,0,0,.2);-moz-box-shadow:0 2px 4px rgba(0,0,0,.2);-webkit-box-shadow:0 2px 4px rgba(0,0,0,.2)}.gbzt,.gbgt{cursor:pointer;display:block;text-decoration:none !important}span#gbg6,span#gbg4{cursor:default}.gbts{border-left:1px solid transparent;border-right:1px solid transparent;display:block;*display:inline-block;padding:0 5px;position:relative;z-index:1000}.gbts{*display:inline}.gbzt .gbts{display:inline;zoom:1}.gbto .gbts{background:#fff;border-color:#bebebe;color:#36c;padding-bottom:1px;padding-top:2px}.gbz0l .gbts{color:#fff;font-weight:bold}.gbtsa{padding-right:9px}#gbz .gbzt,#gbz .gbgt,#gbg .gbgt{color:#ccc!important}.gbtb2{display:block;border-top:2px solid transparent}.gbto .gbzt .gbtb2,.gbto .gbgt .gbtb2{border-top-width:0}.gbtb .gbts{background:url(//ssl.gstatic.com/gb/images/h_bedf916a.png);_background:url(//ssl.gstatic.com/gb/images/h8_3dd87cd8.png);background-position:-27px -22px;border:0;font-size:0;padding:29px 0 0;*padding:27px 0 0;width:1px}.gbzt:hover,.gbzt:focus,.gbgt-hvr,.gbgt:focus{background-color:transparent;background-image:none;_background-image:none;background-position:0 -102px;background-repeat:repeat-x;outline:none;text-decoration:none !important}.gbpdjs .gbto .gbm{min-width:99%}.gbz0l .gbtb2{border-top-color:transparent!important}#gbi4s,#gbi4s1{font-weight:bold}#gbg6.gbgt-hvr,#gbg6.gbgt:focus{background-color:transparent;background-image:none}.gbg4a{font-size:0;line-height:0}.gbg4a .gbts{padding:27px 5px 0;*padding:25px 5px 0}.gbto .gbg4a .gbts{padding:29px 5px 1px;*padding:27px 5px 1px}#gbi4i,#gbi4id{left:5px;border:0;height:24px;position:absolute;top:1px;width:24px}.gbto #gbi4i,.gbto #gbi4id{top:3px}.gbi4p{display:block;width:24px}#gbi4id{background-position:-44px -101px}#gbmpid{background-position:0 0}#gbmpi,#gbmpid{border:none;display:inline-block;height:48px;width:48px}#gbmpiw{display:inline-block;line-height:9px;padding-left:20px;margin-top:10px;position:relative}#gbmpi,#gbmpid,#gbmpiw{*display:inline}#gbg5{font-size:0}#gbgs5{padding:5px !important}.gbto #gbgs5{padding:7px 5px 6px !important}#gbi5{background:url(//ssl.gstatic.com/gb/images/h_bedf916a.png);_background:url(//ssl.gstatic.com/gb/images/h8_3dd87cd8.png);background-position:0 0;display:block;font-size:0;height:17px;width:16px}.gbto #gbi5{background-position:-6px -22px}.gbn .gbmt,.gbn .gbmt:visited,.gbnd .gbmt,.gbnd .gbmt:visited{color:#dd8e27 !important}.gbf .gbmt,.gbf .gbmt:visited{color:#900 !important}.gbmt,.gbml1,.gbmlb,.gbmt:visited,.gbml1:visited,.gbmlb:visited{color:#36c !important;text-decoration:none !important}.gbmt,.gbmt:visited{display:block}.gbml1,.gbmlb,.gbml1:visited,.gbmlb:visited{display:inline-block;margin:0 10px}.gbml1,.gbmlb,.gbml1:visited,.gbmlb:visited{*display:inline}.gbml1,.gbml1:visited{padding:0 10px}.gbml1-hvr,.gbml1:focus{outline:none;text-decoration:underline !important}#gbpm .gbml1{display:inline;margin:0;padding:0;white-space:nowrap}.gbmlb,.gbmlb:visited{line-height:27px}.gbmlb-hvr,.gbmlb:focus{outline:none;text-decoration:underline !important}.gbmlbw{color:#ccc;margin:0 10px}.gbmt{padding:0 20px}.gbmt:hover,.gbmt:focus{background:#eee;cursor:pointer;outline:0 solid black;text-decoration:none !important}.gbm0l,.gbm0l:visited{color:#000 !important;font-weight:bold}.gbmh{border-top:1px solid #bebebe;font-size:0;margin:10px 0}#gbd4 .gbmc{background:#f5f5f5;padding-top:0}#gbd4 .gbsbic::-webkit-scrollbar-track:vertical{background-color:#f5f5f5;margin-top:2px}#gbmpdv{background:#fff;border-bottom:1px solid #bebebe;-moz-box-shadow:0 2px 4px rgba(0,0,0,.12);-o-box-shadow:0 2px 4px rgba(0,0,0,.12);-webkit-box-shadow:0 2px 4px rgba(0,0,0,.12);box-shadow:0 2px 4px rgba(0,0,0,.12);position:relative;z-index:1}#gbd4 .gbmh{margin:0}.gbmtc{padding:0;margin:0;line-height:27px}.GBMCC:last-child:after,#GBMPAL:last-child:after{content:'\0A\0A';white-space:pre;position:absolute}#gbmps{*zoom:1}#gbd4 .gbpc,#gbmpas .gbmt{line-height:17px}#gbd4 .gbpgs .gbmtc{line-height:27px}#gbd4 .gbmtc{border-bottom:1px solid #bebebe}#gbd4 .gbpc{display:inline-block;margin:16px 0 10px;padding-right:50px;vertical-align:top}#gbd4 .gbpc{*display:inline}.gbpc .gbps,.gbpc .gbps2{display:block;margin:0 20px}#gbmplp.gbps{margin:0 10px}.gbpc .gbps{color:#000;font-weight:bold}.gbpc .gbpd{margin-bottom:5px}.gbpd .gbmt,.gbpd .gbps{color:#666 !important}.gbpd .gbmt{opacity:.4;filter:alpha(opacity=40)}.gbps2{color:#666;display:block}.gbp0{display:none}.gbp0 .gbps2{font-weight:bold}#gbd4 .gbmcc{margin-top:5px}.gbpmc{background:#fef9db}.gbpmc .gbpmtc{padding:10px 20px}#gbpm{border:0;*border-collapse:collapse;border-spacing:0;margin:0;white-space:normal}#gbpm .gbpmtc{border-top:none;color:#000 !important;font:11px Arial,sans-serif}#gbpms{*white-space:nowrap}.gbpms2{font-weight:bold;white-space:nowrap}#gbmpal{*border-collapse:collapse;border-spacing:0;border:0;margin:0;white-space:nowrap;width:100%}.gbmpala,.gbmpalb{font:13px Arial,sans-serif;line-height:27px;padding:10px 20px 0;white-space:nowrap}.gbmpala{padding-left:0;text-align:left}.gbmpalb{padding-right:0;text-align:right}#gbmpasb .gbps{color:#000}#gbmpal .gbqfbb{margin:0 20px}.gbp0 .gbps{*display:inline}a.gbiba{margin:8px 20px 10px}.gbmpiaw{display:inline-block;padding-right:10px;margin-bottom:6px;margin-top:10px}.gbxv{visibility:hidden}.gbmpiaa{display:block;margin-top:10px}.gbmpia{border:none;display:block;height:48px;width:48px}.gbmpnw{display:inline-block;height:auto;margin:16px 0 18px;vertical-align:top}.gbqfb,.gbqfba,.gbqfbb{-moz-border-radius:2px;-webkit-border-radius:2px;border-radius:2px;cursor:default !important;display:inline-block;font-weight:bold;height:29px;line-height:29px;min-width:54px;*min-width:70px;padding:0 8px;text-align:center;text-decoration:none !important;-moz-user-select:none;-webkit-user-select:none}.gbqfb:focus,.gbqfba:focus,.gbqfbb:focus{border:1px solid #4d90fe;-moz-box-shadow:inset 0 0 0 1px rgba(255, 255, 255, 0.5);-webkit-box-shadow:inset 0 0 0 1px rgba(255, 255, 255, 0.5);box-shadow:inset 0 0 0 1px rgba(255, 255, 255, 0.5);outline:none}.gbqfb-hvr:focus,.gbqfba-hvr:focus,.gbqfbb-hvr:focus{-webkit-box-shadow:inset 0 0 0 1px #fff,0 1px 1px rgba(0,0,0,.1);-moz-box-shadow:inset 0 0 0 1px #fff,0 1px 1px rgba(0,0,0,.1);box-shadow:inset 0 0 0 1px #fff,0 1px 1px rgba(0,0,0,.1)}.gbqfb-no-focus:focus{border:1px solid #3079ed;-moz-box-shadow:none;-webkit-box-shadow:none;box-shadow:none}.gbqfb-hvr,.gbqfba-hvr,.gbqfbb-hvr{-webkit-box-shadow:0 1px 1px rgba(0,0,0,.1);-moz-box-shadow:0 1px 1px rgba(0,0,0,.1);box-shadow:0 1px 1px rgba(0,0,0,.1)}.gbqfb::-moz-focus-inner,.gbqfba::-moz-focus-inner,.gbqfbb::-moz-focus-inner{border:0}.gbqfba,.gbqfbb{border:1px solid #dcdcdc;border-color:rgba(0,0,0,.1);color:#444 !important;font-size:11px}.gbqfb{background-color:#4d90fe;background-image:-webkit-gradient(linear,left top,left bottom,from(#4d90fe),to(#4787ed));background-image:-webkit-linear-gradient(top,#4d90fe,#4787ed);background-image:-moz-linear-gradient(top,#4d90fe,#4787ed);background-image:-ms-linear-gradient(top,#4d90fe,#4787ed);background-image:-o-linear-gradient(top,#4d90fe,#4787ed);background-image:linear-gradient(top,#4d90fe,#4787ed);filter:progid:DXImageTransform.Microsoft.gradient(startColorStr='#4d90fe',EndColorStr='#4787ed');border:1px solid #3079ed;color:#fff!important;margin:0 0}.gbqfb-hvr{border-color:#2f5bb7}.gbqfb-hvr:focus{border-color:#2f5bb7}.gbqfb-hvr,.gbqfb-hvr:focus{background-color:#357ae8;background-image:-webkit-gradient(linear,left top,left bottom,from(#4d90fe),to(#357ae8));background-image:-webkit-linear-gradient(top,#4d90fe,#357ae8);background-image:-moz-linear-gradient(top,#4d90fe,#357ae8);background-image:-ms-linear-gradient(top,#4d90fe,#357ae8);background-image:-o-linear-gradient(top,#4d90fe,#357ae8);background-image:linear-gradient(top,#4d90fe,#357ae8)}.gbqfb:active{background-color:inherit;-webkit-box-shadow:inset 0 1px 2px rgba(0, 0, 0, 0.3);-moz-box-shadow:inset 0 1px 2px rgba(0, 0, 0, 0.3);box-shadow:inset 0 1px 2px rgba(0, 0, 0, 0.3)}.gbqfba{background-color:#f5f5f5;background-image:-webkit-gradient(linear,left top,left bottom,from(#f5f5f5),to(#f1f1f1));background-image:-webkit-linear-gradient(top,#f5f5f5,#f1f1f1);background-image:-moz-linear-gradient(top,#f5f5f5,#f1f1f1);background-image:-ms-linear-gradient(top,#f5f5f5,#f1f1f1);background-image:-o-linear-gradient(top,#f5f5f5,#f1f1f1);background-image:linear-gradient(top,#f5f5f5,#f1f1f1);filter:progid:DXImageTransform.Microsoft.gradient(startColorStr='#f5f5f5',EndColorStr='#f1f1f1')}.gbqfba-hvr,.gbqfba-hvr:active{background-color:#f8f8f8;background-image:-webkit-gradient(linear,left top,left bottom,from(#f8f8f8),to(#f1f1f1));background-image:-webkit-linear-gradient(top,#f8f8f8,#f1f1f1);background-image:-moz-linear-gradient(top,#f8f8f8,#f1f1f1);background-image:-ms-linear-gradient(top,#f8f8f8,#f1f1f1);background-image:-o-linear-gradient(top,#f8f8f8,#f1f1f1);background-image:linear-gradient(top,#f8f8f8,#f1f1f1);filter:progid:DXImageTransform.Microsoft.gradient(startColorStr='#f8f8f8',EndColorStr='#f1f1f1')}.gbqfbb{background-color:#fff;background-image:-webkit-gradient(linear,left top,left bottom,from(#fff),to(#fbfbfb));background-image:-webkit-linear-gradient(top,#fff,#fbfbfb);background-image:-moz-linear-gradient(top,#fff,#fbfbfb);background-image:-ms-linear-gradient(top,#fff,#fbfbfb);background-image:-o-linear-gradient(top,#fff,#fbfbfb);background-image:linear-gradient(top,#fff,#fbfbfb);filter:progid:DXImageTransform.Microsoft.gradient(startColorStr='#ffffff',EndColorStr='#fbfbfb')}.gbqfbb-hvr,.gbqfbb-hvr:active{background-color:#fff;background-image:-webkit-gradient(linear,left top,left bottom,from(#fff),to(#f8f8f8));background-image:-webkit-linear-gradient(top,#fff,#f8f8f8);background-image:-moz-linear-gradient(top,#fff,#f8f8f8);background-image:-ms-linear-gradient(top,#fff,#f8f8f8);background-image:-o-linear-gradient(top,#fff,#f8f8f8);background-image:linear-gradient(top,#fff,#f8f8f8);filter:progid:DXImageTransform.Microsoft.gradient(startColorStr='#ffffff',EndColorStr='#f8f8f8')}.gbqfba-hvr,.gbqfba-hvr:active,.gbqfbb-hvr,.gbqfbb-hvr:active{border-color:#c6c6c6;-webkit-box-shadow:0 1px 1px rgba(0,0,0,.1);-moz-box-shadow:0 1px 1px rgba(0,0,0,.1);box-shadow:0 1px 1px rgba(0,0,0,.1);color:#222 !important}.gbqfba:active,.gbqfbb:active{-webkit-box-shadow:inset 0 1px 2px rgba(0,0,0,.1);-moz-box-shadow:inset 0 1px 2px rgba(0,0,0,.1);box-shadow:inset 0 1px 2px rgba(0,0,0,.1)}#gbql,#gbgsi,#gbgsa,.gbqfi,.gbmai,.gbp0i,.gbmppci,.gbqfh #gbql{background-image:url('//ssl.gstatic.com/gb/images/k1_a31af7ac.png');background-size:294px 45px}@media screen and (min-resolution:1.25dppx),screen and (-o-min-device-pixel-ratio:5/4),screen and (-webkit-min-device-pixel-ratio:1.25),screen and (min-device-pixel-ratio:1.25){#gbql,#gbgsi,#gbgsa,.gbqfi,.gbmai,.gbp0i,.gbmppci,.gbqfh #gbql{background-image:url('//ssl.gstatic.com/gb/images/k2_aca6bcc6.png')}}#gbd1,#gbd1 .gbmc{width:440px;height:190px}#gbd3,#gbd3 .gbmc{width:440px;height:8em}#gb{height:102px;-moz-user-select:-moz-none;-o-user-select:none;-webkit-user-select:none;user-select:none}#gbbw{top:102px;min-width:980px;}#gb.gbet #gbbw,#gb.gbeti #gbbw{min-width:836px;}#gb.gbeu #gbbw,#gb.gbeui #gbbw{min-width:780px;}.gbxx{display:none !important}#gbq,#gbu{position:absolute;top:0px;white-space:nowrap}#gbu{height:71px}#gbu,#gbq1,#gbq3{z-index:987}#gbq{left:0;_overflow:hidden;width:100%;z-index:986}#gbq2{top:0px;z-index:986}#gbu{right:0;height:30px;margin-right:28px;padding-bottom:0;padding-top:20px}#gbx1,#gbx2{background:#f1f1f1;background:-webkit-gradient(radial,100 36,0,100 -40,120,from(#fafafa),to(#f1f1f1)),#f1f1f1;border-bottom:1px solid #666;border-color:#e5e5e5;height:71px;position:absolute;top:0px;width:100%;z-index:985;min-width:980px;}#gb.gbet #gbx1,#gb.gbeti #gbx1{min-width:836px;}#gb.gbeu #gbx1,#gb.gbeui #gbx1{min-width:780px;}#gbx1.gbxngh,#gbx2.gbxngh{background:-webkit-gradient(radial,100 36,0,100 -40,120,from(#ffffff),to(#f1f1f1)),#f1f1f1}#gbx1{left:0}#gbx2{right:0}#gbq1{left:0;margin:0;padding:0;margin-left:16px;position:absolute}.gbes#gbq1{margin-left:0}#gbq3{left:126px;padding-bottom:0;padding-top:20px;position:absolute;top:0px}#gbql{background-repeat:no-repeat;background-position:-63px 0;display:block;height:37px;width:95px}.gbqla{display:inline-block;outline:none;position:relative}.gbqla2{outline:none}.gbqlca{cursor:pointer;cursor:hand;height:100%;position:absolute;top:0;width:100%;left:0}#gbqlt{border:0;border-collapse:collapse;border-spacing:0;margin:0}#gbqlw{display:table-cell;height:71px;padding:0;padding-right:16px;position:relative;vertical-align:middle}#gbqld{border:none;display:block}.gbqldr{max-height:71px;max-width:160px}#gog{height:99px}.gbh{border-top:none}.gbpl,.gbpr,#gbpx1,#gbpx2{border-top:none !important;top:102px !important}.gbpl,.gbpr{margin-top:4px}.gbi5t{color:#666;display:block;margin:1px 15px;text-shadow:none}#gbq2{display:block;margin-left:126px;padding-bottom:0;padding-top:20px}#gbqf{display:block;margin:0;max-width:572px;min-width:572px;white-space:nowrap}.gbexxl#gbq2 #gbqf,.gbexxli#gb #gbqf,.gbexl#gbq2 #gbqf,.gbexli#gb #gbqf{max-width:572px}.gbet#gbq2 #gbqf,.gbeti#gb #gbqf{max-width:434px;min-width:434px}.gbeu#gbqf,.gbeui#gb #gbqf{max-width:319px;min-width:319px}.gbqff{border:none;display:inline-block;margin:0;padding:0;vertical-align:top;width:100%}.gbqff{*display:inline}.gbqfqw,#gbqfb,.gbqfwa{vertical-align:top}#gbqfaa,#gbqfab,#gbqfqwb{position:absolute}#gbqfaa{left:0}#gbqfab{right:0}.gbqfqwb,.gbqfqwc{right:0;left:0}.gbqfqwb{padding:0 8px}#gbqfbw{margin:0 15px;display:inline-block;vertical-align:top}#gbqfbw{*display:inline}.gbqfi{background-position:-33px 0;display:inline-block;height:13px;margin:7px 19px;width:14px}.gbqfi{*display:inline}.gbqfqw{background:#fff;border:1px solid #d9d9d9;border-top:1px solid #c0c0c0;-moz-border-radius:1px;-webkit-border-radius:1px;border-radius:1px;height:27px;}#gbqfqw{position:relative}.gbqfqw-hvr{border:1px solid #b9b9b9;border-top:1px solid #a0a0a0;-moz-box-shadow:inset 0 1px 2px rgba(0,0,0,.1);-webkit-box-shadow:inset 0 1px 2px rgba(0,0,0,.1);box-shadow:inset 0 1px 2px rgba(0,0,0,.1)}.gbqfwa{display:inline-block;width:100%}.gbqfwa{*display:inline}.gbqfwb{width:40%}.gbqfwc{width:60%}.gbqfwb .gbqfqw{margin-left:10px}.gbqfqw:active,.gbqfqwf{border:1px solid #4d90fe;-moz-box-shadow:inset 0 1px 2px rgba(0,0,0,.3);-webkit-box-shadow:inset 0 1px 2px rgba(0,0,0,.3);box-shadow:inset 0 1px 2px rgba(0,0,0,.3);outline:none}#gbqfq,#gbqfqb,#gbqfqc{background:transparent;border:none;height:19px;margin-top:4px;padding:0;vertical-align:top;width:100%}#gbqfq:focus,#gbqfqb:focus,#gbqfqc:focus{outline:none}.gbqfif,.gbqfsf{font:16px arial,sans-serif}#gbqfbwa{display:none;text-align:center;height:0}#gbqfbwa .gbqfba{margin:16px 8px}#gbqfsa,#gbqfsb{font:bold 11px/27px Arial,sans-serif !important;vertical-align:top}.gbqfh #gbql{background-repeat:no-repeat;display:block;margin-bottom:21px;margin-top:25px}.gbqfh#gbpr .gbqpa{width:71px}.gbqfh .gbes#gbql,.gbesi#gb .gbqfh #gbql{margin-bottom:14px;margin-top:18px}.gbqfh#gbq2{z-index:985}.gbqfh#gbq2{margin:0;margin-left:0 !important;padding-top:0;top:281px}.gbqfh #gbqf{margin:auto;min-width:534px;padding:0 223px !important}.gbqfh #gbqfbw{display:none}.gbqfh #gbqfbwa{display:block}.gbqfh#gbq2{padding-top:0;top:281px}.gbem .gbqfh#gbq2,.gbemi#gb .gbqfh#gbq2{padding-top:0;top:281px}.gbes .gbqfh#gbq2,.gbesi#gb .gbqfh#gbq2{padding-top:0;top:281px}#gbu .gbm,#gbu #gbs{right:5px}.gbpdjs #gbu .gbm,.gbpdjs #gbu #gbs{right:0}.gbpdjs #gbu #gbd4{right:5px}#gbu .gbgt,#gbu .gbgt:active{color:#666}#gbu .gbt{margin-left:15px}#gbu .gbto{box-shadow:none;-moz-box-shadow:none;-webkit-box-shadow:none}#gbg4{padding-right:16px}#gbd1 .gbmc,#gbd3 .gbmc{padding:0}#gbns{display:none}.gbmwc{right:0;position:absolute;top:-999px;width:440px;z-index:999}#gbwc.gbmwca{top:0}.gbmsg{display:none}.gbmab,.gbmac,.gbmad,.gbmae{left:5px;border-style:dashed dashed solid;border-color:transparent;border-bottom-color:#bebebe;border-width:0 10px 10px;cursor:default;display:-moz-inline-box;display:inline-block;font-size:0;height:0;line-height:0;position:absolute;top:0;width:0;z-index:1000}.gbmab,.gbmac{visibility:hidden}.gbmac{border-bottom-color:#fff}.gbto .gbmab,.gbto .gbmac{visibility:visible}.gbmai{background-position:-163px -40px;opacity:.8;font-size:0;line-height:0;position:absolute;height:4px;width:7px}.gbgt-hvr .gbmai{opacity:1;filter:alpha(opacity=100)}#gbgs3{background-color:#f8f8f8;background-image:-webkit-gradient(linear,left top,left bottom,from(#f8f8f8),to(#ececec));background-image:-webkit-linear-gradient(top,#f8f8f8,#ececec);background-image:-moz-linear-gradient(top,#f8f8f8,#ececec);background-image:-ms-linear-gradient(top,#f8f8f8,#ececec);background-image:-o-linear-gradient(top,#f8f8f8,#ececec);background-image:linear-gradient(top,#f8f8f8,#ececec);filter:progid:DXImageTransform.Microsoft.gradient(startColorStr='#f8f8f8',EndColorStr='#ececec');border:1px solid #c6c6c6;-moz-border-radius:2px;-o-border-radius:2px;-webkit-border-radius:2px;border-radius:2px;padding:0 10px;position:relative}#gbgsi{background-position:-163px 0;height:10px;opacity:.8;position:absolute;top:8px;_top:10px;width:10px;left:10px}#gbgsa{background-position:-163px -15px;height:11px;position:absolute;top:8px;width:10px;left:100%}.gbgt-hvr #gbgsa{background-position:-18px -32px}#gbg3:active #gbgsa{background-position:0 0}.gbgt-hvr #gbgsi{opacity:1;filter:alpha(opacity=100)}#gbgss{display:inline-block;width:18px}.gbsbc #gbgss{width:7px}#gbi3{zoom:1}.gbsbc #gbi3{display:none}.gbgt-hvr #gbgs3,#gbg3:focus #gbgs3,#gbg3:active #gbgs3{background-color:#ffffff;background-image:-webkit-gradient(linear,left top,left bottom,from(#ffffff),to(#ececec));background-image:-webkit-linear-gradient(top,#ffffff,#ececec);background-image:-moz-linear-gradient(top,#ffffff,#ececec);background-image:-ms-linear-gradient(top,#ffffff,#ececec);background-image:-o-linear-gradient(top,#ffffff,#ececec);background-image:linear-gradient(top,#ffffff,#ececec);filter:progid:DXImageTransform.Microsoft.gradient(startColorStr='#ffffff',EndColorStr='#ececec');border-color:#bbb}#gbg3:active #gbgs3{border-color:#b6b6b6}#gbg3:active #gbgs3{-moz-box-shadow:inset 0 1px 2px rgba(0,0,0,.2);-webkit-box-shadow:inset 0 1px 2px rgba(0,0,0,.2);box-shadow:inset 0 1px 2px rgba(0,0,0,.2)}#gbgs3 .gbmab{margin:40px 0 0}#gbgs3 .gbmac{margin:41px 0 0}.gbsr #gbgs3 .gbmac{border-bottom-color:#f5f5f5}.gbsr #gbd3,.gbsr #gbd3 .gbmc{background:#f5f5f5;min-height:268px;width:480px}#gbgs1{display:block;overflow:hidden;position:relative}.gbg1t{top:0}.gbg1ta{-o-transition:top .218s ease-out;-moz-transition:top .218s ease-out;-webkit-transition:top .218s ease-out;transition:top .218s ease-out}.gbg1tb{-o-transition:top .13s ease-in;-moz-transition:top .13s ease-in;-webkit-transition:top .13s ease-in;transition:top .13s ease-in}.gbg1tc{-o-transition:top .13s ease-out;-moz-transition:top .13s ease-out;-webkit-transition:top .13s ease-out;transition:top .13s ease-out}#gbi1a{background-color:#d14836;background-image:-webkit-gradient(linear,left top,left bottom,from(#dd4b39),to(#d14836));background-image:-webkit-linear-gradient(top,#dd4b39,#d14836);background-image:-moz-linear-gradient(top,#dd4b39,#d14836);background-image:-ms-linear-gradient(top,#dd4b39,#d14836);background-image:-o-linear-gradient(top,#dd4b39,#d14836);background-image:linear-gradient(top,#dd4b39,#d14836);filter:progid:DXImageTransform.Microsoft.gradient(startColorStr='#dd4b39',EndColorStr='#d14836');border:1px solid #c13828;-moz-border-radius:2px;-o-border-radius:2px;-webkit-border-radius:2px;border-radius:2px;display:block;height:27px;width:27px}.gbgt-hvr #gbi1a{background-color:#c53727;background-image:-webkit-gradient(linear,left top,left bottom,from(#dd4b39),to(#c53727));background-image:-webkit-linear-gradient(top,#dd4b39,#c53727);background-image:-moz-linear-gradient(top,#dd4b39,#c53727);background-image:-ms-linear-gradient(top,#dd4b39,#c53727);background-image:-o-linear-gradient(top,#dd4b39,#c53727);background-image:linear-gradient(top,#dd4b39,#c53727);filter:progid:DXImageTransform.Microsoft.gradient(startColorStr='#dd4b39',EndColorStr='#c53727');border-color:#b0281a;border-bottom-color:#af301f;-moz-box-shadow:0 1px 1px rgba(0,0,0,.2);-webkit-box-shadow:0 1px 1px rgba(0,0,0,.2);box-shadow:0 1px 1px rgba(0,0,0,.2)}#gbg1:focus #gbi1a,#gbg1:active #gbi1a{background-color:#b0281a;background-image:-webkit-gradient(linear,left top,left bottom,from(#dd4b39),to(#b0281a));background-image:-webkit-linear-gradient(top,#dd4b39,#b0281a);background-image:-moz-linear-gradient(top,#dd4b39,#b0281a);background-image:-ms-linear-gradient(top,#dd4b39,#b0281a);background-image:-o-linear-gradient(top,#dd4b39,#b0281a);background-image:linear-gradient(top,#dd4b39,#b0281a);filter:progid:DXImageTransform.Microsoft.gradient(startColorStr='#dd4b39',EndColorStr='#b0281a');border-color:#992a1b;-moz-box-shadow:inset 0 1px 2px rgba(0,0,0,.3);-webkit-box-shadow:inset 0 1px 2px rgba(0,0,0,.3);box-shadow:inset 0 1px 2px rgba(0,0,0,.3)}.gbid#gbi1a{background-color:#f8f8f8;background-image:-webkit-gradient(linear,left top,left bottom,from(#f8f8f8),to(#ececec));background-image:-webkit-linear-gradient(top,#f8f8f8,#ececec);background-image:-moz-linear-gradient(top,#f8f8f8,#ececec);background-image:-ms-linear-gradient(top,#f8f8f8,#ececec);background-image:-o-linear-gradient(top,#f8f8f8,#ececec);background-image:linear-gradient(top,#f8f8f8,#ececec);filter:progid:DXImageTransform.Microsoft.gradient(startColorStr='#f8f8f8',EndColorStr='#ececec');border-color:#c6c6c6}.gbgt-hvr .gbid#gbi1a,#gbg1:focus .gbid#gbi1a,#gbg1:active .gbid#gbi1a{background-color:#ffffff;background-image:-webkit-gradient(linear,left top,left bottom,from(#ffffff),to(#ececec));background-image:-webkit-linear-gradient(top,#ffffff,#ececec);background-image:-moz-linear-gradient(top,#ffffff,#ececec);background-image:-ms-linear-gradient(top,#ffffff,#ececec);background-image:-o-linear-gradient(top,#ffffff,#ececec);background-image:linear-gradient(top,#ffffff,#ececec);filter:progid:DXImageTransform.Microsoft.gradient(startColorStr='#ffffff',EndColorStr='#ececec');border-color:#bbb}#gbg1:active .gbid#gbi1a{border-color:#b6b6b6;-moz-box-shadow:inset 0 1px 2px rgba(0,0,0,.3);-webkit-box-shadow:inset 0 1px 2px rgba(0,0,0,.3);box-shadow:inset 0 1px 2px rgba(0,0,0,.3)}#gbi1,#gbi1c{left:0;bottom:1px;color:#fff;display:block;font-size:14px;font-weight:bold;position:absolute;text-align:center;text-shadow:0 1px rgba(0,0,0,.1);-moz-transition-property:bottom;-moz-transition-duration:0;-o-transition-property:bottom;-o-transition-duration:0;-webkit-transition-property:bottom;-webkit-transition-duration:0;-moz-user-select:none;-o-user-select:none;-webkit-user-select:none;user-select:none;width:100%}.gbgt-hvr #gbi1,#gbg1:focus #gbi1{text-shadow:0 1px rgba(0,0,0,.3)}.gbids#gbi1,.gbgt-hvr .gbids#gbi1,#gbg1:focus .gbids#gbi1,#gbg1:active .gbids#gbi1{color:#999;text-shadow:none}#gbg1 .gbmab{margin:41px 0 0}#gbg1 .gbmac{margin:42px 0 0}.gb_gbnh #gbgs1,.gbnh #gbgs1,#gbgs1.gbnh{visibility:hidden}.gb_gbnh .gbmab,.gbnh .gbmab,.gb_gbnh .gbmac,.gbnh .gbmac{left:1px}.gbng #gbs,.gbng #gbwc,.gbng #gbg1 .gbmac{border-bottom-color:#e5e5e5}.gbng #gbs,.gbng #gbwc{background-color:#e5e5e5}.gbng #gbd1,.gbng #gbd1 .gbmc,.gbng #gbwc,.gbng #gbs,.gbng #gbsf{width:400px}.gbng .gbmsg{visibility:hidden}.gb_gbsh .gbmab,.gb_gbsh .gbmac{left:9px;border-width:0 6px 6px}.gb_gbsh .gbng .gbmab,.gb_gbsh .gbng .gbmac{left:6px}.gb_gbsh #gbg1 .gbmab,.gb_gbsh #gbg3 .gbmab{margin:31px 0 0}.gb_gbsh #gbg1 .gbmac,.gb_gbsh #gbg3 .gbmac{margin:32px 0 0}.gb_gbsh .gbtn.gbto #gbd1,.gb_gbsh .gbtn.gbto #gbs{-webkit-transform:translateX(-354px)}.gb_gbsh .gbng .gbtn.gbto #gbd1,.gb_gbsh .gbng .gbtn.gbto #gbs{-webkit-transform:translateX(-314px)}.gb_gbsh .gbtsb #gbd3,.gb_gbsh .gbtsb #gbs{-webkit-transform:translateX(55px)}.gb_gbsh .gbng .gbtsb #gbd3,.gb_gbsh .gbng .gbtsb #gbs{-webkit-transform:translateX(-350px)}.gb_gbsh.gb_gbshc .gbtn.gbto #gbd1,.gb_gbsh.gb_gbshc .gbtn.gbto #gbs{-webkit-transform:translateX(-144px)}.gb_gbsh.gb_gbshc .gbng .gbtn.gbto #gbd1,.gb_gbsh.gb_gbshc .gbng .gbtn.gbto #gbs{-webkit-transform:translateX(-104px)}.gb_gbsh.gb_gbshc .gbtsb #gbd3,.gb_gbsh.gb_gbshc .gbtsb #gbs{-webkit-transform:translateX(270px)}.gb_gbsh.gb_gbshc .gbng .gbtsb #gbd3,.gb_gbsh.gb_gbshc .gbng .gbtsb #gbs{-webkit-transform:translateX(-140px)}.gb_gbsh .gbtn.gbto #gbd1,.gb_gbsh .gbtn.gbto #gbs,.gb_gbsh .gbtsb.gbto #gbd3,.gb_gbsh .gbtsb.gbto #gbs,.gb_gbsh.gb_gbshc .gbtsb.gbto #gbd3,.gb_gbsh.gb_gbshc .gbtsb.gbto #gbs,.gb_gbsh.gb_gbshc .gbtn.gbto #gbd1,.gb_gbsh.gb_gbshc .gbtn.gbto #gbs{top:37px;-o-transform:translateX(0);-moz-transform:translateX(0)}.gb_gbsh #gbdw,.gb_gbsh #gb .gbes#gbu,.gb_gbsh #gb #gbu,.gb_gbsh .gbtn,.gb_gbsh .gbtsb{position:fixed;transform:translateZ(0);-moz-transform:translateZ(0);-o-transform:translateZ(0);-webkit-transform:translateZ(0);z-index:987}.gb_gbsh.gb_gbsf #gb #gbu{position:absolute}.gbtn{transition:right.15s cubic-bezier(0,1.12,.39,.98);-moz-transition:right.15s cubic-bezier(0,1.12,.39,.98);-o-transition:right.15s cubic-bezier(0,1.12,.39,.98);-webkit-transition:right.15s cubic-bezier(0,1.12,.39,.98)}.gb_gbsh #gb .gbes#gbu,.gb_gbsh #gb #gbu{top:0}.gb_gbsh.gb_gbsf #gb #gbu{top:102px}.gb_gbsh.gb_gbsf #gb .gbes#gbu{top:70px}.gb_gbsh #gbu{visibility:hidden}.gb_gbsh .gbtn,.gb_gbsh #gbwc{visibility:visible}.gb_gbsh .gbtsb.gbto{right:60px;top:7px;width:29px}@-webkit-keyframes gb__sn{0%,49%{opacity:0}50%{opacity:1;top:-30px}100%{opacity:1;top:7px}}@-moz-keyframes gb__sn{0%,49%{opacity:0}50%{opacity:1;top:-30px}100%{opacity:1;top:7px}}@-o-keyframes gb__sn{0%,49%{opacity:0}50%{opacity:1;top:-30px}100%{opacity:1;top:7px}}@keyframes gb__sn{0%,49%{opacity:0}50%{opacity:1;top:-30px}100%{opacity:1;top:7px}}.gb_gbsh .gbtn{right:60px;animation:gb__sn .15s;-webkit-animation:gb__sn .15s;-moz-animation:gb__sn .15s;-o-animation:gb__sn .15s;top:7px}.gb_gbsh.gb_gbshc .gbtn,.gb_gbsh.gb_gbshc .gbtsb.gbto{right:270px}#gbi4t{display:block;margin:1px 0;overflow:hidden;text-overflow:ellipsis}#gbg6 #gbi4t,#gbg4 #gbgs4d{color:#666;text-shadow:none}#gb_70,#gb_71{margin-right:15px;display:inline-block}#gb_70 .gbit,#gb_71 .gbit,#gbg7 .gbit{margin:0 15px;display:block}#gbgs4,.gbgs{background-color:#f8f8f8;background-image:-webkit-gradient(linear,left top,left bottom,from(#f8f8f8),to(#ececec));background-image:-webkit-linear-gradient(top,#f8f8f8,#ececec);background-image:-moz-linear-gradient(top,#f8f8f8,#ececec);background-image:-ms-linear-gradient(top,#f8f8f8,#ececec);background-image:-o-linear-gradient(top,#f8f8f8,#ececec);background-image:linear-gradient(top,#f8f8f8,#ececec);filter:progid:DXImageTransform.Microsoft.gradient(startColorStr='#f8f8f8',EndColorStr='#ececec');border:1px solid #c6c6c6;display:block;-moz-border-radius:2px;-o-border-radius:2px;-webkit-border-radius:2px;border-radius:2px;position:relative}#gbu #gb_70{color:#fff;font-size:11px;font-weight:bold;text-transform:uppercase}#gb_70 .gbgs{background-color:#d14836;background-image:-webkit-gradient(linear,left top,left bottom,from(#dd4b39),to(#d14836));background-image:-webkit-linear-gradient(top,#dd4b39,#d14836);background-image:-moz-linear-gradient(top,#dd4b39,#d14836);background-image:-ms-linear-gradient(top,#dd4b39,#d14836);background-image:-o-linear-gradient(top,#dd4b39,#d14836);background-image:linear-gradient(top,#dd4b39,#d14836);filter:progid:DXImageTransform.Microsoft.gradient(startColorStr='#dd4b39',EndColorStr='#d14836');border:1px solid transparent}#gb_70.gbgt-hvr .gbgs{background-color:#c53727;background-image:-webkit-gradient(linear,left top,left bottom,from(#dd4b39),to(#c53727));background-image:-webkit-linear-gradient(top,#dd4b39,#c53727);background-image:-moz-linear-gradient(top,#dd4b39,#c53727);background-image:-ms-linear-gradient(top,#dd4b39,#c53727);background-image:-o-linear-gradient(top,#dd4b39,#c53727);background-image:linear-gradient(top,#dd4b39,#c53727);filter:progid:DXImageTransform.Microsoft.gradient(startColorStr='#dd4b39',EndColorStr='#c53727');border-color:#b0281a;border-bottom-color:#af301f;-webkit-box-shadow:0 1px 1px rgba(0,0,0,.1);-moz-box-shadow:0 1px 1px rgba(0,0,0,.1);box-shadow:0 1px 1px rgba(0,0,0,.1)}#gb_70:active .gbgs,#gb_70.gbgt-hvr:active .gbgs{background-color:#b0281a;background-image:-webkit-gradient(linear,left top,left bottom,from(#dd4b39),to(#b0281a));background-image:-webkit-linear-gradient(top,#dd4b39,#b0281a);background-image:-moz-linear-gradient(top,#dd4b39,#b0281a);background-image:-ms-linear-gradient(top,#dd4b39,#b0281a);background-image:-o-linear-gradient(top,#dd4b39,#b0281a);background-image:linear-gradient(top,#dd4b39,#b0281a);border-color:#992a1b;-webkit-box-shadow:inset 0 1px 2px rgba(0,0,0,.1);-moz-box-shadow:inset 0 1px 2px rgba(0,0,0,.1);box-shadow:inset 0 1px 2px rgba(0,0,0,.1)}#gbgs4d{display:inline-block;position:relative;z-index:1000}#gbgs4dn{display:inline-block;overflow:hidden;text-overflow:ellipsis}.gbgt-hvr #gbgs4d{background-color:transparent;background-image:none}.gbg4p{margin-top:0px}#gbg4 #gbgs4{height:27px;position:relative;width:27px}.gbgt-hvr #gbgs4,#gbg4:focus #gbgs4,#gbg4:active #gbgs4,#gbg_70:focus .gbgs,#gbg_71:focus .gbgs,#gbg_70:active .gbgs,#gbg_71:active .gbgs,#gbg7:focus .gbgs,#gbg7:active .gbgs{background-color:#ffffff;background-image:-webkit-gradient(linear,left top,left bottom,from(#ffffff),to(#ececec));background-image:-webkit-linear-gradient(top,#ffffff,#ececec);background-image:-moz-linear-gradient(top,#ffffff,#ececec);background-image:-ms-linear-gradient(top,#ffffff,#ececec);background-image:-o-linear-gradient(top,#ffffff,#ececec);background-image:linear-gradient(top,#ffffff,#ececec);filter:progid:DXImageTransform.Microsoft.gradient(startColorStr='#ffffff',EndColorStr='#ececec');border-color:#bbb}#gbg4:active #gbgs4,#gb_70:active .gbgs,#gb_71:active .gbgs,#gbg7:active .gbgs{border-color:#b6b6b6;-moz-box-shadow:inset 0 1px 2px rgba(0,0,0,.3);-webkit-box-shadow:inset 0 1px 2px rgba(0,0,0,.3);box-shadow:inset 0 1px 2px rgba(0,0,0,.3)}#gbi4i,#gbi4id,#gbi4ip{left:0;height:27px;position:absolute;top:0;width:27px}#gbmpi,#gbmpid{margin-right:0;height:96px;width:96px}#gbi4id{background-position:0 -101px}.gbem #gbi4id,.gbemi #gbi4id{background-position:0 -101px}.gbes #gbi4id,.gbesi #gbi4id{background-position:0 -101px}.gbto #gbi4i,.gbto #gbi4ip,.gbto #gbi4id{top:0}#gbgs4 .gbmai{left:33px;top:12px}#gbgs4d .gbmai{left:100%;margin-left:5px;top:12px}#gbgs4 .gbmab,#gbgs4 .gbmac{left:5px}#gbgs4 .gbmab{margin:40px 0 0}#gbgs4 .gbmac{margin:41px 0 0;border-bottom-color:#fef9db}.gbemi .gbg4p,.gbem.gbg4p{margin-top:0px}.gbesi .gbg4p,.gbes.gbg4p{margin-top:0px}.gbemi #gbg4 #gbgs4,.gbem #gbg4 #gbgs4,.gbemi #gbg4 #gbi4i,.gbem #gbg4 #gbi4i,.gbemi #gbg4 #gbi4id,.gbem #gbg4 #gbi4id,.gbemi #gbg4 #gbi4ip,.gbem #gbg4 #gbi4ip{height:27px;width:27px}.gbesi #gbg4 #gbgs4,.gbes #gbg4 #gbgs4,.gbesi #gbi4i,.gbes #gbi4i,.gbesi #gbi4id,.gbes #gbi4id,.gbesi #gbi4ip,.gbes #gbi4ip{height:27px;width:27px}.gbemi #gbgs4 .gbmai,.gbem #gbgs4 .gbmai{left:33px;top:12px}.gbesi #gbgs4 .gbmai,.gbes #gbgs4 .gbmai{left:33px;top:12px}.gbemi#gb #gbg4 #gbgs4 .gbmab,.gbem#gbg4 #gbgs4 .gbmab{left:5px;margin:40px 0 0}.gbemi#gb #gbg4 #gbgs4 .gbmac,.gbem#gbg4 #gbgs4 .gbmac{left:5px;margin:41px 0 0}.gbesi#gb #gbg4 #gbgs4 .gbmab,.gbes#gbg4 #gbgs4 .gbmab{left:5px;margin:40px 0 0}.gbesi#gb #gbg4 #gbgs4 .gbmac,.gbes#gbg4 #gbgs4 .gbmac{left:5px;margin:41px 0 0}#gbgs4d .gbmab{margin:41px 0 0}#gbgs4d .gbmac{margin:42px 0 0;border-bottom-color:#fef9db}#gbgs4d .gbmab,#gbgs4d .gbmac{left:50%;margin-left:-5px}#gbmppc{position:relative}#gbmppc .gbmt{padding-left:55px;padding-bottom:10px;padding-top:10px}.gbmppci{left:20px;background-position:-33px -18px;height:25px;position:absolute;top:11px;width:25px}.gbem#gb,.gbemi#gb{height:102px}.gbes#gb,.gbesi#gb{height:102px}.gbem#gbx1,.gbem#gbx2,.gbem#gbqlw,.gbemi#gb #gbx1,.gbemi#gb #gbx2,.gbemi#gb #gbqlw{height:71px}.gbem#gb #gbbw,.gbemi#gb #gbbw{top:102px}.gbem#gbu,.gbem#gbq2,.gbem#gbq3,.gbemi#gb #gbu,.gbemi#gb #gbq2,.gbemi#gb #gbq3{padding-top:20px}.gbem#gbq2,.gbemi#gb #gbq2{margin-left:126px;padding-bottom:0}.gbexl#gbq2,.gbexli#gb #gbq2,.gbexxl#gbq2,.gbexxli#gb #gbq2{margin-left:126px}.gbem#gbq3,.gbemi#gb #gbq3{left:126px}.gbes#gbx1,.gbes#gbx2,.gbes#gbqlw,.gbesi#gb #gbx1,.gbesi#gb #gbx2,.gbesi#gb #gbqlw{height:57px}.gbes#gb #gbbw,.gbesi#gb #gbbw{top:102px}.gbes#gbu,.gbes#gbq2,.gbes#gbq3,.gbesi#gb #gbu,.gbesi#gb #gbq2,.gbesi#gb #gbq3{padding-top:8px}.gbet#gbq2,.gbeti#gb #gbq2,.gbes#gbq2,.gbesi#gb #gbq2{margin-left:126px;padding-bottom:0}.gbeu#gbq2,.gbeui#gb #gbq2{margin-left:126px;padding-bottom:0}.gbemi#gb .gbto #gbd1,.gbemi#gb .gbto #gbd3,.gbemi#gb .gbto #gbd4,.gbemi#gb .gbto #gbs,.gbto .gbem#gbd1,.gbto .gbem#gbd3,.gbto .gbem#gbd4,.gbto .gbem#gbs{top:51px}.gbesi#gb .gbto #gbd1,.gbesi#gb .gbto #gbd3,.gbesi#gb .gbto #gbd4,.gbesi#gb .gbto #gbs,.gbto .gbes#gbd1,.gbto .gbes#gbd3,.gbto .gbes#gbd4,.gbto .gbes#gbs{top:42px}.gbes#gbq3,.gbesi#gb #gbq3{left:126px}.gbem#gbq1,.gbemi#gb #gbq1{margin-left:16px}.gbem#gbql,.gbemi#gb #gbql,.gbes#gbql,.gbesi#gb #gbql,.gbet#gbql,.gbeti#gb #gbql,.gbeu#gbql,.gbeui#gb #gbql{background-position:-63px 0;height:37px;width:95px}.gbet#gbq1,.gbeti#gb #gbq1,.gbes#gbq1,.gbesi#gb #gbq1{margin-left:16px}.gbeu#gbq1,.gbeui#gb #gbq1{margin-left:16px}.gbemi#gb .gbqldr,.gbem#gbqlw .gbqldr{max-height:71px;max-width:160px}.gbem#gbu,.gbemi#gb #gbu{margin-right:12px}.gbet#gbu,.gbeti#gb #gbu,.gbeu#gbu,.gbeui#gb #gbu,.gbes#gbu,.gbesi#gb #gbu{margin-right:0px}.gbeu#gbu .gbt,.gbeui#gb #gbu .gbt,.gbet#gbu .gbt,.gbeti#gb #gbu .gbt,.gbes#gbu .gbt,.gbesi#gb #gbu .gbt{margin-left:6px}.gbeti#gb .gbqldr,.gbet#gbqlw .gbqldr,.gbesi#gb .gbqldr,.gbes#gbqlw .gbqldr{max-height:57px;max-width:144px}.gbeui#gb .gbqldr,.gbeu#gbqlw .gbqldr{max-height:57px;max-width:124px}.gbemi#gb #gbpr,.gbem#gbpr{left:28px}.gbemi#gb .gbqpa,.gbem#gbpr .gbqpa,.gbesi#gb .gbqpa,.gbes#gbpr .gbqpa{width:71px}.gbesi#gb #gbpr,.gbes#gbpr{left:16px}.gbemi#gb #gbgs4d .gbmab,.gbem#gbg4 #gbgs4d .gbmab{margin:41px 0 0}.gbesi#gb #gbgs4d .gbmab,.gbes#gbg4 #gbgs4d .gbmab{margin:33px 0 0}.gbemi#gb #gbgs4d .gbmac,.gbem#gbg4 #gbgs4d .gbmac{margin:42px 0 0}.gbesi#gb #gbgs4d .gbmac,.gbes#gbg4 #gbgs4d .gbmac{margin:34px 0 0}.gbemi#gb #gbgs4d .gbmac,.gbem#gbg4 #gbgs4d .gbmac,.gbesi#gb #gbgs4d .gbmac,.gbes#gbg4 #gbgs4d .gbmac,.gbemi#gb #gbgs4d .gbmab,.gbem#gbg4 #gbgs4d .gbmab,.gbesi#gb #gbgs4d .gbmab,.gbes#gbg4 #gbgs4d .gbmab{margin-left:-5px}#gb #gbx1,#gb #gbx3{left:0}#gbx1,#gb #gbx1,#gbq,#gbu,#gb #gbq,#gb #gbu{top:30px}#gb #gbu{top:30px}#gbzw #gbz{padding-left:0;z-index:991}#gbz .gbto #gbd,#gbz .gbto #gbs{top:29px}#gbx3{min-width:980px;border-color:#000;background-color:#2d2d2d;opacity:1;filter:alpha(opacity=100)}#gbz .gbzt,#gbz .gbgt{color:#bbb !important;font-weight:bold}#gbq .gbgt-hvr,#gbq .gbgt:focus,#gbz .gbz0l .gbts,#gbz .gbzt:hover,#gbz .gbzt:focus,#gbz .gbgt-hvr,#gbz .gbgt:focus,#gbu .gbz0l .gbts,#gbu .gbzt:hover,#gbu .gbzt:focus,#gbu .gbgt-hvr,#gbu .gbgt:focus{background-color:transparent;background-image:none}#gbz .gbz0l .gbts,#gbz .gbzt:hover,#gbz .gbzt:focus,#gbz .gbgt-hvr,#gbz .gbgt:focus{color:#fff!important}#gbz .gbma{border-top-color:#aaa}#gbz .gbzt:hover .gbma,#gbz .gbzt:focus .gbma,#gbz .gbgt-hvr .gbma,#gbz .gbgt:focus .gbma{border-top-color:#fff}#gbq1.gbto{-moz-box-shadow:none;-o-box-shadow:none;-webkit-box-shadow:none;box-shadow:none}#gbz .gbto .gbma,#gbz .gbto .gbzt:hover .gbma,#gbz .gbto .gbzt:focus .gbma,#gbz .gbto .gbgt-hvr .gbma,#gbz .gbto .gbgt:focus .gbma{border-top-color:#000}#gbz .gbto .gbts,#gbd .gbmt{color:#000 !important;font-weight:bold}#gbd .gbmt:hover,#gbd .gbmt:focus{background-color:#f5f5f5}#gbz .gbts{padding:0 9px;z-index:991}#gbz .gbto .gbts{padding-bottom:1px;padding-top:2px;z-index:1000}#gbqlw{cursor:pointer}#gbzw{left:0;height:30px;margin-left:6px;position:absolute;top:0;z-index:991}#gbz{height:30px}.gbemi#gb #gbzw,.gbem#gbzw{height:30px;margin-left:6px}.gbeui#gb #gbzw,.gbeu#gbzw,.gbeti#gb #gbzw,.gbet#gbzw,.gbesi#gb #gbzw,.gbes#gbzw{height:30px;margin-left:6px}.gbeui#gb #gbzw,.gbeu#gbzw{margin-left:2px}.gbemi#gb #gbzw #gbz,.gbem#gbzw #gbz{height:30px}.gbemi#gb #gbx3,.gbem#gbx3{height:29px}.gbesi#gb #gbzw #gbz,.gbes#gbzw #gbz{height:30px}.gbesi#gb #gbx3,.gbes#gbx3{height:29px}#gb.gbet #gbx3,#gb.gbeti #gbx3{min-width:836px;}#gb.gbeu #gbx3,#gb.gbeui #gbx3{min-width:780px;}#gbzw .gbt{line-height:27px}.gbemi#gb #gbzw .gbt .gbem#gbzw .gbt{line-height:27px}.gbesi#gb #gbzw .gbt,.gbes#gbzw .gbt{line-height:27px}.gbqfh#gbq1{display:none}.gbqfh#gbx1,.gbqfh#gbx2,.gbem#gb .gbqfh#gbx1,.gbem#gb .gbqfh#gbx2,.gbemi#gb .gbqfh#gbx1,.gbemi#gb .gbqfh#gbx2,.gbes#gb .gbqfh#gbx1,.gbes#gb .gbqfh#gbx2,.gbesi#gb .gbqfh#gbx1,.gbesi#gb .gbqfh#gbx2,.gbet#gb .gbqfh#gbx1,.gbet#gb .gbqfh#gbx2,.gbeti#gb .gbqfh#gbx1,.gbeti#gb .gbqfh#gbx2,.gbeu#gb .gbqfh#gbx1,.gbeu#gb .gbqfh#gbx2,.gbeui#gb .gbqfh#gbx1,.gbeui#gb .gbqfh#gbx2{border-bottom-width:0;height:0}.gbes#gb,.gbesi#gb{height:102px}.gbes#gbx1,.gbes#gbx2,.gbes#gbqlw,.gbesi#gb #gbx1,.gbesi#gb #gbx2,.gbesi#gb #gbqlw{height:71px}#gb .gbes#gbx1,#gb .gbes#gbx2,.gbesi#gb #gbx1,.gbesi#gb #gbx2,#gb .gbes#gbq,#gb .gbes#gbu,.gbesi#gb #gbq,.gbesi#gb #gbu{top:30px}.gbes#gb #gbbw,.gbesi#gb #gbbw{top:102px !important}.gbpro.gbes#gb #gbbw,.gbpro.gbesi#gb #gbbw{top:132px !important}.gbes#gbu,.gbes#gbq2,.gbes#gbq3,.gbesi#gb #gbu,.gbesi#gb #gbq2,.gbesi#gb #gbq3{padding-top:20px}.gbes#gbq2,.gbesi#gb #gbq2{padding-bottom:0}.gbesi#gb .gbto #gbd1,.gbesi#gb .gbto #gbd3,.gbesi#gb .gbto #gbd4,.gbesi#gb .gbto #gbs,.gbto .gbes#gbd1,.gbto .gbes#gbd3,.gbto .gbes#gbd4,.gbes#gbu .gbto #gbs{top:51px}.gbemi#gb #gbd,.gbem#gbzw #gbd,.gbemi#gb #gbzw .gbto #gbs,.gbem#gbzw .gbto #gbs{top:29px}.gbesi#gb #gbd,.gbes#gbzw #gbd,.gbesi#gb #gbzw .gbto #gbs,.gbes#gbzw .gbto #gbs{top:29px}.gbesi#gb #gbzw .gbto .gbts,.gbes#gbzw .gbto .gbts{padding-bottom:3px}.gbesi#gb #gbg3 .gbmab,.gbes#gbg3 .gbmab,.gbesi#gb #gbgs4 .gbmab,.gbes#gbg4 .gbmab{margin:40px 0 0}.gbesi#gb #gbg1 .gbmab,.gbes#gbg1 .gbmab{margin:41px 0 0}.gbesi#gb #gbg1 .gbmac,.gbes#gbg1 .gbmac{margin:42px 0 0}.gbesi#gb #gbg3 .gbmac,.gbes#gbg3 .gbmac,.gbesi#gb #gbgs4 .gbmac,.gbes#gbg4 .gbmac{margin:41px 0 0}.gbesi#gb #gbgs4d .gbmab,.gbes#gbg4 #gbgs4d .gbmab{margin:41px 0 0}.gbesi#gb #gbgs4d .gbmac,.gbes#gbg4 #gbgs4d .gbmac{margin:42px 0 0}#gbmpicb,#gbmpicp{bottom:0;color:#fff;display:block;font-size:9px;font-weight:bold;position:absolute;text-align:center;text-decoration:none !important;-moz-transition:opacity .218s ease-in-out;-o-transition:opacity .218s ease-in-out;-webkit-transition:opacity .218s ease-in-out;transition:opacity .218s ease-in-out;white-space:normal;width:96px}#gbmpicb{background-color:#4d90fe;opacity:.7;filter:alpha(opacity=70);padding:7px 0;-moz-transition:background-color .218s ease-in-out;-o-transition:background-color .218s ease-in-out;-webkit-transition:background-color .218s ease-in-out;transition:background-color .218s ease-in-out}#gbmpicp{padding:12px 0 7px 0}#gbmpas{max-height:276px}#gbmm{max-height:530px}.gbsb{-webkit-box-sizing:border-box;display:block;position:relative;*zoom:1}.gbsbic{overflow:auto}.gbsbis .gbsbt,.gbsbis .gbsbb{-webkit-mask-box-image:-webkit-gradient(linear,left top,right top,color-stop(0,rgba(0,0,0,.1)),color-stop(.5,rgba(0,0,0,.8)),color-stop(1,rgba(0,0,0,.1)));left:0;margin-right:0;opacity:0;position:absolute;width:100%}.gbsb .gbsbt:after,.gbsb .gbsbb:after{content:"";display:block;height:0;left:0;position:absolute;width:100%}.gbsbis .gbsbt{background:-webkit-gradient(linear,left top,left bottom,from(rgba(0,0,0,.2)),to(rgba(0,0,0,0)));background-image:-webkit-linear-gradient(top,rgba(0,0,0,.2),rgba(0,0,0,0));background-image:-moz-linear-gradient(top,rgba(0,0,0,.2),rgba(0,0,0,0));background-image:-ms-linear-gradient(top,rgba(0,0,0,.2),rgba(0,0,0,0));background-image:-o-linear-gradient(top,rgba(0,0,0,.2),rgba(0,0,0,0));background-image:linear-gradient(top,rgba(0,0,0,.2),rgba(0,0,0,0));height:6px;top:0}.gbsb .gbsbt:after{border-top:1px solid #ebebeb;border-color:rgba(0,0,0,.3);top:0}.gbsb .gbsbb{-webkit-mask-box-image:-webkit-gradient(linear,left top,right top,color-stop(0,rgba(0,0,0,.1)),color-stop(.5,rgba(0,0,0,.8)),color-stop(1,rgba(0,0,0,.1)));background:-webkit-gradient(linear,left bottom,left top,from(rgba(0,0,0,.2)),to(rgba(0,0,0,0)));background-image:-webkit-linear-gradient(bottom,rgba(0,0,0,.2),rgba(0,0,0,0));background-image:-moz-linear-gradient(bottom,rgba(0,0,0,.2),rgba(0,0,0,0));background-image:-ms-linear-gradient(bottom,rgba(0,0,0,.2),rgba(0,0,0,0));background-image:-o-linear-gradient(bottom,rgba(0,0,0,.2),rgba(0,0,0,0));background-image:linear-gradient(bottom,rgba(0,0,0,.2),rgba(0,0,0,0));bottom:0;height:4px}.gbsb .gbsbb:after{border-bottom:1px solid #ebebeb;border-color:rgba(0,0,0,.3);bottom:0}.gbsbic::-webkit-scrollbar{height:16px;width:16px}.gbsbic::-webkit-scrollbar-button{height:0;width:0}.gbsbic::-webkit-scrollbar-button:start:decrement,.gbsbic::-webkit-scrollbar-button:end:increment{display:block}.gbsbic::-webkit-scrollbar-button:vertical:start:increment,.gbsbic::-webkit-scrollbar-button:vertical:end:decrement{display:none}.gbsbic::-webkit-scrollbar-track:vertical,.gbsbic::-webkit-scrollbar-track:horizontal,.gbsbic::-webkit-scrollbar-thumb:vertical,.gbsbic::-webkit-scrollbar-thumb:horizontal,.gbsbis .gbsbic::-webkit-scrollbar-track:vertical,.gbsbis .gbsbic::-webkit-scrollbar-track:horizontal,.gbsbis .gbsbic::-webkit-scrollbar-thumb:vertical,.gbsbis .gbsbic::-webkit-scrollbar-thumb:horizontal{border-style:solid;border-color:transparent}.gbsbic::-webkit-scrollbar-track:vertical{background-clip:padding-box;background-color:#fff;border-left-width:5px;border-right-width:0}.gbsbic::-webkit-scrollbar-track:horizontal{background-clip:padding-box;background-color:#fff;border-bottom-width:0;border-top-width:5px}.gbsbic::-webkit-scrollbar-thumb{-webkit-box-shadow:inset 1px 1px 0 rgba(0,0,0,.1),inset 0 -1px 0 rgba(0,0,0,.07);background-clip:padding-box;background-color:rgba(0,0,0,.2);min-height:28px;padding-top:100px}.gbsbic::-webkit-scrollbar-thumb:hover{-webkit-box-shadow:inset 1px 1px 1px rgba(0,0,0,.25);background-color:rgba(0,0,0,.4)}.gbsbic::-webkit-scrollbar-thumb:active{-webkit-box-shadow:inset 1px 1px 3px rgba(0,0,0,.35);background-color:rgba(0,0,0,.5)}.gbsbic::-webkit-scrollbar-thumb:vertical{border-width:0;border-left-width:5px}.gbsbic::-webkit-scrollbar-thumb:horizontal{border-width:0;border-top-width:5px}.gbsbis .gbsbic::-webkit-scrollbar-track:vertical{border-left-width:6px;border-right-width:1px}.gbsbis .gbsbic::-webkit-scrollbar-track:horizontal{border-bottom:1px;border-top:6px}.gbsbis .gbsbic::-webkit-scrollbar-thumb:vertical{border-width:0;border-left-width:6px;border-right-width:1px}.gbsbis .gbsbic::-webkit-scrollbar-thumb:horizontal{border-width:0;border-bottom:1px;border-top:6px}.gbsbic::-webkit-scrollbar-track:hover{-webkit-box-shadow:inset 1px 0 0 rgba(0,0,0,.1);background-color:rgba(0,0,0,.05)}.gbsbic::-webkit-scrollbar-track:active{-webkit-box-shadow:inset 1px 0 0 rgba(0,0,0,.14),inset -1px -1px 0 rgba(0,0,0,.07);background-color:rgba(0,0,0,.05)}#sfcnt{display:none}#subform_ctrl{display:none}</style><style id="gstyle">body{margin:0;}.hp{height:100%;min-height:500px;overflow-y:auto;position:absolute;width:100%}#gog{padding:3px 8px 0}.gac_m td{line-height:17px}body,td,a,p,.h{font-family:arial,sans-serif}.h{color:#12c;font-size:20px}.q{color:#00c}.ts td{padding:0}.ts{border-collapse:collapse}em{font-weight:bold;font-style:normal}.lst{height:20px;width:496px}.ds{display:inline-block}span.ds{margin:3px 0 4px;margin-left:4px}.ctr-p{margin:0 auto;min-width:980px}.jhp input[type="submit"]{background-image:-webkit-gradient(linear,left top,left bottom,from(#f5f5f5),to(#f1f1f1));background-image:-webkit-linear-gradient(top,#f5f5f5,#f1f1f1);-webkit-border-radius:2px;-webkit-user-select:none;background-color:#f5f5f5;background-image:linear-gradient(top,#f5f5f5,#f1f1f1);background-image:-o-linear-gradient(top,#f5f5f5,#f1f1f1);border:1px solid #dcdcdc;border:1px solid rgba(0, 0, 0, 0.1);border-radius:2px;color:#666;cursor:default;font-family:arial,sans-serif;font-size:11px;font-weight:bold;height:29px;line-height:27px;margin:11px 6px;min-width:54px;padding:0 8px;text-align:center}.jhp input[type="submit"]:hover{background-image:-webkit-gradient(linear,left top,left bottom,from(#f8f8f8),to(#f1f1f1));background-image:-webkit-linear-gradient(top,#f8f8f8,#f1f1f1);-webkit-box-shadow:0 1px 1px rgba(0,0,0,0.1);background-color:#f8f8f8;background-image:linear-gradient(top,#f8f8f8,#f1f1f1);background-image:-o-linear-gradient(top,#f8f8f8,#f1f1f1);border:1px solid #c6c6c6;box-shadow:0 1px 1px rgba(0,0,0,0.1);color:#333}.jhp input[type="submit"]:focus{border:1px solid #4d90fe;outline:none}a.gb1,a.gb2,a.gb3,a.gb4{color:#11c !important}body{background:#fff;color:#222}a{color:#12c;text-decoration:none}a:hover,a:active{text-decoration:underline}.fl a{color:#12c}a:visited{color:#609}a.gb1,a.gb4{text-decoration:underline}a.gb3:hover{text-decoration:none}#ghead a.gb2:hover{color:#fff!important}.sblc{padding-top:5px}.sblc a{display:block;margin:2px 0;margin-left:13px;font-size:11px;}.lsbb{height:30px;display:block}.ftl,#footer a{color:#666;margin:2px 10px 0}#footer a:active{color:#dd4b39}.lsb{border:none;color:#000;cursor:pointer;height:30px;margin:0;outline:0;font:15px arial,sans-serif;vertical-align:top}.lst:focus{outline:none}#addlang a{padding:0 3px}body,html{font-size:small}h1,ol,ul,li{margin:0;padding:0}.nojsb{display:none}.nojsv{visibility:hidden}#body,#footer{display:block}#footer{font-size:10pt;min-height:49px;position:absolute;bottom:0;width:100%}#footer>div{border-top:1px solid #ebebeb;bottom:0;padding:3px 0 10px;position:absolute;width:100%}#flci{float:left;margin-left:-260px;text-align:left;width:260px}#fll{float:right;text-align:right;width:100%}#ftby{padding-left:260px}#ftby>div,#fll>div,#footer a{display:inline-block}@media only screen and (min-width:1222px){#ftby{margin: 0 44px}}.nojsb{display:none}.nojsv{visibility:hidden}.nbcl{background:url(/images/nav_logo132.png) no-repeat -140px -230px;height:11px;width:11px}</style><style>.lst-t{width:100%;}.kpbb,.kprb,.kpgb,.kpgrb{-webkit-border-radius:2px;border-radius:2px;color:#fff}.kpbb:hover,.kprb:hover,.kpgb:hover,.kpgrb:hover{-webkit-box-shadow:0 1px 1px rgba(0,0,0,0.1);box-shadow:0 1px 1px rgba(0,0,0,0.1);color:#fff}.kpbb:active,.kprb:active,.kpgb:active,.kpgrb:active{-webkit-box-shadow:inset 0 1px 2px rgba(0,0,0,0.3);box-shadow:inset 0 1px 2px rgba(0,0,0,0.3)}.kpbb{background-image:-webkit-gradient(linear,left top,left bottom,from(#4d90fe),to(#4787ed));background-image:-webkit-linear-gradient(top,#4d90fe,#4787ed);background-color:#4d90fe;background-image:linear-gradient(top,#4d90fe,#4787ed);border:1px solid #3079ed}.kpbb:hover{background-image:-webkit-gradient(linear,left top,left bottom,from(#4d90fe),to(#357ae8));background-image:-webkit-linear-gradient(top,#4d90fe,#357ae8);background-color:#357ae8;background-image:linear-gradient(top,#4d90fe,#357ae8);border:1px solid #2f5bb7}a.kpbb:link,a.kpbb:visited{color:#fff}.kprb{background-image:-webkit-gradient(linear,left top,left bottom,from(#dd4b39),to(#d14836));background-image:-webkit-linear-gradient(top,#dd4b39,#d14836);background-color:#dd4b39;background-image:linear-gradient(top,#dd4b39,#d14836);border:1px solid #dd4b39}.kprb:hover{background-image:-webkit-gradient(linear,left top,left bottom,from(#dd4b39),to(#c53727));background-image:-webkit-linear-gradient(top,#dd4b39,#c53727);background-color:#c53727;background-image:linear-gradient(top,#dd4b39,#c53727);border:1px solid #b0281a;border-bottom-color:#af301f}.kprb:active{background-image:-webkit-gradient(linear,left top,left bottom,from(#dd4b39),to(#b0281a));background-image:-webkit-linear-gradient(top,#dd4b39,#b0281a);background-color:#b0281a;background-image:linear-gradient(top,#dd4b39,#b0281a);}.kpgb{background-image:-webkit-gradient(linear,left top,left bottom,from(#3d9400),to(#398a00));background-image:-webkit-linear-gradient(top,#3d9400,#398a00);background-color:#3d9400;background-image:linear-gradient(top,#3d9400,#398a00);border:1px solid #29691d;}.kpgb:hover{background-image:-webkit-gradient(linear,left top,left bottom,from(#3d9400),to(#368200));background-image:-webkit-linear-gradient(top,#3d9400,#368200);background-color:#368200;background-image:linear-gradient(top,#3d9400,#368200);border:1px solid #2d6200}.kpgrb{background-image:-webkit-gradient(linear,left top,left bottom,from(#f5f5f5),to(#f1f1f1));background-image:-webkit-linear-gradient(top,#f5f5f5,#f1f1f1);background-color:#f5f5f5;background-image:linear-gradient(top,#f5f5f5,#f1f1f1);border:1px solid #dcdcdc;color:#555}.kpgrb:hover{background-image:-webkit-gradient(linear,left top,left bottom,from(#f8f8f8),to(#f1f1f1));background-image:-webkit-linear-gradient(top,#f8f8f8,#f1f1f1);background-color:#f8f8f8;background-image:linear-gradient(top,#f8f8f8,#f1f1f1);border:1px solid #dcdcdc;color:#333}a.kpgrb:link,a.kpgrb:visited{color:#555}#gbqfq{padding:1px 0 0 9px}#pocs{background:#fff1a8;color:#000;font-size:10pt;margin:0;padding:5px 7px 0px}#pocs.sft{background:transparent;color:#777}#pocs a{color:#11c}#pocs.sft a{color:#36c}#pocs > div{margin:0;padding:0}.gl{white-space:nowrap}.big .tsf-p{padding-left:126px;padding-right:352px}.tsf-p{padding-left:126px;padding-right:46px}.fade #center_col,.fade #rhs,.fade #leftnav{filter:alpha(opacity=33.3);opacity:0.333}.fade-hidden #center_col,.fade-hidden #rhs,.fade-hidden #leftnav{visibility:hidden}.flyr-o,.flyr-w{position:absolute;background-color:#fff;z-index:3;display:block}.flyr-o{filter:alpha(opacity=66.6);opacity:0.666;}.flyr-w{filter:alpha(opacity=20.0);opacity:0.2;}.flyr-h{filter:alpha(opacity=0);opacity:0}.flyr-c{display:none}.flt,.flt u,a.fl{text-decoration:none}.flt:hover,.flt:hover u,a.fl:hover{text-decoration:underline}#knavm{color:#4273db;display:inline;font:11px arial,sans-serif!important;left:-13px;position:absolute;top:2px;z-index:2}#pnprev #knavm{bottom:1px;top:auto}#pnnext #knavm{bottom:1px;left:40px;top:auto}a.noline{outline:0}</style><script>var _gjwl=location;function _gjuc(){var a=_gjwl.href.indexOf("#");return 0<=a&&(a=_gjwl.href.substring(a+1),/(^|&)q=/.test(a)&&-1==a.indexOf("#")&&!/(^|&)cad=h($|&)/.test(a))?(_gjwl.replace("/search?"+a.replace(/(^|&)fp=[^&]*/g,"")+"&cad=h"),1):0}function _gjp(){window._gjwl.hash&&window._gjuc()||setTimeout(_gjp,500)};
window.rwt=function(a,g,h,m,n,i,c,o,j,d){return true};
(function(){try{var e=!0,h=null,k=!1;var ba=function(a,b,c,d){d=d||{};d._sn=["cfg",b,c].join(".");window.gbar.logger.ml(a,d)};var n=window.gbar=window.gbar||{},q=window.gbar.i=window.gbar.i||{},ca;function _tvn(a,b){var c=parseInt(a,10);return isNaN(c)?b:c}function _tvf(a,b){var c=parseFloat(a);return isNaN(c)?b:c}function _tvv(a){return!!a}function r(a,b,c){(c||n)[a]=b}n.bv={n:_tvn("2",0),r:"r_cp.r_qf.",f:".49.65.51.40.70.46.55.36.",e:"31215",m:_tvn("2",1)};
function da(a,b,c){var d="on"+b;if(a.addEventListener)a.addEventListener(b,c,k);else if(a.attachEvent)a.attachEvent(d,c);else{var g=a[d];a[d]=function(){var a=g.apply(this,arguments),b=c.apply(this,arguments);return void 0==a?b:void 0==b?a:b&&a}}}var ea=function(a){return function(){return n.bv.m==a}},fa=ea(1),ga=ea(2);r("sb",fa);r("kn",ga);q.a=_tvv;q.b=_tvf;q.c=_tvn;q.i=ba;var s=window.gbar.i.i;var u=function(){},v=function(){},w=function(a){var b=new Image,c=ha;b.onerror=b.onload=b.onabort=function(){try{delete ia[c]}catch(a){}};ia[c]=b;b.src=a;ha=c+1},ia=[],ha=0;r("logger",{il:v,ml:u,log:w});var x=window.gbar.logger;var y={},ja={},z=[],ka=q.b("0.1",0.1),la=q.a("1",e),ma=function(a,b){z.push([a,b])},na=function(a,b){y[a]=b},oa=function(a){return a in y},A={},C=function(a,b){A[a]||(A[a]=[]);A[a].push(b)},D=function(a){C("m",a)},pa=function(a,b){var c=document.createElement("script");c.src=a;c.async=la;Math.random()<ka&&(c.onerror=function(){c.onerror=h;u(Error("Bundle load failed: name="+(b||"UNK")+" url="+a))});(document.getElementById("xjsc")||document.body).appendChild(c)},
G=function(a){for(var b=0,c;(c=z[b])&&c[0]!=a;++b);c&&(!c[1].l&&!c[1].s)&&(c[1].s=e,E(2,a),c[1].url&&pa(c[1].url,a),c[1].libs&&F&&F(c[1].libs))},qa=function(a){C("gc",a)},H=h,ra=function(a){H=a},E=function(a,b,c){if(H){a={t:a,b:b};if(c)for(var d in c)a[d]=c[d];try{H(a)}catch(g){}}};r("mdc",y);r("mdi",ja);r("bnc",z);r("qGC",qa);r("qm",D);r("qd",A);r("lb",G);r("mcf",na);r("bcf",ma);r("aq",C);r("mdd","");r("has",oa);r("trh",ra);r("tev",E);if(q.a("1")){var I=q.a("1"),sa=q.a("1"),ta=q.a("1"),ua=window.gapi={},va=function(a,b){var c=function(){n.dgl(a,b)};I?D(c):(C("gl",c),G("gl"))},wa={},xa=function(a){a=a.split(":");for(var b;(b=a.pop())&&wa[b];);return!b},F=function(a){function b(){for(var b=a.split(":"),d=0,g;g=b[d];++d)wa[g]=1;for(b=0;d=z[b];++b)d=d[1],(g=d.libs)&&(!d.l&&d.i&&xa(g))&&d.i()}n.dgl(a,b)},J=window.___jsl={};J.h="m;/_/scs/abc-static/_/js/k=gapi.gapi.en.a0irxetnvx4.O/m=__features__/am=EA/rt=j/d=1/rs=AItRSTMM3Tduq30stOAPMPXAx0A6ctSSsg";J.ms="https://apis.google.com";
J.m="";J.l=[];I||z.push(["gl",{url:"//ssl.gstatic.com/gb/js/abc/glm_e7bb39a7e1a24581ff4f8d199678b1b9.js"}]);var ya={pu:sa,sh:"",si:ta};y.gl=ya;r("load",va,ua);r("dgl",va);r("agl",xa);q.o=I};var za=q.b("0.1",0.001),Aa=0;
function _mlToken(a,b){try{if(1>Aa){Aa++;var c,d=a,g=b||{},f=encodeURIComponent,m="es_plusone_gc_20130717.0_p0",l=["//www.google.com/gen_204?atyp=i&zx=",(new Date).getTime(),"&jexpid=",f("35972"),"&srcpg=",f("prop=1"),"&jsr=",Math.round(1/za),"&ogev=",f("3wXyUfSDM4LC4APHsoGYDQ"),"&ogf=",n.bv.f,"&ogrp=",f(""),"&ogv=",f("1374518932.1374081516"),m?"&oggv="+f(m):"","&ogd=",f("com"),"&ogl=",f("en")];g._sn&&(g._sn="og."+g._sn);for(var p in g)l.push("&"),
l.push(f(p)),l.push("="),l.push(f(g[p]));l.push("&emsg=");l.push(f(d.name+":"+d.message));var t=l.join("");Ba(t)&&(t=t.substr(0,2E3));c=t;var B=window.gbar.logger._aem(a,c);w(B)}}catch(Y){}}var Ba=function(a){return 2E3<=a.length},Da=function(a,b){return b};function Ga(a){u=a;r("_itl",Ba,x);r("_aem",Da,x);r("ml",u,x);a={};y.er=a}q.a("")?Ga(function(a){throw a;}):q.a("1")&&Math.random()<za&&Ga(_mlToken);var _E="left",L=function(a,b){var c=a.className;K(a,b)||(a.className+=(""!=c?" ":"")+b)},M=function(a,b){var c=a.className,d=RegExp("\\s?\\b"+b+"\\b");c&&c.match(d)&&(a.className=c.replace(d,""))},K=function(a,b){var c=RegExp("\\b"+b+"\\b"),d=a.className;return!(!d||!d.match(c))},Ha=function(a,b){K(a,b)?M(a,b):L(a,b)};r("ca",L);r("cr",M);r("cc",K);q.k=L;q.l=M;q.m=K;q.n=Ha;var Ia=["gb_71","gb_155"],N;function Ja(a){N=a}function Ka(a){var b=N&&!a.href.match(/.*\/accounts\/ClearSID[?]/)&&encodeURIComponent(N());b&&(a.href=a.href.replace(/([?&]continue=)[^&]*/,"$1"+b))}function La(a){window.gApplication&&(a.href=window.gApplication.getTabUrl(a.href))}function Ma(a){try{var b=(document.forms[0].q||"").value;b&&(a.href=a.href.replace(/([?&])q=[^&]*|$/,function(a,c){return(c||"&")+"q="+encodeURIComponent(b)}))}catch(c){s(c,"sb","pq")}}
var Na=function(){for(var a=[],b=0,c;c=Ia[b];++b)(c=document.getElementById(c))&&a.push(c);return a},Oa=function(){var a=Na();return 0<a.length?a[0]:h},Pa=function(){return document.getElementById("gb_70")},O={},P={},Qa={},Q={},R=void 0,Va=function(a,b){try{var c=document.getElementById("gb");L(c,"gbpdjs");S();Ra(document.getElementById("gb"))&&L(c,"gbrtl");if(b&&b.getAttribute){var d=b.getAttribute("aria-owns");if(d.length){var g=document.getElementById(d);if(g){var f=b.parentNode;if(R==d)R=void 0,
M(f,"gbto");else{if(R){var m=document.getElementById(R);if(m&&m.getAttribute){var l=m.getAttribute("aria-owner");if(l.length){var p=document.getElementById(l);p&&p.parentNode&&M(p.parentNode,"gbto")}}}Sa(g)&&Ta(g);R=d;L(f,"gbto")}}}}D(function(){n.tg(a,b,e)});Ua(a)}catch(t){s(t,"sb","tg")}},Wa=function(a){D(function(){n.close(a)})},Xa=function(a){D(function(){n.rdd(a)})},Ra=function(a){var b,c="direction",d=document.defaultView;d&&d.getComputedStyle?(a=d.getComputedStyle(a,""))&&(b=a[c]):b=a.currentStyle?
a.currentStyle[c]:a.style[c];return"rtl"==b},Za=function(a,b,c){if(a)try{var d=document.getElementById("gbd5");if(d){var g=d.firstChild,f=g.firstChild,m=document.createElement("li");m.className=b+" gbmtc";m.id=c;a.className="gbmt";m.appendChild(a);if(f.hasChildNodes()){c=[["gbkc"],["gbf","gbe","gbn"],["gbkp"],["gbnd"]];for(var d=0,l=f.childNodes.length,g=k,p=-1,t=0,B;B=c[t];t++){for(var Y=0,$;$=B[Y];Y++){for(;d<l&&K(f.childNodes[d],$);)d++;if($==b){f.insertBefore(m,f.childNodes[d]||h);g=e;break}}if(g){if(d+
1<f.childNodes.length){var Ca=f.childNodes[d+1];!K(Ca.firstChild,"gbmh")&&!Ya(Ca,B)&&(p=d+1)}else if(0<=d-1){var Ea=f.childNodes[d-1];!K(Ea.firstChild,"gbmh")&&!Ya(Ea,B)&&(p=d)}break}0<d&&d+1<l&&d++}if(0<=p){var aa=document.createElement("li"),Fa=document.createElement("div");aa.className="gbmtc";Fa.className="gbmt gbmh";aa.appendChild(Fa);f.insertBefore(aa,f.childNodes[p])}n.addHover&&n.addHover(a)}else f.appendChild(m)}}catch(xb){s(xb,"sb","al")}},Ya=function(a,b){for(var c=b.length,d=0;d<c;d++)if(K(a,
b[d]))return e;return k},$a=function(a,b,c){Za(a,b,c)},ab=function(a,b){Za(a,"gbe",b)},bb=function(){D(function(){n.pcm&&n.pcm()})},cb=function(){D(function(){n.pca&&n.pca()})},db=function(a,b,c,d,g,f,m,l,p,t){D(function(){n.paa&&n.paa(a,b,c,d,g,f,m,l,p,t)})},eb=function(a,b){O[a]||(O[a]=[]);O[a].push(b)},fb=function(a,b){P[a]||(P[a]=[]);P[a].push(b)},gb=function(a,b){Qa[a]=b},hb=function(a,b){Q[a]||(Q[a]=[]);Q[a].push(b)},Ua=function(a){a.preventDefault&&a.preventDefault();a.returnValue=k;a.cancelBubble=
e},ib=h,Ta=function(a,b){S();if(a){jb(a,"Opening&hellip;");T(a,e);var c="undefined"!=typeof b?b:1E4,d=function(){kb(a)};ib=window.setTimeout(d,c)}},lb=function(a){S();a&&(T(a,k),jb(a,""))},kb=function(a){try{S();var b=a||document.getElementById(R);b&&(jb(b,"This service is currently unavailable.%1$sPlease try again later.","%1$s"),T(b,e))}catch(c){s(c,"sb","sdhe")}},jb=function(a,b,c){if(a&&b){var d=Sa(a);if(d){if(c){d.innerHTML="";b=b.split(c);c=0;for(var g;g=b[c];c++){var f=document.createElement("div");f.innerHTML=g;
d.appendChild(f)}}else d.innerHTML=b;T(a,e)}}},T=function(a,b){var c=void 0!==b?b:e;c?L(a,"gbmsgo"):M(a,"gbmsgo")},Sa=function(a){for(var b=0,c;c=a.childNodes[b];b++)if(K(c,"gbmsg"))return c},S=function(){ib&&window.clearTimeout(ib)},mb=function(a){var b="inner"+a;a="offset"+a;return window[b]?window[b]:document.documentElement&&document.documentElement[a]?document.documentElement[a]:0},nb=function(){return k},ob=function(){return!!R};r("so",Oa);r("sos",Na);r("si",Pa);r("tg",Va);r("close",Wa);
r("rdd",Xa);r("addLink",$a);r("addExtraLink",ab);r("pcm",bb);r("pca",cb);r("paa",db);r("ddld",Ta);r("ddrd",lb);r("dderr",kb);r("rtl",Ra);r("op",ob);r("bh",O);r("abh",eb);r("dh",P);r("adh",fb);r("ch",Q);r("ach",hb);r("eh",Qa);r("aeh",gb);ca=q.a("")?La:Ma;r("qs",ca);r("setContinueCb",Ja);r("pc",Ka);r("bsy",nb);q.d=Ua;q.j=mb;var pb={};y.base=pb;r("wg",{rg:{}});var qb={tiw:q.c("15000",0),tie:q.c("30000",0)};y.wg=qb;var rb={thi:q.c("10000",0),thp:q.c("180000",0),tho:q.c("5000",0),tet:q.b("0.5",0)};y.wm=rb;z.push(["m",{url:"//ssl.gstatic.com/gb/js/smm_f3709b68f5d2f2cb75de9df3f7819c89.js"}]);var sb={};y.heavy=sb;n.sg={c:"1"};if(q.a("1")){var tb=q.a("");z.push(["gc",{auto:tb,url:"//ssl.gstatic.com/gb/js/abc/gci_91f30755d6a6b787dcc2a4062e6e9824.js",libs:"googleapis.client:plusone"}]);var ub={version:"gci_91f30755d6a6b787dcc2a4062e6e9824.js",index:"0",lang:"en"};y.gc=ub;var vb=function(a){window.googleapis&&window.iframes?a&&a():(a&&qa(a),G("gc"))};r("lGC",vb);q.a("1")&&r("lPWF",vb)};window.__PVT="APfa0bqc1ez4qedLw4lOPVTFRprQGB-q1VC54rLuWcGNsa7O3vK3MBLWYhEUa7NHNPmOpdQtTpSYtwRzwBWaWndeqoCEpfbZxQ==";if(q.a("1")&&q.a("1")){var wb=function(a){vb(function(){C("pw",a);G("pw")})};r("lPW",wb);z.push(["pw",{url:"//ssl.gstatic.com/gb/js/abc/pwm_45f73e4df07a0e388b0fa1f3d30e7280.js"}]);var yb=[],zb=function(a){yb[0]=a},Ab=function(a,b){var c=b||{};c._sn="pw";u(a,c)},Bb={signed:yb,elog:Ab,base:"https://plusone.google.com/u/0",loadTime:(new Date).getTime()};y.pw=Bb;var Cb=function(a,b){for(var c=b.split("."),d=function(){var b=arguments;a(function(){for(var a=n,d=0,f=c.length-1;d<f;++d)a=a[c[d]];a[c[d]].apply(a,b)})},g=n,f=0,m=c.length-1;f<
m;++f)g=g[c[f]]=g[c[f]]||{};return g[c[f]]=d};Cb(wb,"pw.clk");Cb(wb,"pw.hvr");r("su",zb,n.pw)};var Db={G:1,J:2,ga:3,A:4,Z:5,O:6,H:7,P:8,ka:9,X:10,N:11,W:12,V:13,Q:14,T:15,S:16,ia:17,D:18,R:19,ja:20,ha:21,B:22,I:23,ma:24,na:25,la:26,g:27,u:28,w:29,v:30,fa:31,ba:32,ca:33,L:34,M:35,ea:36,da:37,aa:38,F:39,U:40,C:41,$:42,Y:43,K:500},Eb=[1,2,3,4,5,6,9,10,11,13,14,28,29,30,34,35,37,38,39,40,41,42,43,500];var Fb=q.b("0.001",1E-4),Gb=q.b("0.01",1),Hb=k,Ib=k;if(q.a("1")){var Jb=Math.random();Jb<=Fb&&(Hb=e);Jb<=Gb&&(Ib=e)}var Kb=Db,U=h;function Lb(){var a=0,b=function(b,d){q.a(d)&&(a|=b)};b(1,"1");b(2,"1");b(4,"1");b(8,"");return a}
function Mb(a,b){var c=Fb,d=Hb,g;g=a;if(!U){U={};for(var f=0;f<Eb.length;f++){var m=Eb[f];U[m]=e}}if(g=!!U[g])c=Gb,d=Ib;if(d){d=encodeURIComponent;g="es_plusone_gc_20130717.0_p0";n.rp?(f=n.rp(),f="-1"!=f?f:""):f="";c=["//www.google.com/gen_204?atyp=i&zx=",(new Date).getTime(),"&oge=",a,"&ogex=",d("35972"),"&ogev=",d("3wXyUfSDM4LC4APHsoGYDQ"),"&ogf=",n.bv.f,"&ogp=",d("1"),"&ogrp=",d(f),"&ogsr=",Math.round(1/c),"&ogv=",d("1374518932.1374081516"),
g?"&oggv="+d(g):"","&ogd=",d("com"),"&ogl=",d("en"),"&ogus=",Lb()];if(b){"ogw"in b&&(c.push("&ogw="+b.ogw),delete b.ogw);var l;g=b;f=[];for(l in g)0!=f.length&&f.push(","),f.push(Nb(l)),f.push("."),f.push(Nb(g[l]));l=f.join("");""!=l&&(c.push("&ogad="),c.push(d(l)))}w(c.join(""))}}function Nb(a){"number"==typeof a&&(a+="");return"string"==typeof a?a.replace(".","%2E").replace(",","%2C"):a}v=Mb;r("il",v,x);var Ob={};y.il=Ob;var Pb=function(a,b,c,d,g,f,m,l,p,t){D(function(){n.paa(a,b,c,d,g,f,m,l,p,t)})},Qb=function(){D(function(){n.prm()})},Rb=function(a){D(function(){n.spn(a)})},Sb=function(a){D(function(){n.sps(a)})},Tb=function(a){D(function(){n.spp(a)})},Ub={"27":"//lh3.googleusercontent.com/-6D9GO1MSV4k/AAAAAAAAAAI/AAAAAAAAAAA/6WbMvdKKTUk/s27-c/photo.jpg","27":"//lh3.googleusercontent.com/-6D9GO1MSV4k/AAAAAAAAAAI/AAAAAAAAAAA/6WbMvdKKTUk/s27-c/photo.jpg","27":"//lh3.googleusercontent.com/-6D9GO1MSV4k/AAAAAAAAAAI/AAAAAAAAAAA/6WbMvdKKTUk/s27-c/photo.jpg"},Vb=function(a){return(a=Ub[a])||"//lh3.googleusercontent.com/-6D9GO1MSV4k/AAAAAAAAAAI/AAAAAAAAAAA/6WbMvdKKTUk/s27-c/photo.jpg"},
Wb=function(){D(function(){n.spd()})};r("spn",Rb);r("spp",Tb);r("sps",Sb);r("spd",Wb);r("paa",Pb);r("prm",Qb);eb("gbd4",Qb);
if(q.a("1")){var Xb={d:q.a(""),e:"mthode@mthode.org",sanw:q.a(""),p:"//lh3.googleusercontent.com/-6D9GO1MSV4k/AAAAAAAAAAI/AAAAAAAAAAA/6WbMvdKKTUk/s96-c/photo.jpg",cp:"1",xp:q.a("1"),mg:"%1$s (delegated)",md:"%1$s (default)",mh:"276",s:"1",pp:Vb,ppl:q.a(""),ppa:q.a("1"),ppm:"Google+ page"};
y.prf=Xb};var V,Yb,W,Zb,X=0,$b=function(a,b,c){if(a.indexOf)return a.indexOf(b,c);if(Array.indexOf)return Array.indexOf(a,b,c);for(c=c==h?0:0>c?Math.max(0,a.length+c):c;c<a.length;c++)if(c in a&&a[c]===b)return c;return-1},Z=function(a,b){return-1==$b(a,X)?(s(Error(X+"_"+b),"up","caa"),k):e},bc=function(a,b){Z([1,2],"r")&&(V[a]=V[a]||[],V[a].push(b),2==X&&window.setTimeout(function(){b(ac(a))},0))},cc=function(a,b,c){if(Z([1],"nap")&&c){for(var d=0;d<c.length;d++)Yb[c[d]]=e;n.up.spl(a,b,"nap",c)}},dc=function(a,
b,c){if(Z([1],"aop")&&c){if(W)for(var d in W)W[d]=W[d]&&-1!=$b(c,d);else{W={};for(d=0;d<c.length;d++)W[c[d]]=e}n.up.spl(a,b,"aop",c)}},ec=function(){try{if(X=2,!Zb){Zb=e;for(var a in V)for(var b=V[a],c=0;c<b.length;c++)try{b[c](ac(a))}catch(d){s(d,"up","tp")}}}catch(g){s(g,"up","mtp")}},ac=function(a){if(Z([2],"ssp")){var b=!Yb[a];W&&(b=b&&!!W[a]);return b}};Zb=k;V={};Yb={};W=h;
var X=1,fc=function(a){var b=e;try{b=!a.cookie}catch(c){}return b},gc=function(){try{return!!window.localStorage&&"object"==typeof window.localStorage}catch(a){return k}},hc=function(a){return a&&a.style&&a.style.oa&&"undefined"!=typeof a.load},ic=function(a,b,c,d){try{fc(document)||(d||(b="og-up-"+b),gc()?window.localStorage.setItem(b,c):hc(a)&&(a.setAttribute(b,c),a.save(a.id)))}catch(g){g.code!=DOMException.QUOTA_EXCEEDED_ERR&&s(g,"up","spd")}},jc=function(a,b,c){try{if(fc(document))return"";c||
(b="og-up-"+b);if(gc())return window.localStorage.getItem(b);if(hc(a))return a.load(a.id),a.getAttribute(b)}catch(d){d.code!=DOMException.QUOTA_EXCEEDED_ERR&&s(d,"up","gpd")}return""},kc=function(a,b,c){a.addEventListener?a.addEventListener(b,c,k):a.attachEvent&&a.attachEvent("on"+b,c)},lc=function(a){for(var b=0,c;c=a[b];b++){var d=n.up;c=c in d&&d[c];if(!c)return k}return e};r("up",{r:bc,nap:cc,aop:dc,tp:ec,ssp:ac,spd:ic,gpd:jc,aeh:kc,aal:lc});
var mc=function(a,b){a[b]=function(c){var d=arguments;n.qm(function(){a[b].apply(this,d)})}};mc(n.up,"sl");mc(n.up,"si");mc(n.up,"spl");n.mcf("up",{sp:q.b("0.01",1),tld:"com",prid:"1"});function nc(){function a(){if(document.getElementById("gb")==h)v(Kb.g,{_m:"nogb"});else{for(var b;(b=f[m++])&&!("m"==b[0]||b[1].auto););b&&(E(2,b[0]),b[1].url&&pa(b[1].url,b[0]),b[1].libs&&F&&F(b[1].libs));m<f.length&&setTimeout(a,0)}}function b(){0<g--?setTimeout(b,0):a()}var c=q.a("1"),d=q.a(""),g=3,f=z,m=0,l=window.gbarOnReady;if(l)try{l()}catch(p){s(p,"mle","or")}d?r("ldb",a):c?da(window,"load",b):b()}r("rdl",nc);}catch(e){window.gbar&&gbar.logger&&gbar.logger.ml(e,{"_sn":"cfg.init"});}})();
(function(){try{var c=window.gbar.i.i;var e=window.gbar;var f={};function g(a,b){f[a]||(f[a]=[]);f[a].push(b)}function h(a){if(a.type)for(var b=f[a.type],d=0;b&&d<b.length;++d)try{b[d](a)}catch(k){c(k,"sbc","d")}}e.bc={subscribe:g,dispatch:h};}catch(e){window.gbar&&gbar.logger&&gbar.logger.ml(e,{"_sn":"cfg.init"});}})();
(function(){try{var a=window.gbar;var b=a.i;a.mcf("sw",{uo:"",s:"https",h:"plus.google.com",po:"",pa:"/u/0/_/notifications/frame",q:"sourceid=1",f:"pid=1",ipd:b.c("5",5),it:b.c("60",-1),l:"en",sto:b.c("10",10),mnr:b.c("0",0),co:b.a(""),obc:b.a(""),siu:"https://accounts.google.com/ServiceLogin?hl=en&continue=https://www.google.com/",ogd:".google.com",sk:"og.og.en_US.pSMH1HeDt-4.DU",jk:"og.og.en_US.fH3Dx4xMBcw.O",js:"www.gstatic.com",ml:"gu",ms:"AItRSTPkaqCDq-UZ2qqaNF_F4noVBkAoaA",
lj:b.a(""),ge:b.a("1"),gto:b.c("10")});}catch(e){window.gbar&&gbar.logger&&gbar.logger.ml(e,{"_sn":"cfg.init"});}})();
(function(){try{var a=window.gbar;var b=[],d=function(c){b.push(c)};a.nuc=b;a.anuc=d;var e=function(){a.aq("m",function(){a.lni()})};a.lni=e;a.mcf("no",{ht:parseInt("100",10),n:"Notifications",es:parseInt("1",10),s:"https",h:"plus.google.com",po:"",fpmsec:parseInt("0",10),xuo:"",xhm:"POST",xpa:"/u/0/_/n/gcosuc"});}catch(e){window.gbar&&gbar.logger&&gbar.logger.ml(e,{"_sn":"cfg.init"});}})();
(function(){try{var a=window.gbar;var b=[],d=function(c){b.push(c)};a.smc=b;a.asmc=d;a.mcf("sb",{ht:parseInt("100",10),n:"Share"});}catch(e){window.gbar&&gbar.logger&&gbar.logger.ml(e,{"_sn":"cfg.init"});}})();
(function(){try{var b=window.gbar;var d=function(a,c){b[a]=function(){return window.navigator&&window.navigator.userAgent?c(window.navigator.userAgent):!1}},e=function(a){return!(/AppleWebKit\/.+(?:Version\/[35]\.|Chrome\/[01]\.)/.test(a)||-1!=a.indexOf("Firefox/3.5."))};d("bs_w",e);}catch(e){window.gbar&&gbar.logger&&gbar.logger.ml(e,{"_sn":"cfg.init"});}})();
(function(){try{var a=window.gbar;a.mcf("sf",{});}catch(e){window.gbar&&gbar.logger&&gbar.logger.ml(e,{"_sn":"cfg.init"});}})();
(function(){try{var aa=window.gbar.i.i;var a=window.gbar;var e=a.i;var k,n;var u=function(b,d){aa(b,"es",d)},v=function(b){return document.getElementById(b)},w=function(b,d){var f=Array.prototype.slice.call(arguments,1);return function(){var c=Array.prototype.slice.call(arguments);c.unshift.apply(c,f);return b.apply(this,c)}},x=void 0,y=void 0,ba=e.c("840"),ca=e.c("640");e.c("840");
var ia=e.c("640"),ja=e.c("590"),ka=e.c("1514"),la=e.c("1474");e.c("1474");var ma=e.c("1252"),na=e.c("1060"),oa=e.c("995"),pa=e.c("851"),A={},B={},C={},D={},E={},F={},G={};A.h=e.c("102");A.m=e.c("44");A.f=e.c("126");
B.h=e.c("102");B.m=e.c("44");B.f=e.c("126");C.h=e.c("102");C.m=e.c("44");C.f=e.c("126");D.h=e.c("102");D.m=e.c("28");D.f=e.c("126");E.h=e.c("102");E.m=e.c("16");E.f=e.c("126");F.h=e.c("102");
F.m=e.c("16");F.f=e.c("126");G.h=e.c("102");G.m=e.c("12");G.f=e.c("126");
var H=e.c("16"),J=e.c("572"),qa=e.c("434"),ra=e.c("319"),sa=e.c("572"),ta=e.c("572"),ua=e.c("572"),va=e.c("434"),wa=e.c("319"),xa=e.c("126"),ya=e.c("126"),za=e.c("126"),
Aa=e.c("126"),Ba=e.c("126"),Ca=e.c("126"),Da=e.c("126"),Ea=e.c("15"),Fa=e.c("15"),K=e.c("15"),Ga=e.c("15"),Ha=e.c("6"),Ia=e.c("6"),Ja=e.c("6"),
Ka=e.c("44"),La=e.c("44"),Ma=e.c("44"),Na=e.c("28"),Oa=e.c("16"),Pa=e.c("16"),Qa=e.c("12"),Ra=e.c("30"),Sa=e.c("236"),Ta=e.c("304"),Ua=e.c("35");e.a("1");
var Va=e.c("980"),Wa="gb gbq gbu gbzw gbpr gbq2 gbqf gbqff gbq3 gbq4 gbq1 gbqlw gbql gbx1 gbx2 gbx3 gbx4 gbg1 gbg3 gbg4 gbd1 gbd3 gbd4 gbs gbwc gbprc".split(" "),M=["gbzw"],Q=e.a(""),Xa=e.a(""),R=[],U=!0,W=function(b){try{a.close();var d=e.c("27");"xxl"==b?(V("gbexxl"),d=e.c("27")):"xl"==b?(V("gbexl"),d=e.c("27")):"lg"==b?(V(""),d=
e.c("27")):"md"==b?(V("gbem"),d=e.c("27")):"sm"==b?V("gbes"):"ty"==b?V("gbet"):"ut"==b&&V("gbeu");if(window.opera){var f=M.length;for(b=0;b<f;b++){var c=v(M[b]);if(c){var q=c.style.display;c.style.display="none";b+=0*c.clientHeight;c.style.display=q}}}a.sps(d)}catch(r){u(r,"stem")}},Ya=w(W,"xxl"),Za=w(W,"xl"),$a=w(W,"lg"),ab=w(W,"md"),bb=w(W,"sm"),cb=w(W,"ty"),db=w(W,"ut"),Y=function(b){try{W(b);var d=e.j("Height"),f=e.j("Width"),
c=C;switch(b){case "ut":c=G;break;case "ty":c=F;break;case "sm":c=E;break;case "md":c=D;break;case "lg":c=C;break;case "xl":c=B;break;case "xxl":c=A}eb(d,f,b,c);X()}catch(q){u(q,"seme")}},fb=function(b){try{R.push(b)}catch(d){u(d,"roec")}},gb=function(){if(U)try{for(var b=0,d;d=R[b];++b)d(k)}catch(f){u(f,"eoec")}},hb=function(b){try{return U=b}catch(d){u(d,"ear")}},ib=function(){var b=e.j("Height"),d=e.j("Width"),f=C,c="lg";if(d<pa&&Q)c="ut",f=G;else if(d<oa&&Q)c="ty",f=F;else if(d<na||b<ja)c="sm",
f=E;else if(d<ma||b<ia)c="md",f=D;Xa&&(d>la&&b>ca&&(c="xl",f=B),d>ka&&b>ba&&(c="xxl",f=A));eb(b,d,c,f);return c},X=function(){try{var b=v("gbx1");if(b){var d=a.rtl(v("gb")),f=b.clientWidth,b=f<=Va,c=v("gb_70"),q=v("gbg4"),r=v("gbg6")||q;if(!x)if(c)x=c.clientWidth;else if(r)x=r.clientWidth;else return;if(!y){var s=v("gbg3");s&&(y=s.clientWidth)}var N=k.mo,t,m,l;switch(N){case "xxl":t=Ka;m=Ea;l=xa;break;case "xl":t=La;m=Fa;l=ya;break;case "md":t=Na;m=Ga;l=Aa;break;case "sm":t=Oa-H;m=Ha;l=Ba;break;case "ty":t=
Pa-H;m=Ia;l=Ca;break;case "ut":t=Qa-H;m=Ja;l=Da;break;default:t=Ma,m=K,l=za}var p=a.snw&&a.snw();p&&(l+=p+m);var p=x,z=v("gbg1");z&&(p+=z.clientWidth+m);(s=v("gbg3"))&&(p+=y+m);var S=v("gbgs4dn");q&&!S&&(p+=q.clientWidth+m);var da=v("gbd4"),T=v("gb_71");T&&!da&&(p+=T.clientWidth+m+K);p=Math.min(Ta,p);l+=t;var O=v("gbqfbw"),I=v("gbq4");I&&(l+=I.offsetWidth);O&&(O.style.display="",l+=O.clientWidth+Ra);var I=f-l,ea=v("gbqf"),fa=v("gbqff"),h=a.gpcc&&a.gpcc();if(ea&&fa&&!h){h=f-p-l;switch(N){case "ut":h=
Math.min(h,wa);h=Math.max(h,ra);break;case "ty":h=Math.min(h,va);h=Math.max(h,qa);break;case "xl":h=Math.min(h,ua);h=Math.max(h,J);break;case "xxl":h=Math.min(h,ta);h=Math.max(h,J);break;default:h=Math.min(h,sa),h=Math.max(h,J)}ea.style.maxWidth=h+"px";fa.style.maxWidth=h+"px";I-=h}var g=v("gbgs3");if(g){var N=I<=Sa,ga=a.cc(g,"gbsbc");N&&!ga?(a.ca(g,"gbsbc"),a.close()):!N&&ga&&(a.cr(g,"gbsbc"),a.close())}g=I;z&&(z.style.display="",g-=z.clientWidth+m);s&&(s.style.display="",g-=s.clientWidth+m);q&&
!S&&(g-=q.clientWidth+m);T&&!da&&(g-=T.clientWidth+m+K);var q=S?0:Ua,P=S||v("gbi4t");if(P&&!c){g>q?(P.style.display="",P.style.maxWidth=g+"px"):P.style.display="none";r&&(r.style.width=g<x&&g>q?g+"px":"");var ha=v("gbgs4d"),r="left";x>g^d&&(r="right");P.style.textAlign=r;ha&&(ha.style.textAlign=r)}s&&0>g&&(g+=s.clientWidth,s.style.display="none");z&&0>g&&(g+=z.clientWidth,z.style.display="none");if(O&&(0>g||c&&g<c.clientWidth))O.style.display="none";var c=d?"right":"left",d=d?"left":"right",L=v("gbu"),
lb=""!=L.style[c];b?(L.style[c]=f-L.clientWidth-t+"px",L.style[d]="auto"):(L.style[c]="",L.style[d]="");b!=lb&&a.swsc&&a.swsc(b)}}catch(mb){u(mb,"cb")}},eb=function(b,d,f,c){k={};k.mo=f;k.vh=b;k.vw=d;k.es=c;f!=n&&(gb(),e.f&&e.f())},jb=function(b){A.h+=b;B.h+=b;C.h+=b;D.h+=b;E.h+=b;F.h+=b;G.h+=b},kb=function(){return k},nb=function(){try{if(!0==U){var b=n;n=ib();if(b!=n)switch(n){case "ut":db();break;case "ty":cb();break;case "sm":bb();break;case "md":ab();break;case "xl":Za();break;case "xxl":Ya();
break;default:$a()}}X();var d=v("gb");if(d){var f=d.style.opacity;d.style.opacity=".99";for(b=0;1>b;b++)b+=0*d.offsetWidth;d.style.opacity=f}}catch(c){u(c,"sem")}},V=function(b){var d=v("gb");d&&Z(d,"gbexxli gbexli  gbemi gbesi gbeti gbeui".split(" "));for(var d=[],f=0,c;c=Wa[f];f++)if(c=v(c)){switch(b){case "gbexxl":Z(c,"gbexl  gbem gbes gbet gbeu".split(" "));a.ca(c,b);break;case "gbexl":Z(c,"gbexxl  gbem gbes gbet gbeu".split(" "));a.ca(c,b);break;case "":Z(c,"gbexxl gbexl gbem gbes gbet gbeu".split(" "));
a.ca(c,b);break;case "gbem":Z(c,"gbexxl gbexl  gbes gbet gbeu".split(" "));a.ca(c,b);break;case "gbes":Z(c,"gbexxl gbexl  gbem gbet gbeu".split(" "));a.ca(c,b);break;case "gbet":Z(c,"gbexxl gbexl  gbem gbes gbeu".split(" "));a.ca(c,b);break;case "gbeu":Z(c,"gbexxl gbexl  gbem gbes gbet".split(" ")),a.ca(c,b)}d.push(c)}return d},Z=function(b,d){for(var f=0,c=d.length;f<c;++f)d[f]&&a.cr(b,d[f])},ob=function(){try{if(!0==U)switch(ib()){case "ut":$("gbeui");break;case "ty":$("gbeti");break;case "sm":$("gbesi");
break;case "md":$("gbemi");break;case "xl":$("gbexli");break;case "xxl":$("gbexxli");break;default:$("")}X()}catch(b){u(b,"semol")}},$=function(b){var d=v("gb");d&&a.ca(d,b)};a.eli=ob;a.elg=nb;a.elxxl=w(Y,"xxl");a.elxl=w(Y,"xl");a.ell=w(Y,"lg");a.elm=w(Y,"md");a.els=w(Y,"sm");a.elr=kb;a.elc=fb;a.elx=gb;a.elh=jb;a.ela=hb;a.elp=X;a.upel=w(Y,"lg");a.upes=w(Y,"md");a.upet=w(Y,"sm");ob();nb();a.mcf("el",{});}catch(e){window.gbar&&gbar.logger&&gbar.logger.ml(e,{"_sn":"cfg.init"});}})();
(function(){try{var a=window.gbar;var d=function(){return document.getElementById("gbqfqw")},h=function(){return document.getElementById("gbqfq")},k=function(){return document.getElementById("gbqf")},l=function(){return document.getElementById("gbqfb")},n=function(b){var c=document.getElementById("gbqfaa");c.appendChild(b);m()},p=function(b){var c=document.getElementById("gbqfab");c.appendChild(b);m()},m=function(){var b=document.getElementById("gbqfqwb");if(b){var c=document.getElementById("gbqfaa"),e=document.getElementById("gbqfab");
if(c||e){var f="left",g="right";a.rtl(document.getElementById("gb"))&&(f="right",g="left");c&&(b.style[f]=c.offsetWidth+"px");e&&(b.style[g]=e.offsetWidth+"px")}}},q=function(b){a.qm(function(){a.qfhi(b)})};a.qfgw=d;a.qfgq=h;a.qfgf=k;a.qfas=n;a.qfae=p;a.qfau=m;a.qfhi=q;a.qfsb=l;}catch(e){window.gbar&&gbar.logger&&gbar.logger.ml(e,{"_sn":"cfg.init"});}})();
(function(){try{var c=window.gbar.i.i;var e=window.gbar;var f="gbq1 gbq2 gbpr gbqfbwa gbx1 gbx2".split(" "),h=function(b){var a=document.getElementById("gbqld");if(a&&(a.style.display=b?"none":"block",a=document.getElementById("gbql")))a.style.display=b?"block":"none"},k=function(){try{for(var b=0,a;a=f[b];b++){var d=document.getElementById(a);d&&e.ca(d,"gbqfh")}e.elp&&e.elp();h(!0)}catch(g){c(g,"gas","ahcc")}},l=function(){try{for(var b=0,a;a=f[b];b++){var d=document.getElementById(a);d&&e.cr(d,"gbqfh")}e.elp&&e.elp();h(!1)}catch(g){c(g,"gas","rhcc")}},
m=function(){try{var b=document.getElementById(f[0]);return b&&e.cc(b,"gbqfh")}catch(a){c(a,"gas","ih")}};e.gpca=k;e.gpcr=l;e.gpcc=m;}catch(e){window.gbar&&gbar.logger&&gbar.logger.ml(e,{"_sn":"cfg.init"});}})();
(function(){try{var b=window.gbar.i.i;var c=window.gbar;var f=function(d){try{var a=document.getElementById("gbom");a&&d.appendChild(a.cloneNode(!0))}catch(e){b(e,"omas","aomc")}};c.aomc=f;}catch(e){window.gbar&&gbar.logger&&gbar.logger.ml(e,{"_sn":"cfg.init"});}})();
(function(){try{var a=window.gbar;a.bcf("cp",{url:"//ssl.gstatic.com/gb/js/abc/cpw2_c94afff183bcdf611d0d2e035b8d4a38.js",libs:"iframes-styles-slide-menu"});a.mcf("cp",{scheme:"https",host:"plus.google.com",port:"",path:"/u/0/_/socialgraph/circlepicker/menu",query:"hl=en"});
var d=function(b){var c=function(){a.cp&&a.cp.bl?b&&b():(b&&a.aq("cp",b),window.setTimeout(function(){a.lb("cp")},0))},e=function(){a.lGC&&a.lGC(c)};a.mdi.wg?e():a.qm(e)},f=function(b){var c=(new Date).getTime();d(function(){a.cp.me(b,c)})},g=function(b){d(function(){a.cp.ml(b)})},h=function(b){var c=(new Date).getTime();d(function(){a.cp.c!=h&&a.cp.c(b,c)})},k=function(b,c){d(function(){a.cp.rc(b,c)})},l=function(b,c,e){d(function(){a.cp.rel(b,c,e)})},m={l:d,me:f,ml:g,c:h,rc:k,rel:l,bl:!1};
a.cp=m;}catch(e){window.gbar&&gbar.logger&&gbar.logger.ml(e,{"_sn":"cfg.init"});}})();
(function(){try{var a=window.gbar;a.mcf("pm",{p:""});}catch(e){window.gbar&&gbar.logger&&gbar.logger.ml(e,{"_sn":"cfg.init"});}})();
(function(){try{var a=window.gbar;a.mcf("mm",{s:"1"});}catch(e){window.gbar&&gbar.logger&&gbar.logger.ml(e,{"_sn":"cfg.init"});}})();
(function(){try{var a=window.gbar;var b=a.i;a.bcf("op",{url:"//ssl.gstatic.com/gb/js/om_3dc3f8231b9e7ee0ea7ef7d066036e45.js"});var d=function(c){b.d(c);a.aq("op",function(){b.e()});c=function(){a.lb("op")};a.mdi.wg?c():a.qm(c)};b.e=d;a.mcf("op",{l:"en",u:b.c("0",0)});}catch(e){window.gbar&&gbar.logger&&gbar.logger.ml(e,{"_sn":"cfg.init"});}})();
(function(){try{var d=window.gbar.i.i;var e=window.gbar;var f=e.i;var g=f.c("1",0),h=/\bgbmt\b/,k=function(a){try{var b=document.getElementById("gb_"+g),c=document.getElementById("gb_"+a);b&&f.l(b,h.test(b.className)?"gbm0l":"gbz0l");c&&f.k(c,h.test(c.className)?"gbm0l":"gbz0l")}catch(l){d(l,"sj","ssp")}g=a},m=e.qs,n=function(a){var b;b=a.href;var c=window.location.href.match(/.*?:\/\/[^\/]*/)[0],c=RegExp("^"+c+"/search\\?");if((b=c.test(b))&&!/(^|\\?|&)ei=/.test(a.href))if((b=window.google)&&b.kEXPI)a.href+="&ei="+b.kEI},p=function(a){m(a);
n(a)},q=function(){if(window.google&&window.google.sn){var a=/.*hp$/;return a.test(window.google.sn)?"":"1"}return"-1"};e.rp=q;e.slp=k;e.qs=p;e.qsi=n;}catch(e){window.gbar&&gbar.logger&&gbar.logger.ml(e,{"_sn":"cfg.init"});}})();
(function(){try{var a=window.gbar;a.mcf("cm",{e:"1",s:"https",h:"plus.google.com",po:"",gpa:"/u/0/_/og/storage/get",spa:"/u/0/_/og/storage/set",rpa:"/u/0/_/og/storage/remove",q:"sourceid=1",guo:"",suo:"",ruo:"",srlo:"",klo:"[[]]",
rs:"0.01"});}catch(e){window.gbar&&gbar.logger&&gbar.logger.ml(e,{"_sn":"cfg.init"});}})();
(function(){try{window.gbar.rdl();}catch(e){window.gbar&&gbar.logger&&gbar.logger.ml(e,{"_sn":"cfg.init"});}})();
(window['gbar']=window['gbar']||{})._CONFIG=[[[0,"www.gstatic.com","og.og.en_US.fH3Dx4xMBcw.O","com","en","1",0,["2","2",".49.65.51.40.70.46.55.36.","r_cp.r_qf.","31215","1374518932","1374081516"],"35972","3wXyUfSDM4LC4APHsoGYDQ",0,0,"og.og.-1xvvehcp1dp6o.L.W.O","AItRSTPkaqCDq-UZ2qqaNF_F4noVBkAoaA","AItRSTOTbv3INx5ORL7sB-Ces25exX2J_Q"],["","https","plus.google.com","","/u/0/_/notifications/frame","sourceid=1","pid=1","en",5],0,["m;/_/scs/abc-static/_/js/k=gapi.gapi.en.a0irxetnvx4.O/m=__features__/am=EA/rt=j/d=1/rs=AItRSTMM3Tduq30stOAPMPXAx0A6ctSSsg","https://apis.google.com","","1","1","","APfa0bqc1ez4qedLw4lOPVTFRprQGB-q1VC54rLuWcGNsa7O3vK3MBLWYhEUa7NHNPmOpdQtTpSYtwRzwBWaWndeqoCEpfbZxQ==",1,"es_plusone_gc_20130717.0_p0"],["1","gci_91f30755d6a6b787dcc2a4062e6e9824.js","googleapis.client:plusone","0","en"],["1","iframes-styles-slide-menu","https","plus.google.com","","/u/0/_/socialgraph/circlepicker/menu","hl=en"],[100,"Notifications",1,"https","plus.google.com","",0,"","POST","/u/0/_/n/gcosuc",3000,0],[100,"Share"],["0.01","com","1",[["","",""],"","w",["","",""]],[["","",""],"",["","",""],0,0]],["%1$s (default)","Google+ page",0,"%1$s (delegated)"],[1,1,1,0,"0"],[1,"0.001","0.01"],[1,"0.1"],[],[],[],[[""],[""]]]];window.gbar&&gbar.pw&&gbar.pw.su&&gbar.pw.su(true);</script> </head><body class="hp" onload="try{if(!google.j.b){document.f&amp;&amp;document.f.q.focus();document.gbqf&amp;&amp;document.gbqf.q.focus();}}catch(e){}if(document.images)new Image().src='/images/nav_logo132.png'" alink="#dd4b39" bgcolor="#fff" id="gsr" link="#12c" text="#222" vlink="#61c"><div id="pocs" style="display:none;position:absolute"><div id="pocs0"><span><span>Google</span> Instant is unavailable. Press Enter to search.</span>&nbsp;<a href="/support/websearch/bin/answer.py?answer=186645&amp;form=bb&amp;hl=en">Learn more</a></div><div id="pocs1"><span>Google</span> Instant is off due to connection speed. Press Enter to search.</div><div id="pocs2">Press Enter to search.</div></div><div id="cst"><div style="display:none">&nbsp;</div></div> <a href="/setprefs?suggon=2&amp;prev=https://www.google.com/&amp;sig=0_w5Y3j2sjo4QHU2W0ggSqV-iPr9Q%3D" style="left:-1000em;position:absolute">Screen reader users, click here to turn off Google Instant.</a>  <textarea name="csi" id="csi" style="display:none"></textarea><script>if(google.j.b)document.body.style.visibility='hidden';</script><div id="mngb"><div id=gb><script>window.gbar&&gbar.eli&&gbar.eli()</script><div id=gbw><div id=gbzw><div id=gbz><span class=gbtcb></span><ol id=gbzc class=gbtc><li class=gbt><a onclick=gbar.logger.il(1,{t:119}); class=gbzt id=gb_119 href="https://plus.google.com/u/0/?tab=wX"><span class=gbtb2></span><span class=gbts>+Matthew</span></a></li><li class=gbt><a onclick=gbar.logger.il(1,{t:1}); class="gbzt gbz0l gbp1" id=gb_1 href="https://www.google.com/webhp?hl=en&tab=ww"><span class=gbtb2></span><span class=gbts>Search</span></a></li><li class=gbt><a onclick=gbar.qs(this);gbar.logger.il(1,{t:2}); class=gbzt id=gb_2 href="https://www.google.com/imghp?hl=en&tab=wi"><span class=gbtb2></span><span class=gbts>Images</span></a></li><li class=gbt><a onclick=gbar.logger.il(1,{t:23}); class=gbzt id=gb_23 href="https://mail.google.com/mail/?tab=wm"><span class=gbtb2></span><span class=gbts>Mail</span></a></li><li class=gbt><a onclick=gbar.logger.il(1,{t:25}); class=gbzt id=gb_25 href="https://drive.google.com/?tab=wo&authuser=0"><span class=gbtb2></span><span class=gbts>Drive</span></a></li><li class=gbt><a onclick=gbar.logger.il(1,{t:24}); class=gbzt id=gb_24 href="https://www.google.com/calendar?tab=wc"><span class=gbtb2></span><span class=gbts>Calendar</span></a></li><li class=gbt><a onclick=gbar.logger.il(1,{t:38}); class=gbzt id=gb_38 href="https://sites.google.com/?tab=w3"><span class=gbtb2></span><span class=gbts>Sites</span></a></li><li class=gbt><a onclick=gbar.qs(this);gbar.logger.il(1,{t:3}); class=gbzt id=gb_3 href="https://groups.google.com/grphp?hl=en&tab=wg"><span class=gbtb2></span><span class=gbts>Groups</span></a></li><li class=gbt><a onclick=gbar.logger.il(1,{t:53}); class=gbzt id=gb_53 href="https://www.google.com/contacts/?hl=en&tab=wC"><span class=gbtb2></span><span class=gbts>Contacts</span></a></li><li class=gbt><a class=gbgt id=gbztm href="http://www.google.com/intl/en/options/" onclick="gbar.tg(event,this)" aria-haspopup=true aria-owns=gbd><span class=gbtb2></span><span id=gbztms class="gbts gbtsa"><span id=gbztms1>More</span><span class=gbma></span></span></a><div class=gbm id=gbd aria-owner=gbztm><div id=gbmmb class="gbmc gbsb gbsbis"><ol id=gbmm class="gbmcc gbsbic"><li class=gbmtc><a onclick=gbar.qs(this);gbar.logger.il(1,{t:8}); class=gbmt id=gb_8 href="https://maps.google.com/maps?hl=en&tab=wl">Maps</a></li><li class=gbmtc><a onclick=gbar.qs(this);gbar.logger.il(1,{t:36}); class=gbmt id=gb_36 href="https://www.youtube.com/?tab=w1">YouTube</a></li><li class=gbmtc><a onclick=gbar.logger.il(1,{t:5}); class=gbmt id=gb_5 href="https://news.google.com/nwshp?hl=en&tab=wn">News</a></li><li class=gbmtc><a onclick=gbar.qs(this);gbar.logger.il(1,{t:51}); class=gbmt id=gb_51 href="https://translate.google.com/?hl=en&tab=wT">Translate</a></li><li class=gbmtc><a onclick=gbar.logger.il(1,{t:17}); class=gbmt id=gb_17 href="http://www.google.com/mobile/?hl=en&tab=wD">Mobile</a></li><li class=gbmtc><a onclick=gbar.qs(this);gbar.logger.il(1,{t:10}); class=gbmt id=gb_10 href="http://books.google.com/bkshp?hl=en&tab=wp">Books</a></li><li class=gbmtc><a onclick=gbar.logger.il(1,{t:172}); class=gbmt id=gb_172 href="https://www.google.com/offers?utm_source=xsell&utm_medium=products&utm_campaign=sandbar&hl=en&tab=wG">Offers</a></li><li class=gbmtc><a onclick=gbar.logger.il(1,{t:212}); class=gbmt id=gb_212 href="https://wallet.google.com/manage/?tab=wa">Wallet</a></li><li class=gbmtc><a onclick=gbar.qs(this);gbar.logger.il(1,{t:6}); class=gbmt id=gb_6 href="http://www.google.com/shopping?hl=en&tab=wf">Shopping</a></li><li class=gbmtc><a onclick=gbar.logger.il(1,{t:30}); class=gbmt id=gb_30 href="https://www.blogger.com/?tab=wj">Blogger</a></li><li class=gbmtc><a onclick=gbar.qs(this);gbar.logger.il(1,{t:27}); class=gbmt id=gb_27 href="https://www.google.com/finance?tab=we">Finance</a></li><li class=gbmtc><a onclick=gbar.qs(this);gbar.logger.il(1,{t:31}); class=gbmt id=gb_31 href="https://plus.google.com/u/0/photos?tab=wq">Photos</a></li><li class=gbmtc><div class="gbmt gbmh"></div></li><li class=gbmtc><a onclick=gbar.logger.il(1,{t:66}); href="http://www.google.com/intl/en/options/" class=gbmt>Even more</a></li></ol><div class=gbsbt></div><div class=gbsbb></div></div></div></li></ol></div></div><div id=gbq><div id=gbq1 class="gbt gbqfh"><a class=gbqla href="/webhp?hl=en&tab=ww" onclick="gbar.logger.il(39)" title="Go to Google Home"><table id=gbqlt><tr><td id=gbqlw class=gbgt><span id=gbql></span></td></tr></table><div class=gbqlca></div></a></div><div id=gbq2 class="gbt gbqfh"><div id=gbqfw ><form id=gbqf name=gbqf method=get action="/search" onsubmit="gbar.logger.il(31);"><fieldset class=gbxx><legend class=gbxx>Hidden fields</legend><div id=gbqffd><input type=hidden name="safe" value="off"><input type=hidden name="output" value="search"><input type=hidden name="sclient" value="psy-ab"></div></fieldset><fieldset class=gbqff id=gbqff><legend class=gbxx></legend><div id=gbfwa class="gbqfwa "><div id=gbqfqw class=gbqfqw><div id=gbqfqwb class=gbqfqwc><input id=gbqfq class=gbqfif name=q type=text autocomplete=off value="" ></div></div></div></fieldset><div id=gbqfbw><button id=gbqfb aria-label="Google Search" class=gbqfb name=btnG><span class=gbqfi></span></button></div><div id=gbqfbwa class=jsb><button id=gbqfba aria-label="Google Search" name=btnK class=gbqfba><span id=gbqfsa>Google Search</span></button><button id=gbqfbb aria-label="I'm Feeling Lucky" name=btnI class="gbqfba" onclick="google.x(this,function() {google.ifl && google.ifl.o();})"><span id=gbqfsb>I'm Feeling Lucky</span></button></div></form></div></div></div><div id=gbu><div id=gbvg class=gbvg><h2 class=gbxx>Account Options</h2><span class=gbtcb></span><ol class="gbtc gbsr"><li class=gbt><a class=gbgt id=gbg6 href="https://plus.google.com/u/0/me?tab=wX" onclick="gbar.tg(event,document.getElementById('gbg4'))" tabindex=-1 aria-haspopup=true aria-owns=gbd4><span id=gbi4t>mthode@mthode.org</span></a></li><li class="gbt gbtn"><a class="gbgt gbgtd gbnh gb_gbnh " id=gbg1 href="https://plus.google.com/u/0/notifications/all?hl=en" title="Notifications" onclick="gbar.tg(event,this)" aria-haspopup=true aria-owns=gbd1><span id=gbgs1 class=gbg1t><span id=gbi1a class=gbid></span><span id=gbi1 class=gbids>&nbsp;</span></span><span class=gbmab></span><span class=gbmac></span></a><div id=gbd1 class="gbm gbmsgo" aria-owner=gbg1><div class=gbmc></div><div class=gbmsg></div></div></li><li class="gbt gbtsb"><a class=gbgt id=gbg3 href="https://plus.google.com/u/0/stream/all?hl=en" onclick="gbar.tg(event,this)" aria-haspopup=true aria-owns=gbd3><div id=gbgs3><span class=gbmab></span><span class=gbmac></span><span id=gbgsi></span><span id=gbgss>&nbsp;</span><span id=gbi3>Share</span><span id=gbgsa></span></div></a><div class="gbm gbmsgo" id=gbd3 aria-owner=gbg3><div class=gbmc></div><div class=gbmsg></div></div></li><li class=gbt guidedhelpid=gbacsw><a class="gbgt gbg4p" id=gbg4 href="https://plus.google.com/u/0/me?tab=wX" onclick="gbar.tg(event,this)" aria-haspopup=true aria-owns=gbd4><span id=gbgs4><img id=gbi4i width=27 height=27 onerror="window.gbar&&gbar.pge?gbar.pge():this.loadError=1;" src="//lh3.googleusercontent.com/-6D9GO1MSV4k/AAAAAAAAAAI/AAAAAAAAAAA/6WbMvdKKTUk/s27-c/photo.jpg" alt="Matthew Thode"><img id=gbi4ip style=display:none><span id=gbi4id style="display:none"></span><span class=gbmai></span><span class=gbmab></span><span class=gbmac></span></span></a><div class=gbm id=gbd4 aria-owner=gbg4 guidedhelpid=gbd4><div class=gbmc><div id=gbmpdv><div class=gbpmc><table id=gbpm><tr><td class=gbpmtc><div id=gbpms>This account is managed by <span class=gbpms2>mthode.org</span>.</div> <a target=_blank class=gbml1 href="http://www.google.com/support/accounts/bin/answer.py?answer=181692&hl=en">Learn more</a></td></tr></table></div><div id=gbmpiw><a class="gbmpiaa gbp1" onclick="gbar.logger.il(10,{t:146})" href="https://plus.google.com/u/0/me?tab=wX"><span id=gbmpid style="display:none"></span><img id=gbmpi width=96 height=96 onerror="window.gbar&&gbar.ppe?gbar.ppe():this.loadError=1;" src="//lh3.googleusercontent.com/-6D9GO1MSV4k/AAAAAAAAAAI/AAAAAAAAAAA/6WbMvdKKTUk/s27-c/photo.jpg" alt="Matthew Thode"></a><span id=gbmpicb><span class=gbxv>Change photo</span></span><a href="https://plus.google.com/u/0/me?tab=wX" id=gbmpicp onclick="gbar.i.e(event)">Change photo</a></div><div class=gbpc><span id=gbmpn class=gbps onclick="gbar.logger.il(10,{t:69})">Matthew Thode</span><span class=gbps2>mthode@mthode.org</span><div class=gbmlbw><a id=gb_156 onclick="gbar.logger.il(10,{t:156})" href="https://www.google.com/settings?ref=home" class=gbmlb>Account</a>&ndash;<a onclick="gbar.logger.il(10,{t:156})" href="https://www.google.com/settings/privacy" class=gbmlb>Privacy</a></div><a role=button id=gbmplp onclick="gbar.logger.il(10,{t:146})" href="https://plus.google.com/u/0/me?tab=wX" class="gbqfb gbiba gbp1">View profile</a></div></div><div id=gbmps><div id=gbmpasb class='gbsb gbsbis'><div id=gbmpas class=gbsbic><div id=gbmpm_0 class="gbmtc gbp0"><a id=gbmpm_0_l href="https://www.google.com/webhp?authuser=0" class=gbmt><span class="gbmpiaw gbxv"><img class=gbmpia width=48 height=48 onerror="window.gbar&&gbar.pae?gbar.pae(this):this.loadError=1;" data-asrc="//lh3.googleusercontent.com/-6D9GO1MSV4k/AAAAAAAAAAI/AAAAAAAAAAA/6WbMvdKKTUk/s48-c/photo.jpg" alt="Matthew Thode"></span><span class=gbmpnw><span class=gbps>Matthew Thode</span><span class=gbps2>mthode@mthode.org</span></span></a></div></div><div class=gbsbt></div><div class=gbsbb></div></div><div id=gbmppc class="gbxx gbmtc"><a class=gbmt href="https://plus.google.com/u/0/dashboard"><span class=gbmppci></span>All your Google+ pages &rsaquo;</a></div></div><table id=gbmpal><tr><td class=gbmpala><a role=button href="https://accounts.google.com/AddSession?hl=en&continue=https://www.google.com/" class=gbqfbb>Add account</a></td><td class=gbmpalb><a target=_top role=button id=gb_71 onclick="gbar.logger.il(9,{l:'o'})" href="https://accounts.google.com/Logout?hl=en&continue=https://www.google.com/" class=gbqfbb>Sign out</a></td></tr></table></div></div></li><noscript><li class=gbt><a id=gbg7 href="https://accounts.google.com/Logout?hl=en&continue=https://www.google.com/" class=gbgt><span class=gbgs><span class=gbit>Sign out</span></span></a></li></noscript><div style="display:none"><div class=gbm id=gbd5 aria-owner=gbg5><div class=gbmc><ol id=gbom class=gbmcc><li class="gbkc gbmtc"><a  class=gbmt href="/preferences?hl=en">Search settings</a></li><li class=gbmtc><div class="gbmt gbmh"></div></li><li class="gbe gbmtc"><a  id=gmlas class=gbmt href="/advanced_search?hl=en">Advanced search</a></li><li class="gbe gbmtc"><a  class=gbmt href="/language_tools?hl=en">Language tools</a></li><li class=gbmtc><div class="gbmt gbmh"></div></li><li class="gbkp gbmtc"><a class=gbmt href="https://www.google.com/history/?hl=en">Web History</a></li></ol></div></div></div></ol><div id=gbdw></div></div></div></div><div id=gbx1 class="gbqfh"></div><div id=gbx3></div><div id=gbbw><div id=gbb></div></div><script>window.gbar&&gbar.elp&&gbar.elp()</script></div><div id="iflved" data-ved="0CAMQnRs" style="display:none"></div></div><textarea name="wgjc" id="wgjc" style="display:none"></textarea><textarea name="wgjs" id="wgjs" style="display:none"></textarea><textarea name="wgju" id="wgju" style="display:none"></textarea><textarea name="hcache" id="hcache" style="display:none"></textarea><div id="main"><span class="ctr-p" id="body"><center><div id="lga" style="height:231px;margin-top:-22px"><img alt="Google" height="95" src="/images/srpr/logo4w.png" width="275" id="hplogo" onload="window.lol&&lol()" style="padding-top:112px"></div><div style="height:102px"></div><div id="prm-pt" style="font-size:83%;min-height:3.5em"><br><script>window.gbar&&gbar.up&&gbar.up.tp&&gbar.up.tp();</script></div></center></span><div class="ctr-p" id="footer"><div><div id="ftby"><div id="fll"><div id="flls"><a href="/intl/en/ads/">Advertising&nbsp;Programs</a>‎<a href="/services/">Business Solutions</a>‎<a href="/intl/en/policies/">Privacy & Terms</a>‎</div><div id="flrs"><a href="https://plus.google.com/116899029375914044550" rel="publisher">+Google</a>‎<a href="/intl/en/about.html">About Google</a>‎</div></div><div id="flci"></div></div></div></div></div><script>(function(){var _co='[\x22body\x22,\x22footer\x22,\x22xjsi\x22]';var _mstr='\x3cspan class\x3dctr-p id\x3dbody\x3e\x3c/span\x3e\x3cspan class\x3dctr-p id\x3dfooter\x3e\x3c/span\x3e\x3cspan id\x3dxjsi\x3e\x3c/span\x3e';function _gjp(){!(location.hash && _gjuc())&& setTimeout(_gjp,500);}
var _coarr = eval('(' + _co + ')');google.j[1]={cc:[],co:_coarr,bl:['mngb','gb_'],funcs:[
{'n':'pcs','i':'gstyle','css':document.getElementById('gstyle').innerHTML,'is':'','r':true,'sc':true},{'n':'pc','i':'cst','h':document.getElementById('cst').innerHTML,'is':'','r':true,'sc':true},{'n':'pc','i':'main','h':_mstr,'is':'','r':true,'sc':true}]
};})();</script><script data-url="/extern_chrome/5f676f190f2104e2.js?bav=or.r_qf" id="ecs">function wgjp(){var xjs=document.createElement('script');xjs.src=document.getElementById('ecs').getAttribute('data-url');(document.getElementById('xjsd')|| document.body).appendChild(xjs);};</script><div id=xjsd></div><div id=xjsi><script>if(google.y)google.y.first=[];(function(){function b(a){window.setTimeout(function(){var c=document.createElement("script");c.src=a;document.getElementById("xjsd").appendChild(c)},0)}google.dljp=function(a){google.xjsu=a;b(a)};google.dlj=b;})();
if(!google.xjs){window._=window._||{};window._._DumpException=function(e){throw e};if(google.timers&&google.timers.load.t){google.timers.load.t.xjsls=new Date().getTime();}google.dljp('/xjs/_/js/k\x3dxjs.s.en_US.MpiVkF51mpA.O/m\x3dc,sb_sri,cr,jp,r,hsm,j,p,pcc,csi/am\x3dECY/rt\x3dj/d\x3d1/sv\x3d1/rs\x3dAItRSTPRuFntKU9uIBZE-i038GblYo37eQ');google.xjs=1;}google.pmc={"c":{},"sb":{"agen":false,"cgen":true,"client":"hp","dh":true,"ds":"","eqch":true,"fl":true,"host":"google.com","jsonp":true,"lyrs":29,"msgs":{"dym":"Did you mean:","lcky":"I\u0026#39;m Feeling Lucky","lml":"Learn more","oskt":"Input tools","psrc":"This search was removed from your \u003Ca href=\"/history\"\u003EWeb History\u003C/a\u003E","psrl":"Remove","sbit":"Search by image","srae":"Please check your microphone.  \u003Ca href=\"https://support.google.com/chrome/?p=ui_voice_search\" target=\"_blank\"\u003ELearn more\u003C/a\u003E","srch":"Google Search","sril":"en_US","srim":"Click \u003Cb\u003EAllow\u003C/b\u003E to start voice search","sriw":"Waiting...","srlm":"Listening...","srlu":"%1$s voice search not available","srne":"No Internet connection","srnt":"Didn't get that. \u003Ca href=\"#\"\u003ETry again\u003C/a\u003E","srnv":"Please check your microphone and audio levels.  \u003Ca href=\"https://support.google.com/chrome/?p=ui_voice_search\" target=\"_blank\"\u003ELearn more\u003C/a\u003E","srpe":"Voice search has been turned off.  \u003Ca href=\"https://support.google.com/chrome/?p=ui_voice_search\" target=\"_blank\"\u003EDetails\u003C/a\u003E","srrm":"Speak now","srtt":"Search by voice"},"ovr":{"ent":1,"l":1,"ms":1},"pq":"","psy":"p","qcpw":false,"scd":10,"sce":4,"spch":true,"sre":true,"stok":"fDnVVj0xMG7qUYr_M6Q7m2glvsI","token":"bw354juNI1mDCc0SPCUjKg"},"cr":{"eup":false,"qir":false,"rctj":true,"ref":false,"uff":false},"cdos":{"bih":997,"biw":1918,"dima":"b"},"gf":{"pid":196,"si":true},"jp":{"mcr":5},"vm":{"bv":49784469,"d":"dmg","tc":true,"te":true,"tk":true,"ts":true},"tbui":{"dfi":{"am":["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"],"df":["EEEE, MMMM d, y","MMMM d, y","MMM d, y","M/d/yyyy"],"fdow":6,"nw":["S","M","T","W","T","F","S"],"wm":["January","February","March","April","May","June","July","August","September","October","November","December"]},"g":28,"k":true,"m":{"app":true,"bks":true,"blg":true,"dsc":true,"fin":true,"flm":true,"frm":true,"isch":true,"klg":true,"map":true,"mobile":true,"nws":true,"plcs":true,"ppl":true,"prc":true,"pts":true,"rcp":true,"shop":true,"vid":true},"t":null},"sic":{},"mb":{"db":false,"m_errors":{"default":"\u003Cfont color=red\u003EError:\u003C/font\u003E The server could not complete your request.  Try again in 30 seconds."},"m_tip":"Click for more information","nlpm":"-153px -84px","nlpp":"-153px -70px","utp":true},"wobnm":{},"cfm":{"data_url":"/m/financedata?safe=off\u0026output=search\u0026source=mus"},"actn":{},"abd":{"abd":false,"dabp":false,"deb":false,"der":false,"det":false,"psa":false,"sup":false},"llc":{"carmode":"list","cns":false,"dst":3185505,"fling_time":300,"float":true,"hot":false,"ime":true,"mpi":0,"oq":"","p":false,"sticky":true,"t":false,"udp":600,"uds":600,"udt":600,"urs":false,"usr":true},"rkab":{"bl":"Feedback / More info","db":"Reported","di":"Thank you.","dl":"Report another problem","rb":"Wrong?","ri":"Please report the problem.","rl":"Cancel"},"bihu":{"MESSAGES":{"msg_img_from":"Image from %1$s","msg_ms":"More sizes","msg_si":"Similar"}},"riu":{"cnfrm":"Reported","prmpt":"Report"},"ifl":{"opts":[{"href":"/url?url=/doodles/happy-holidays-from-google-2010","id":"doodley","msg":"I'm Feeling Doodley"},{"href":"/url?url=http://www.googleartproject.com/collection/tokyo-national-museum/artwork/maple-viewers-kano-hideyori/453020/\u0026sa=t\u0026usg=AFQjCNFjg92OHE77tWUZiaAx2dl43Gk95g","id":"artistic","msg":"I'm Feeling Artistic"},{"href":"/url?url=/search?q%3Drestaurants%26tbm%3Dplcs","id":"hungry","msg":"I'm Feeling Hungry"},{"href":"/url?url=http://agoogleaday.com/%23date%3D2011-07-15\u0026sa=t\u0026usg=AFQjCNH4uOAvdBFnSR2cdquCknLiNgI-lg","id":"puzzled","msg":"I'm Feeling Puzzled"},{"href":"/url?url=/trends/hottrends","id":"trendy","msg":"I'm Feeling Trendy"},{"href":"/url?url=/earth/explore/showcase/hubble20th.html%23tab%3Dngc-602","id":"stellar","msg":"I'm Feeling Stellar"},{"href":"/url?url=/doodles/les-pauls-96th-birthday","id":"playful","msg":"I'm Feeling Playful"},{"href":"/url?url=/intl/en/culturalinstitute/worldwonders/shirakawa-go-gokayama/","id":"wonderful","msg":"I'm Feeling Wonderful"}]},"rmcl":{"bl":"Feedback / More info","db":"Reported","di":"Thank you.","dl":"Report another problem","rb":"Wrong?","ri":"Please report the problem.","rl":"Cancel"},"kp":{"use_top_media_styles":true},"rk":{"bl":"Feedback / More info","db":"Reported","di":"Thank you.","dl":"Report another problem","efe":false,"rb":"Wrong?","ri":"Please report the problem.","rl":"Cancel"},"lu":{"cm_hov":true,"tt_kft":true,"uab":true},"imap":{},"m":{"ab":{"on":true},"ajax":{"gl":"us","hl":"en","q":""},"css":{"adpbc":"#fec","adpc":"#fffbf2","def":false,"showTopNav":true},"elastic":{"js":true,"rhs4Col":1072,"rhs5Col":1160,"rhsOn":true,"tiny":false},"exp":{"lru":true,"tnav":true},"kfe":{"adsClientId":33,"clientId":29,"kfeHost":"clients1.google.com","kfeUrlPrefix":"/webpagethumbnail?r=4\u0026f=3\u0026s=400:585\u0026query=\u0026hl=en\u0026gl=us","vsH":585,"vsW":400},"msgs":{"details":"Result details","hPers":"Hide private results","hPersD":"Currently hiding private results","loading":"Still loading...","mute":"Mute","noPreview":"Preview not available","sPers":"Show all results","sPersD":"Currently showing private results","unmute":"Unmute"},"nokjs":{"on":true},"time":{"hUnit":1500}},"tnv":{"m":false,"ms":false,"t":false},"adp":{},"adsm":{},"async":{},"bds":{},"ca":{},"erh":{},"hp":{},"hv":{},"lc":{},"lor":{},"ob":{},"r":{},"sf":{},"sfa":{},"shlb":{},"srl":{},"st":{},"tbpr":{},"tr":{},"hsm":{},"j":{"cspd":0,"hme":true,"icmt":false,"mcr":5,"tct":" \\u3000?"},"p":{"ae":true,"avgTtfc":2000,"brba":false,"dlen":24,"dper":3,"eae":true,"fbdc":500,"fbdu":-1,"fbh":true,"fd":1000000,"focus":true,"ftwd":200,"gpsj":true,"hiue":true,"hpt":310,"iavgTtfc":2000,"kn":true,"knrt":true,"maxCbt":1500,"mds":"dfn,klg,prc,sp,mbl_he,mbl_hs,mbl_re,mbl_rs,mbl_sv","msg":{"dym":"Did you mean:","gs":"Google Search","kntt":"Use the up and down arrow keys to select each result. Press Enter to go to the selection.","pcnt":"New Tab","sif":"Search instead for","srf":"Showing results for"},"nprr":1,"ohpt":false,"ophe":true,"pmt":250,"pq":true,"rpt":50,"sc":"psy-ab","tdur":50,"ufl":true},"pcc":{},"csi":{"acsi":true,"cbu":"/gen_204","csbu":"/gen_204"},"SpiLtA":{},"7GvTbw":{},"/1S6iw":{},"8aqNqA":{}};google.y.first.push(function(){google.loadAll(['cdos','gf','vm','tbui','sic','mb','wobnm','cfm','actn','abd','llc','bihu','ifl','kp','lu','imap','m','tnv','adp','async','erh','hv','lc','ob','sf','sfa','srl','tbpr','tr']);if(google.med){google.med('init');google.initHistory();google.med('history');}google.History&&google.History.initialize('/');google.hs&&google.hs.init&&google.hs.init()});if(google.j&&google.j.en&&google.j.xi){window.setTimeout(google.j.xi,0);}</script></div><style id="_css0"></style><script>(function(){var b,c,d,e;function g(a,f){a.removeEventListener?(a.removeEventListener("load",f,!1),a.removeEventListener("error",f,!1)):(a.detachEvent("onload",f),a.detachEvent("onerror",f))}function h(a){e=(new Date).getTime();++c;a=a||window.event;a=a.target||a.srcElement;g(a,h)}var k=document.getElementsByTagName("img");b=k.length;
for(var l=c=0,m;l<b;++l)m=k[l],m.complete||"string"!=typeof m.src||!m.src?++c:m.addEventListener?(m.addEventListener("load",h,!1),m.addEventListener("error",h,!1)):(m.attachEvent("onload",h),m.attachEvent("onerror",h));d=b-c;
function n(){if(google.timers.load.t){google.timers.load.t.ol=(new Date).getTime();google.timers.load.t.iml=e;google.kCSI.imc=c;google.kCSI.imn=b;google.kCSI.imp=d;void 0!==google.stt&&(google.kCSI.stt=google.stt);google.csiReport&&google.csiReport()}}window.addEventListener?window.addEventListener("load",n,!1):window.attachEvent&&window.attachEvent("onload",n);google.timers.load.t.prt=e=(new Date).getTime();})();
</script></body></html>"""
